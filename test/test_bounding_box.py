"""Unit test for bounding box class
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '11/17/2019'
__version__ = '0.1.0'

import numpy
import unittest

from genfemcalc import meshgrid


class TestBoundingBox(unittest.TestCase):
    def setUp(self):
        self.sbox = meshgrid.geometrymrg.BoundingBox.fromCenterAndSize(size=2.0)

    def test_flatten(self):
        expected_interleaved = [-1.0, -1.0, -1.0, 1.0, 1.0, 1.0]
        expected_mixed = [-1.0, 1.0, -1.0, 1.0, -1.0, 1.0]
        self.assertTrue(numpy.allclose(self.sbox.flatten(), expected_interleaved))
        self.assertTrue(numpy.allclose(self.sbox.flatten(interleaved=False), expected_mixed))

    def test_extents(self):
        expected_extents = numpy.array([2.0, 2.0, 2.0])
        self.assertTrue(numpy.allclose(self.sbox.extents, expected_extents))
        scale = [1.0, 2.0, 3.0]
        box = self.sbox.scale(scale, inplace=False)
        scaled_extents = expected_extents * scale
        self.assertTrue(numpy.allclose(box.extents, scaled_extents))

    def test_fromCenterAndSize(self):
        box = meshgrid.geometrymrg.BoundingBox.fromCenterAndSize(center=(1, 1, 1), size=2)
        self.assertTrue(numpy.allclose(box.lbf, (0.0, 0.0, 0.0)))
        self.assertTrue(numpy.allclose(box.rtb, (2.0, 2.0, 2.0)))

    def test_center(self):
        box = meshgrid.geometrymrg.BoundingBox((0.0, 1.0, 2.0), (4.0, 5.0, 10.0))
        self.assertTrue(numpy.allclose(box.center, (2.0, 3.0, 6.0)))

    def test_offset(self):
        box = self.sbox.offset(distance=1.0, inplace=False)
        self.assertTrue(numpy.allclose(box.lbf, (-2.0, -2.0, -2.0)))
        self.assertTrue(numpy.allclose(box.rtb, (2.0, 2.0, 2.0)))
        box = self.sbox.offset(distance=-0.5, inplace=False)
        self.assertTrue(numpy.allclose(box.lbf, (-0.5, -0.5, -0.5)))
        self.assertTrue(numpy.allclose(box.rtb, (0.5, 0.5, 0.5)))
        box = self.sbox.offset(distance=(0.1, 0.2, 0.3), inplace=False)
        self.assertTrue(numpy.allclose(box.lbf, (-1.1, -1.2, -1.3)))
        self.assertTrue(numpy.allclose(box.rtb, (1.1, 1.2, 1.3)))

    def test_anchorAt(self):
        box = meshgrid.geometrymrg.BoundingBox((0.0, 1.0, 2.0), (4.0, 5.0, 10.0))
        box.anchorAt((0.0, 0.0, 0.0))
        self.assertTrue(numpy.allclose(box.flatten(), (0.0, 0.0, 0.0, 4.0, 4.0, 8.0)))
        box = meshgrid.geometrymrg.BoundingBox((0.0, 1.0, 2.0), (4.0, 5.0, 10.0))
        box.anchorAt((0.0, 0.0, 0.0), anchor='rtb')
        self.assertTrue(numpy.allclose(box.flatten(), (-4.0, -4.0, -8.0, 0.0, 0.0, 0.0)))
        box = meshgrid.geometrymrg.BoundingBox((0.0, 1.0, 2.0), (4.0, 5.0, 10.0))
        box.anchorAt((0.0, 0.0, 0.0), anchor='center')
        self.assertTrue(numpy.allclose(box.flatten(), (-2.0, -2.0, -4.0, 2.0, 2.0, 4.0)))
