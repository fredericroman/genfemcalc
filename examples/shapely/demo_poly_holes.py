# -*- coding: utf-8 -*-
"""
Holes in polygons
-----------------
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '11/16/2019'
__version__ = '0.1.0'

import numpy
import os
import random
import shapely

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
import meshgrid.graphics
from meshgrid.utilsshapely import buildRandomPolygon
import scipy.spatial

PKGDIR = os.path.dirname(os.path.abspath(genfemcalc.__file__))
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


def removeShortEdges(coords, eps):
    """Remove consecutive points that are closer than eps.
    """
    newcoords = [coords[0]]
    eps2 = eps*eps

    def square_dist(a, b):
        return (a[0]-b[0])**2 + (a[1]-b[1])**2
    for i in range(1, len(coords)):
        if square_dist(newcoords[-1], coords[i]) >= eps:
            newcoords.append(coords[i])
        elif len(coords[i]) > 2 and coords[i][2] < -0.5:
            newcoords[-1] = coords[i]


    return newcoords


def projectCoordinates(coords, projection_tree, distance,
                       eps=0.0, skip_condition=None):
    if len(coords) < 0:
        return 0, coords
    counter = 0
    pts = numpy.asarray(coords)
    print("DUPA DUPA : ", pts.shape)
    poly_tree = scipy.spatial.cKDTree(pts[:, 0:2])
    neighbours = poly_tree.query_ball_tree(projection_tree, r=distance, eps=eps)
    for i in range(len(neighbours)):
        if skip_condition is None or skip_condition(pts[i, :]):
            continue
        if len(neighbours[i]) > 0:
            j = neighbours[i][0]
            pts[i, 0:2] = projection_tree.data[j, :]
    return counter, pts.tolist()


def projectPolyPoints(poly, pts_tree, distance, eps, with_holes=True):
    print('Poly')
    counter = 0
    holes = []
    sk = lambda x: len(x)>2 and x[2] < -0.5
    cnt, exterior = projectCoordinates(poly.exterior.coords, pts_tree, distance,
                                       eps, sk)
    exterior = removeShortEdges(exterior, distance/10)
    counter += cnt
    if len(exterior) < 3:
        return cnt, None
    if with_holes:
        for hole in poly.interiors:
            cnt, interior = projectCoordinates(hole.coords, pts_tree, distance, eps, sk)
            interior = removeShortEdges(interior, distance/10)
            if len(interior) > 2:
                holes.append(tuple(interior))
            counter += cnt
    if len(holes) > 0:
        out_poly = shapely.geometry.Polygon(exterior, holes)
    else:
        out_poly = shapely.geometry.Polygon(exterior)
    js = shapely.geometry.JOIN_STYLE.mitre
    sm_eps = distance/4
    out_poly = out_poly.buffer(-sm_eps, 1, join_style=js).buffer(sm_eps, 1, join_style=js)
    if out_poly.area < (distance/2)**2:
        print('Degenerate poly')
        out_poly = None
    return counter, out_poly



class DemoPolyHoles(examples.Demo):
    def buildHoles(self):
        holes = []
        s = random.randint(0,10000)
        self.seed = s
        print('Seed: ',s)

        spec = {'out_radius': 0.40, 'nsides': 8, 'randomize_angle': False,
                'start_angle': 0.0, 'radius_factor': 1.0,
                'z': 1.0, 'seed':  9531}
        holes.append(buildRandomPolygon((1.5, 1.5), **spec))
        spec['nsides'] = 24
        spec['radius_factor'] = 0.8
        holes.append(buildRandomPolygon((0.5, 1.5), **spec))
        spec['nsides'] = 16
        spec['radius_factor'] = 0.5
        holes.append(buildRandomPolygon((1.5, 0.5), **spec))
        return holes

    def cutHoles(self, base, holes):
        domain = []
        for base_poly in base:
            for hole in holes:
                if hole.intersects(base_poly):
                    base_poly = base_poly.difference(hole)
                    if base_poly.is_empty:
                        break
            if not base_poly.is_empty:
                if base_poly.geom_type in ['Polygon', 'MultiPolygon']:
                    domain.append(base_poly)
                elif base_poly.geom_type == 'GeometryCollection':
                    for geom in base_poly:
                        if geom.geom_type in ['Polygon', 'MultiPolygon']:
                            domain.append(geom)
        return domain

    def cleanupPolyModel(self, poly_soup, radius, eps=0.0):
        """
        Cleanpu Poly model by removing very close vertices and
        projecting other vertices on close edges of background mesh
        """
        cleaned_poly_soup = []
        points = meshgrid.utilsshapely.getPointsOfPolySoup(poly_soup)
        print('Points of poly soup', points.shape)
        print(points)
        idx = numpy.nonzero(points[:, 2] < 1)[0]
        print(idx)
        point_tree = scipy.spatial.cKDTree(points[idx, 0:2])
        counter = 0
        for poly in poly_soup:
            if poly.geom_type not in ['Polygon', 'MultiPolygon']:
                raise ValueError(f"Invalid poly type: {poly.geom_type}")
            if isinstance(poly, shapely.geometry.MultiPolygon):
                out_sub_polys = []
                for subpoly in poly:
                    cnt, out_poly = projectPolyPoints(subpoly, point_tree, radius, eps)
                    if out_poly is not None:
                        if out_poly.geom_type not in ['Polygon', 'MultiPolygon']:
                            raise ValueError('KUPA')
                        cleaned_poly_soup.append(out_poly)
                    counter += cnt
            else:
                cnt, out_poly = projectPolyPoints(poly, point_tree, radius, eps)
                if out_poly is not None:
                    if out_poly.geom_type not in ['Polygon', 'MultiPolygon']:
                        raise ValueError('DUPA')
                    cleaned_poly_soup.append(out_poly)
                counter += cnt
        return counter, cleaned_poly_soup

    def demonstrate(self):
        model = meshgrid.modelsgmsh.Rectangle()
        context = {'xa': 0.0, 'xb': 2.0, 'ya': 0.0, 'yb': 2.0,
                   'quadsonly': True, 'transfinite': True, 'edge_size':0.6,
                   'tsdensity': [6, 6]}
        domain = model.discretize(context)
        mesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh)
        mesh.node_coords[:, 2] = -1.0
        holes = shapely.geometry.MultiPolygon(self.buildHoles())
        polysoup = meshgrid.utilsshapely.makeMultiPolygon(mesh)
        ptt = meshgrid.utilsshapely.getPointsOfPolySoup(holes)
        print('---------------------HERE ')
        print(ptt)
        holesdomain = self.cutHoles(polysoup, holes)

        def get_pts_colors(poly_soup):
            pts = []
            colors = []
            for poly in poly_soup:
                if isinstance(poly, shapely.geometry.MultiPolygon):
                    colors.append('red')
                    for subpoly in poly:
                        for pt in subpoly.exterior.coords:
                            pts.append(pt)
                else:
                    colors.append('yellow')
                    for pt in poly.exterior.coords:
                        pts.append(pt)
            return pts, colors
        pts, colors = get_pts_colors(holesdomain)
        pts = numpy.asarray(pts)
        print('HERE SHAHE is : ', pts.shape)
        viewer = meshgrid.graphics.MeshViewer(nrows=1, ncols=2, colorbar=False)
        viewer.showPolygons(holesdomain, viewport=(0, 0), colors=colors)
        viewer.cbar.setArray(pts[:, 2])
        viewer.cbar.setLut(3)
        # viewer.cbar.cbar.set_ticks(numpy.array([-1, 0, 1]))
        viewer.axes[0, 0].scatter(pts[:, 0], pts[:, 1], zorder=10, c=pts[:, 2],
                                  cmap=viewer.cbar.cmap, s=3)
        projected_count, cleaned = self.cleanupPolyModel(holesdomain, radius=0.10, eps=0.001)
        projected_count, cleaned = self.cleanupPolyModel(cleaned, radius=0.10, eps=0.001)
        projected_count, cleaned = self.cleanupPolyModel(cleaned, radius=0.10, eps=0.001)
        pts, dummy = get_pts_colors(cleaned)
        outfile = os.path.join(OUTDIR, 'cut_holes.vtk')
        meshgrid.utilsshapely.writeVtkFromPolySoup(outfile, cleaned)
        viewer.showPolygons(cleaned, viewport=(0, 1), shrink=None, colors=colors, edgecolor='black')
        print("Projected points: ", projected_count)
        pts = numpy.asarray(pts)
        print("PTS SHAPE: ", pts.shape)
        viewer.axes[0, 1].scatter(pts[:, 0], pts[:, 1], zorder=10,
                                  s=2, cmap=viewer.cbar.cmap)
        viewer.autoscale(aspect='equal')
        viewer.keep()

if __name__ == '__main__':
    demo = DemoPolyHoles()
    demo.run()
    print('Seed: ', demo.seed)
