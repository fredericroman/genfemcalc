# -*- coding: utf-8 -*-
"""
Visualize polygonal soup
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '11/23/2019'
__version__ = '0.1.0'

import os
import shapely

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
import meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoShowPolySoup(examples.Demo):
    def makePolySoup(self):
        poly_soup = []
        poly_spec = {'center': (0.0, 0.0), 'out_radius': 1.0,
                     'nsides': 4, 'start_angle': 45.0, 'randomize_angle': False}
        poly_soup.append(meshgrid.utilsshapely.buildRandomPolygon(**poly_spec))
        poly_spec['center'] = (1.0, 1.0)
        poly_soup.append(meshgrid.utilsshapely.buildRandomPolygon(**poly_spec))
        poly_spec['center'] = (-2.0, 2.0)
        outer = meshgrid.utilsshapely.buildRandomPolygon(**poly_spec)
        poly_spec['center'] = (-2.2, 2.0)
        poly_spec['out_radius'] = 0.1
        inner1 = meshgrid.utilsshapely.buildRandomPolygon(**poly_spec)
        poly_spec['center'] = (-1.8, 2.0)
        inner2 = meshgrid.utilsshapely.buildRandomPolygon(**poly_spec)
        holes = shapely.geometry.MultiPolygon((inner1, inner2))
        diff = outer.difference(inner1)
        diff = diff.difference(inner2)
        poly_soup.append(diff)
        poly_soup.append(holes)
        return poly_soup

    def demonstrate(self):
        poly_soup = self.makePolySoup()
        meshgrid.utilsshapely.countPolySoup(poly_soup)
        colors = []
        for poly in poly_soup:
            if isinstance(poly, shapely.geometry.MultiPolygon):
                colors.append('red')
            else:
                colors.append('yellow')
        pts = meshgrid.utilsshapely.getPointsOfPolySoup(poly_soup)
        viewer = meshgrid.graphics.MeshViewer()
        viewer.showPolygons(poly_soup, colors=colors)
        viewer.showPoints(pts, zorder=10)
        viewer.autoscale()
        viewer.keep()

if __name__ == '__main__':
    demo = DemoShowPolySoup()
    demo.run()
