genfemcalc.meshgrid.geometrymrg module
======================================

.. automodule:: genfemcalc.meshgrid.geometrymrg
   :members:
   :undoc-members:
   :show-inheritance:
