# -*- coding: utf-8 -*-
"""
Set of various utilities for checking matrix properties

Created on Wed Aug  7 06:52:15 2019

@author: putanowr
"""

import sys
import numpy

def isSymmetric(M, rtol=1e-05, atol=1e-08):
    """Return true if matrix is symmetric
    """
    return numpy.allclose(M, M.T, rtol=rtol, atol=atol)

def isInvertible(M, tol=sys.float_info.epsilon):
    """Return true when matrix is invertible, that is it is non
    singular and the inverse will not be corrupted by rounding error
    """
    return numpy.linalg.cond(M) < 1/tol
       
