if __debug__:
    print(f'Invoking __init__.py for {__name__}')

from . import fem
from . import fembasic
from . import meshgrid

