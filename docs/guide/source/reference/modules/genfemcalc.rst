genfemcalc package
==================

.. automodule:: genfemcalc
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::

   genfemcalc.benchmarks
   genfemcalc.examples
   genfemcalc.fem
   genfemcalc.fembasic
   genfemcalc.meshgrid
   genfemcalc.test
   genfemcalc.utils
