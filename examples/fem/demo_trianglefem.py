# -*- coding: utf-8 -*-
"""
Triangle element terms
======================
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/5/2019'
__version__ = '0.1.0'

import numpy as np
import os

import genfemcalc
from genfemcalc import examples

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoTriangleFem(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        element_points = np.array([[0.0, 0.0, 0.0],
                                    [2.0, 0.0, 0.0],
                                    [0.0, 1.0, 0.0]])
        M = genfemcalc.fembasic.trianglefem.calcMassTerm(element_points)
        print(M)
        # This matrix should be 1/6 1/12 1/12
        #                       1/12 1/6 1/12
        #                       1/12 1/12 1/6
        #  Times the are of the triangle


if __name__ == '__main__':
    demo = DemoTriangleFem()
    demo.run()
