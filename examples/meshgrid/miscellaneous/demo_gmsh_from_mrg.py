# -*- coding: utf-8 -*-
"""
Gmsh mesh from MRG
==================

TODO : Add description of demo demo_gmsh_from_mrg.py
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '11/8/2019'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid


PKGDIR = os.path.dirname(os.path.abspath(genfemcalc.__file__))
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoGmshFromMrg(examples.Demo):
    def demonstrate(self):
        infile = os.path.join(DATADIR, 'quad_triangle_line.vtk')
        mrgmesh = meshgrid.iomrg.readMrgFromVtk(infile)
        mrgmesh.listElemCount()
        outfile = os.path.join(OUTDIR, 'qtl_out.vtk')
        meshgrid.iomrg.writeGmshFromMrg(outfile, mrgmesh)


if __name__ == '__main__':
    demo = DemoGmshFromMrg()
    demo.run()
