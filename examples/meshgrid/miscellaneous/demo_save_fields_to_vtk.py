# -*- coding: utf-8 -*-
"""
Save mesh with fields to VTK
============================

"""
__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.04  19:28:19'
__version__ = '0.1.0'

import os
import numpy as np

import genfemcalc
from genfemcalc import meshgrid
from genfemcalc import examples

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoSaveFieldsToVTK(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        file_name = os.path.join(DATADIR, 'simple_quad_tri_mesh.vtk')
        mesh = meshgrid.iomrg.readMrgFromVtk(file_name)
        f = mesh.evalNodeField('x_plus_y', 'x+y')
        print('Field shape: ', f.shape)
        f = mesh.evalNodeField('velocity', '[x,y]')
        print('Field shape: ', f.shape)

        mesh.addElemField('markers', np.arange(mesh.numb_elems))

        outfile = os.path.join(DATADIR, 'mesh_with_fields.vtk')
        meshgrid.iomrg.writeVtkFromMrg(mesh, outfile, binary=False)


if __name__ == '__main__':
    demo = DemoSaveFieldsToVTK()
    demo.run()
