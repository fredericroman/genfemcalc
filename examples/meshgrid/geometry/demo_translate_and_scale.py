# -*- coding: utf-8 -*-
"""
Mesh translation and scaling
----------------------------
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/11/2019'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoTranslateAndScale(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        filename = os.path.join(DATADIR, 'square_5_5.png')
        mesh = meshgrid.iomrg.readMrgFromImage(filename)

        moved_mesh = meshgrid.geometrymrg.anchorMeshAt(mesh, [0.0, 0.0, 0.0],
                                                       mode='lbf', inplace=False)
        skinner = meshgrid.adjacency.Skinner.fromMesh(moved_mesh)

        boundary_mesh = skinner.buildMesh()

        moved_mesh = meshgrid.geometrymrg.scale(moved_mesh, [0.5])

        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerMesh(mesh, 'mesh')
        viewer.registerMesh(moved_mesh, 'moved_mesh')
        viewer.registerMesh(boundary_mesh, 'boundary_mesh')
        viewer.show('mesh', color='black', facecolor='yellow')
        viewer.show('moved_mesh', color='black', facecolor='green')
        viewer.show('boundary_mesh', color='red')
        viewer.keep()


if __name__ == '__main__':
    demo = DemoTranslateAndScale()
    demo.run()
