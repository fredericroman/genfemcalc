# -*- coding: utf-8 -*-
"""
Field evaluation
================

"""
__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.04  19:18:20'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoEvalField(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(sefl):
        fname = os.path.join(DATADIR, 'simple_quad_tri_mesh.vtk')
        mymesh = meshgrid.iomrg.readMrgFromVtk(fname)
        mymesh.evalNodeField('x_plus_y', 'x+y')
        mymesh.evalNodeField('velocity', '[x,y]')
        print(mymesh.node_coords)
        print('-------------')
        print(mymesh.node_fields['x_plus_y'])
        print('-------------')
        print(mymesh.node_fields['velocity'])


if __name__ == '__main__':
    demo = DemoEvalField()
    demo.demonstrate()
