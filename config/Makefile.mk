export genfemcalc_CONFIG_DIR

list-default :
	$(foreach prog, $(DEMOS), $(info $(prog)))

demo_% : demo_%.py
	"$(PYTHON)" -O $< --close-images
