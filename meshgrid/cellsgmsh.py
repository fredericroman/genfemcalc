"""Support for GMSH cell types

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.10.14 9:47:4'
__version__ = '0.3.0'

import itertools
import numpy as np
import sympy


def getCellsShapes():
    """Return set of shapes (as strings) that are possible for gmsh cells.

    Returns
    ------
        set of :obj:`string`
            Set of names of GMSH cells' shapes

    Example
    -------
    Display names of GMSH cell shapes sorted alphabetically

    .. doctest::

        >>> from genfemcalc.fem.cellsgmsh import getCellsShapes
        >>> print(sorted(getCellsShapes()))
        ['hexahedron', 'line', 'point', 'quadrangle', 'tetrahedron', 'triangle']

    """
    shapes = set()
    for info in getCellsInfo().values():
        shapes.add(info['shape'])
    return shapes


def filterCellsInfo(selector):
    """Select subset of cells that meet the criteria specified by selector

    The selector is a dictionary. Possible keys are:
        shape
        numb_nodes
        dim
        numb_edges
        total_degree
        partial_degree
    """
    criteria = ['shape', 'numb_nodes', 'dim', 'numb_edges', 'total_degree', 'partial_degree']

    if set(criteria).isdisjoint(selector.keys()):
        raise ValueError('Selector has not any valid criteria')

    def query(cell_info):
        return {
            'shape': cell_info.shape,
            'dim': cell_info.dim,
            'numb_nodes': cell_info.sym_nodes.shape[0],
            'numb_edges': len(cell_info.edges),
            'numb_faces': len(cell_info.faces),
            'total_degree': cell_info.total_degree,
            'partial_degree': cell_info.partial_degree,
            'incomplete': cell_info.incomplete
        }

    selected_info = {}
    for typeid, info in getCellsInfo().items():
        outcome = dict(zip(criteria, itertools.repeat('True')))
        info_query = query(info)
        for key in criteria:
            outcome[key] = info_query[key] in list(selector[key])
        if all(outcome.values()):
            selected_info[typeid] = info

    return selected_info


def getCellsInfo(typeid=None):
    """Return dictionary with information about reference cell.

    Parameters
    ----------
        typeid : int
            Numeric id of GMSH cell type.
    Return
    ------
        dict
            Either the dictionary of information for a single cell type
            or the dictionary of dictionaries for all cell types.
    Example
    -------
    To find edges connectivity in linear quad element which in GMSH has id 3
    we can do

    >>> from genfemcalc.fem.cellsgmsh import getCellsInfo
    >>> quad_cell = getCellsInfo(3)
    >>> print(f"quad edges -> {quad_cell['edges']}")
    quad edges -> [[0, 1], [1, 2], [2, 3], [3, 0]]

    """
    def tosymbolic(x):
        return sympy.nsimplify(np.array(x, dtype=np.float64), rational=True)
    info = {1: {'type': 1,
                'dim': 1,
                'shape': 'line',
                'description': '2-node line',
                'faces': [],
                'faces_type': [],
                'edges': [[0, 1]],
                'edges_type': [1],
                'sym_nodes': tosymbolic([[0, 0, 0], [1, 0, 0]]),
                'monobase': '[1,x]',
                'total_degree': 1,
                'partial_degree': 1,
                'incomplete': False},
            2: {'type': 2,
                'dim': 2,
                'shape': 'triangle',
                'description': '3-node triangle',
                'faces': [[0, 1, 2]],
                'faces_type': [2],
                'edges': [[0, 1], [1, 2], [2, 0]],
                'edges_type': [1, 1, 1],
                'sym_nodes': tosymbolic([[0, 0, 0], [1, 0, 0], [0, 1, 0]]),
                'monobase': '[1,x,y]',
                'total_degree': 1,
                'partial_degree': 1,
                'incomplete': False},
            3: {'type': 3,
                'dim': 2,
                'shape': 'quadrangle',
                'description': '4-node quadrangle',
                'faces': [[0, 1, 2, 3]],
                'faces_type': [3],
                'edges': [[0, 1], [1, 2], [2, 3], [3, 0]],
                'edges_type': [1, 1, 1, 1],
                'sym_nodes': tosymbolic([[0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0]]),
                'monobase': '[1,x,y,x*y]',
                'total_degree': 2,
                'partial_degree': 1,
                'incomplete': False},
            4: {'type': 4,
                'dim': 3,
                'shape': 'tetrahedron',
                'description': '4-node tetrahedron',
                'faces': [[0, 2, 1], [0, 3, 2], [3, 1, 2], [1, 0, 3]],
                'faces_type': [2, 2, 2, 2],
                'edges': [[0, 1], [0, 2], [0, 3], [1, 2], [1, 3], [2, 3]],
                'edges_type': [1, 1, 1, 1, 1, 1],
                'sym_nodes': tosymbolic([[0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]]),
                'monobase': '[1,x,y,z]',
                'total_degree': 1,
                'partial_degree': 1,
                'incomplete': False},
            5: {'type': 5,
                'dim': 3,
                'shape': 'hexahedron',
                'description': '8-node hexahedron',
                'faces': [[0, 3, 2, 1], [4, 7, 6, 5], [0, 4, 7, 3],
                          [1, 2, 6, 5], [2, 3, 7, 6], [0, 1, 5, 4]],
                'faces_type': [3, 3, 3, 3, 3, 3],
                'edges': [[0, 1], [1, 2], [2, 3], [3, 0], [4, 5], [5, 6], [6, 7], [7, 4],
                          [0, 4], [1, 4], [2, 6], [3, 7]],
                'edges_type': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                'sym_nodes': tosymbolic([[0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0],
                                         [0, 0, 1], [1, 0, 1], [1, 1, 1], [0, 1, 1]]),
                'monobase': '[1,x,y,z,x*y,x*z,y*z,x*y*z]',
                'total_degree': 3,
                'partial_degree': 1,
                'incomplete': False},
            8: {'type': 8,
                'dim': 1,
                'shape': 'line',
                'description': '3-node second order line (2 nodes associated with the vertices and 1 with the edge)',
                'faces': [],
                'faces_type': [],
                'edges': [[0, 2, 1]],
                'edges_type': [8],
                'sym_nodes': tosymbolic([[0, 0, 0], [1, 0, 0], [0.5, 0, 0]]),
                'monobase': '[1,x,x*x]',
                'degree': 2,
                'incomplete': False},
            9: {'type': 9,
                'dim': 2,
                'shape': 'triangle',
                'description': ('6-node second order triangle'
                                '(3 nodes associated with the vertices and 3 with the edges)'),
                'faces': [[0, 3, 1, 4, 2, 5]],
                'faces_type': [9],
                'edges': [[0, 3, 1], [1, 4, 2], [2, 5, 0]],
                'edges_type': [8, 8, 8],
                'sym_nodes': tosymbolic([[0, 0, 0], [1, 0, 0], [0, 1, 0],
                                         [0.5, 0, 0], [0.5, 0.5, 0], [0, 0.5, 0]]),
                'monobase': '[1,x,y,x*y,x*x,y*y]',
                'total_degree': 2,
                'partial_degree': 2,
                'incomplete': False},
            10: {'type': 10,
                 'dim': 2,
                 'shape': 'quadrangle',
                 'description': ('9-node second order quadrangle (4 nodes associated with the vertices,'
                                 ' 4 with the edges and 1 with the face)'),
                 'faces': [[0, 4, 1, 5, 2, 6, 3, 7]],
                 'faces_type': [10],
                 'edges': [[0, 4, 1], [1, 5, 2], [2, 6, 3], [3, 7, 0]],
                 'edges_type': [8, 8, 8, 8],
                 'sym_nodes': tosymbolic([[0, 0, 0],
                                          [1, 0, 0],
                                          [1, 1, 0],
                                          [0, 1, 0],
                                          [0.5, 0, 0],
                                          [1.0, 0.5, 0],
                                          [0.5, 1.0, 0],
                                          [0.0, 0.5, 0],
                                          [0.5, 0.5, 0]]),
                 'monobase': '[1,x,y,x*y,x*x,y*y,x**2*y,x*y**2,x**2*y**2]',
                 'total_degree': 4,
                 'partial_degree': 2,
                 'incomplete': False},
            15: {'type': 15,
                 'dim': 0,
                 'shape': 'point',
                 'description': '1-node point',
                 'edges': [],
                 'edges_type': [],
                 'faces': [],
                 'faces_type': [],
                 'sym_nodes': tosymbolic([[0, 0, 0]]),
                 'monobase': '[1]',
                 'total_degree': 0,
                 'partial_degree': 0,
                 'incomplete': False}}
    if typeid is not None:
        return info[typeid]
    else:
        return info
