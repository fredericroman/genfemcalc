# -*- coding: utf-8 -*-
"""Reference elements.

This module provides utilities to describe reference elements.

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.13 11:19:25'
__version__ = '0.3.0'

import enum
import sympy
import sympy.parsing.sympy_parser
import numpy as np

from genfemcalc import meshgrid
from genfemcalc import utils


class CellFamily(enum.IntEnum, metaclass=utils.enums.MetaIntEnum):
    """Enum for cell families
    MetaIntEnum adds to this class two convenient methods: fromStr, convert
    """
    GMSH = 0
    VTK = 1
    MESHIO = 2


def buildShapeFunctions(dim, nodes, monobase):
    base = sympy.Matrix(sympy.parsing.sympy_parser.parse_expr(monobase))
    nb = len(base)
    base_on_nodes = sympy.zeros(nb, nb)
    x, y, z = sympy.symbols('x y z')
    for i in range(nb):
        for j in range(nb):
            base_on_nodes[i, j] = base[i].subs({x: nodes[j, 0], y: nodes[j, 1], z: nodes[j, 2]})
    symbolic_base = base_on_nodes.inv()*base
    args = [x, y, z]
    numeric_base = utils.symbolic.lambdify_wrapper(args[0:dim], symbolic_base)
    return symbolic_base, numeric_base


def buildJacobian(dim, symbolic):
    args = sympy.symbols('x y z')
    symbolic_jac = symbolic.jacobian(args[0:dim])
    numeric_jac = utils.symbolic.lambdify_wrapper(args[0:dim], symbolic_jac)
    return symbolic_jac, numeric_jac


class RefCell(object):
    def __init__(self, family, info):
        type_translator = CellIdTranslator('gmsh', family)
        self._cell_type = type_translator(info['type'])
        self._family = CellFamily.convert((family,))
        self._sym_nodes = info['sym_nodes']
        self._shape = info['shape']
        self._description = info['description']
        self._dim = info['dim']
        self._faces = info['faces']
        self._edges = info['edges']
        self._edges_type = [type_translator(t) for t in info['edges_type']]
        self._faces_type = [type_translator(t) for t in info['faces_type']]
        self._numb_nodes = self._sym_nodes.shape[0]

        to_numeric = sympy.lambdify([], self._sym_nodes)
        self._points = np.array(to_numeric())
        self._numb_faces = len(self._faces)
        self._numb_edges = len(self._edges)
        self._numb_facets = [1, 2, len(self.edges), len(self.faces)][self._dim]
        symbolic, numeric = buildShapeFunctions(self._dim, self._sym_nodes,
                                                info['monobase'])
        self._numeric_shape_fun = numeric
        self._symbolic_shape_fun = symbolic

        symbolic_jac, numeric_jac = buildJacobian(self._dim, symbolic)
        self._numeric_jacobian = numeric_jac
        self._symbolic_jacobian = symbolic_jac

    @property
    def family(self):
        return self._family

    @property
    def shape(self):
        return self._shape

    @property
    def numb_nodes(self):
        return self._numb_nodes

    @property
    def numb_edges(self):
        return len(self.edges)

    @property
    def numb_faces(self):
        return len(self.faces)

    @property
    def numb_facets(self):
        return self._numb_facets

    @property
    def points(self):
        return self._points

    @property
    def type(self):
        return self._cell_type

    @property
    def edges_type(self):
        return self._edges_type

    @property
    def faces_type(self):
        return self._faces_type

    @property
    def faces(self):
        return self._faces

    @property
    def edges(self):
        return self._edges
        
    @property
    def numeric_shape_fun(self):
        return self._numeric_shape_fun

    @property
    def symbolic_shape_fun(self):
        return self._symbolic_shape_fun

    @property
    def numeric_jacobian(self):
        return self._numeric_jacobian
    
    @property
    def symbolic_jacobian(self):
        return self._symbolic_jacobian

    @property
    def dim(self):
        return self._dim

    def N(self, points):
        """Evaluate shape functions on points with reference coordinates

        Parameters
        ----------
            points : ndarray
                2D array of point coordinates. Its shape should be (g,k) where g is number of points
                and k is number of coordinates. CAUTION: It should be k >= cell dimension.
                If k > cell dimension superfluous coordinates are ignored

        Returns
        -------
            N : ndarray (2D array)
                The returned array has shape (g, n) where g is number of evaluation points,
                and n is the number of shape functions.
        """
        mat = self._numeric_shape_fun(*points[:, 0:self.dim].T)
        return np.squeeze(np.moveaxis(mat, 2, 0))

    def dN(self, points):
        """Evaluate shape function derivatives on points with reference coordinates

        Parameters
        ----------
            points : ndarray
                2D array of point coordinates. Its shape should be (g,k) where g is number of points
                and k is number of coordinates. CAUTION: It should be k >= cell dimension.
                If k > cell dimension superfluous coordinates are ignored

        Returns
        -------
            N : ndarray (3D array)
                The returned array has shape (g, n, d) where g is number of evaluation points,
                n is the number of shape functions and d is the number of derivatives.

        """
        mat = self._numeric_jacobian(*points[:, 0:self.dim].T)
        return np.moveaxis(mat, [0, 2], [2, 0])

    @property
    def facets(self):
        """Return facets definitions that list of list of nodes
        that form the cell facets.
        """
        if self.dim == 3:
            return self.faces
        elif self.dim == 2:
            return self.edges
        elif self.dim == 1:
            return [[self.edges[0][0]], [self.edges[0][-1]]]
        else:
            return [0]

    @property
    def facets_type(self):
        if self.dim == 3:
            return self.faces_type
        elif self.dim == 2:
            return self.edges_type
        elif self.dim == 1:
            return [15, 15]
        else:
            return [15]


def getRefCell(family, type_id):
    """Return reference cell of given family.

    Parameters
         family - integer, string or CellFamily enum
         typeId - integer

    rc = getRefCell('vtk', 3)
    """
    id_translator = CellIdTranslator(family, 'gmsh')
    gmsh_id = id_translator(type_id)
    ci = meshgrid.cellsgmsh.getCellsInfo(gmsh_id)
    return RefCell(family, ci)


def idVtkToGmsh(vtk_id):
    """Return GMSH cell type corresponding to VTK cell type.
    Parameters
    ----------
        vtk_id : int
            Numeric id of VTK cell type
    Returns
    -------
        int
            Numeric id of GMSH cell type
    """
    vtk2gmsh = {1: 15,  # point
                3:  1,  # line
                5:  2,  # triangle
                9:  3,  # quadrangle
                10: 4,  # tetrahedron
                12: 5   # hexahedron
                }
    type_id = int(vtk_id)
    gmsh_id = vtk2gmsh.get(type_id)
    if gmsh_id is None:
        raise ValueError(f"VTK cell {type_id} has not equivalent GMSH")
    return gmsh_id


def idGmshToVtk(gmsh_id):
    """Return VTK cell type corresponding to GMSH cell type.
    """
    gmsh2vtk = {1: 3,   # line
                2: 5,   # triangle
                3: 9,   # quadrangle
                4: 10,  # tetrahedron
                5: 12,  # hexahedron
                15: 1,   # point
                }
    type_id = int(gmsh_id)
    vtk_id = gmsh2vtk.get(type_id)
    if vtk_id is None:
        raise ValueError(f"GMSH cell {type_id} has not equivalent VTK")
    return vtk_id


def idMeshioToGmsh(meshio_id):
    """Translate meshio cell type to GMSH cell type.
    """
    # noinspection PyProtectedMember
    gmsh_id = meshgrid.utilsgmsh.meshio_to_gmsh_type.get(meshio_id)
    if gmsh_id is None:
        raise ValueError(f"Invalid meshio cell type: {meshio_id}")
    return gmsh_id


def idGmshToMeshio(gmsh_id):
    """Translate GMSH cell type to meshio cell type.
    """
    # noinspection PyProtectedMember
    meshio_id = meshgrid.utilsgmsh.gmsh_to_meshio_type.get(gmsh_id)
    if meshio_id is None:
        raise ValueError(f"GMSH cell type {gmsh_id} has no equivalent meshio cell type")
    return meshio_id


def idVtkToMeshio(vtk_id):
    return idGmshToMeshio(idVtkToGmsh(vtk_id))


def idMeshioToVtk(meshio_id):
    return idGmshToVtk(idMeshioToGmsh(meshio_id))


def idIdentity(type_id):
    return type_id


class CellIdTranslator(object):
    __translators = {
        (CellFamily.VTK, CellFamily.VTK): idIdentity,
        (CellFamily.VTK, CellFamily.MESHIO): idVtkToMeshio,
        (CellFamily.VTK, CellFamily.GMSH): idVtkToGmsh,
        (CellFamily.GMSH, CellFamily.GMSH): idIdentity,
        (CellFamily.GMSH, CellFamily.VTK): idGmshToVtk,
        (CellFamily.GMSH, CellFamily.MESHIO): idGmshToMeshio,
        (CellFamily.MESHIO, CellFamily.MESHIO): idIdentity,
        (CellFamily.MESHIO, CellFamily.GMSH): idMeshioToGmsh,
        (CellFamily.MESHIO, CellFamily.VTK): idMeshioToVtk
    }

    def __init__(self, from_family, to_family):
        f = CellFamily.convert(from_family)
        t = CellFamily.convert(to_family)
        self._direction = (f, t)
        self._translator = self.__translators[(f, t)]

    @property
    def direction(self):
        return self._direction

    def __call__(self, cell_type):
        return self._translator(cell_type)


def buildRefcellsDict(mesh):
    """Creates dictionary of reference cells.
    The dictionary is indexed by cell type ID.

    Parameters
    ----------
        mesh : object supporting mesh protocol
    Returns
    -------
        dict :
            Dictionary of meshgrid.refcells.RefCell objects
    """
    cells_dict = dict()
    for type_id in mesh.unique_elem_types:
        cells_dict[type_id] = meshgrid.refcells.getRefCell(mesh.cells_family, type_id)
    return cells_dict
