genfemcalc.benchmarks package
=============================

.. automodule:: genfemcalc.benchmarks
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::

   genfemcalc.benchmarks.errors
   genfemcalc.benchmarks.exact_solutions
   genfemcalc.benchmarks.matrices
   genfemcalc.benchmarks.meshes
   genfemcalc.benchmarks.problems
