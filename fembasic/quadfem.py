"""This module provides function for calculation of FEM matrices
and vectors on 2D quad element

Reference quad element : [-1,1]x[-1,1]
Vertex numbering

.. code-block:: text

        3-------2
        |       |
        |       |
        0-------1

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.13 11:18:50'
__version__ = '0.2.0'

import itertools
import numpy as np
import math


def calcArea(x, y):
    r"""Calculate area of quad with nodes (x_i,y_i)
    The implementation uses Shoelance formula:
    https://en.wikipedia.org/wiki/Shoelace_formula

    CAUTION: coordinates should be in counterclockwise order
    """
    return 0.5*np.abs(np.dot(x, np.roll(y, 1))-np.dot(y, np.roll(x, 1)))


def calcShapeFun(ksi, eta):
    """Calculate shape function at points with reference
    coordinates in ksi and eta

    Return:
        Matrix (n by 4) of shape function values
    """
    npt = len(ksi)
    assert npt == len(eta), "Coordinates must be of the same length"
    # noinspection PyPep8Naming
    N = np.zeros((npt, 4), dtype=np.float64)
    N[:, 0] = (1-ksi)*(1-eta)/4
    N[:, 1] = (1+ksi)*(1-eta)/4
    N[:, 2] = (1+ksi)*(1+eta)/4
    N[:, 3] = (1-ksi)*(1+eta)/4
    return N


def calcShapeDerivFun(ksi, eta):
    """Calculate derivatives of shape function at points with reference
    coordinates in ksi and eta

    Returns
    -------
        ndarray
            dN (n by 2*4) of derivatives values the arrangement in a row is:
            dN_0/dksi, dN_0/deta, ... dN_i/dkis, dN_i/deta ...
    """
    npt = len(ksi)
    assert npt == len(eta), "Coordinates must be of the same length"
    # noinspection PyPep8Naming
    dN = np.zeros((npt, 4*2), dtype=np.float64)

    dN[:, 0] = -(1-eta)/4
    dN[:, 2] = (1-eta)/4
    dN[:, 4] = (1+eta)/4
    dN[:, 6] = -(1+eta)/4

    dN[:, 1] = -(1-ksi)/4
    dN[:, 3] = -(1+ksi)/4
    dN[:, 5] = (1+ksi)/4
    dN[:, 7] = (1-ksi)/4
    return dN


def calcJacobian2D2D(x, y, dN):
    r"""Calculate jacobian matrix for isoparametric quad element
    with nodal coordinates :math:`(x_i, y_i)` at specific points :math:`(\xi,\eta)`
    in reference element

    The arrangement of a row is:

    .. math::
         \frac{dx}{d \xi}\; \frac{dx}{d\eta}\; \frac{dy}{d\xi}\; \frac{dy}{d\eta}

    thus the i-th row corresponds to Jacobian matrix at i-th point

    Parameters
    ----------
        x : ndarray
            vector of x-coordinates of element nodes
        y : ndarray
            vector of y-coordinates of element nodes
        dN : ndarray
            3D array of shape function derivatives at Gauss point.
            The shape of this array should be (g, d, n) where:

                * g  is number of Gauss points
                * d  is the dimension of reference element (2)
                * n  is the number of shape functions (4)
    """
    npt = dN.shape[0]
    assert len(x) == 4 and len(y) == 4
    jacobian = np.zeros((npt, 4), dtype=np.float64)
    jacobian[:, 0] = np.dot(dN[:, 0::2], x)
    jacobian[:, 1] = np.dot(dN[:, 1::2], x)
    jacobian[:, 2] = np.dot(dN[:, 0::2], y)
    jacobian[:, 3] = np.dot(dN[:, 1::2], y)
    return jacobian


def calcJacobian2D3D(x, y, z, dN):
    """Calculate jacobian matrix for isoparametric quad element
    with nodal coordinates (x_i, y_i) at specific points (ksi,eta)
    in reference element
    The arrangement of a row is:
    .. math::

        J[i,:] = dx/dksi dx/deta dy/dkis dy/deta dz/dksi dz/deta

    thus the row corresponds to jacobian matrix at i-th point(ksi_i, eta_i)
    """
    npt = dN.shape[0]
    assert len(x) == 4 and len(y) == 4 and len(z) == 4
    jacobian = np.zeros((npt, 6), dtype=np.float64)
    jacobian[:, 0] = np.matmul(dN[:, 0::2], x)
    jacobian[:, 1] = np.matmul(dN[:, 1::2], x)
    jacobian[:, 2] = np.matmul(dN[:, 0::2], y)
    jacobian[:, 3] = np.matmul(dN[:, 1::2], y)
    jacobian[:, 4] = np.matmul(dN[:, 0::2], z)
    jacobian[:, 5] = np.matmul(dN[:, 1::2], z)
    return jacobian


def calcSourceTerm(nodes_coords, ngpt, f=(1.0, 1.0, 1.0, 1.0), dtype=np.float64):
    x = nodes_coords[:, 0]
    y = nodes_coords[:, 1]
    if nodes_coords.shape[1] == 3:
        z = nodes_coords[:, 2]
        return calcSourceTerm2D3D(x, y, z, ngpt, f, dtype)
    else:
        return calcSourceTerm2D2D(x, y, ngpt, f, dtype)


def calcSourceTerm2D3D(x, y, z, ngpt, f=(1.0, 1.0, 1.0, 1.0), dtype=np.float64):
    r"""Calculate vector for the source term,
    that is the term \int f v.

    Arguments:
        f - sequence of f nodal values
    """
    ksi, eta, ww = getQuadrature(ngpt)

    # noinspection PyPep8Naming
    F = np.zeros((4,), dtype=dtype)
    # noinspection PyPep8Naming
    N = calcShapeFun(ksi, eta)
    # Calculate interpolation of f on Gauss points
    if len(f) == 4:
        interpolated_f = np.matmul(N, f)
    elif len(f) == 1:
        interpolated_f = np.ones((len(ksi), 1), dtype=dtype)*f
    else:
        raise IndexError(
            "f should be a sequence of 1 or 4 values; got %d" % len(f))
    # noinspection PyPep8Naming
    dN = calcShapeDerivFun(ksi, eta)
    jacobian = calcJacobian2D3D(x, y, z, dN)
    for gp in range(len(ww)):
        jmat = jacobian[gp, :].reshape(3, 2)
        volume_form = math.sqrt(np.linalg.det(np.dot(jmat.T, jmat)))
        F += N[gp, :]*interpolated_f[gp]*volume_form*ww[gp]
    return F


def calcSourceTerm2D2D(x, y, ngpt, f=(1.0, 1.0, 1.0, 1.0), dtype=np.float64):
    r"""Calculate vector for the source term,
    that is the term \int f v.

    Arguments:
        f - sequence of f nodal values
    """
    ksi, eta, ww = getQuadrature(ngpt)

    # noinspection PyPep8Naming
    F = np.zeros((4,), dtype=dtype)
    # noinspection PyPep8Naming
    N = calcShapeFun(ksi, eta)
    # Calculate interpolation of f on Gauss points
    if len(f) == 4:
        interpolated_f = np.matmul(N, f)
    elif len(f) == 1:
        interpolated_f = np.ones((len(ksi), 1), dtype=dtype)*f
    else:
        raise IndexError(
                "f should be a sequence of 1 or 4 values; got %d" % len(f))
    # noinspection PyPep8Naming
    dN = calcShapeDerivFun(ksi, eta)
    jacobian = calcJacobian2D2D(x, y, dN)
    for gp in range(len(ww)):
        jmat = jacobian[gp, :].reshape(2, 2)
        det_jmat = np.linalg.det(jmat)
        F += N[gp, :]*interpolated_f[gp]*det_jmat*ww[gp]
    return F


def calcMassTerm(nodes_coords, ngpt, dtype=np.float64):
    x = nodes_coords[:, 0]
    y = nodes_coords[:, 1]
    if nodes_coords.shape[1] == 3:
        z = nodes_coords[:, 2]
        return calcMassTerm2D3D(x, y, z, ngpt, dtype)
    else:
        return calcMassTerm2D2D(x, y, ngpt, dtype)


def calcMassTerm2D3D(x, y, z, ngpt, dtype=np.float64):
    r"""Calculate the term \int N_i N_j
    """
    ksi, eta, ww = getQuadrature(ngpt)

    mass_mat = np.zeros((4, 4), dtype=dtype)
    # noinspection PyPep8Naming
    N = calcShapeFun(ksi, eta)
    # noinspection PyPep8Naming
    dN = calcShapeDerivFun(ksi, eta)
    jacobian = calcJacobian2D3D(x, y, z, dN)
    for gp in range(len(ww)):
        jmat = jacobian[gp, :].reshape(3, 2)
        volume_form = math.sqrt(np.linalg.det(np.dot(jmat.T, jmat)))
        mass_mat += np.outer(N[gp, :], N[gp, :])*volume_form*ww[gp]
    return mass_mat


def calcMassTerm2D2D(x, y, ngpt, dtype=np.float64):
    r"""Calculate the term \int N_i N_j
    """
    ksi, eta, ww = getQuadrature(ngpt)

    mass_mat = np.zeros((4, 4), dtype=dtype)
    # noinspection PyPep8Naming
    N = calcShapeFun(ksi, eta)
    # noinspection PyPep8Naming
    dN = calcShapeDerivFun(ksi, eta)
    jacobian = calcJacobian2D2D(x, y, dN)
    for gp in range(len(ww)):
        jmat = jacobian[gp, :].reshape(2, 2)
        det_jmat = np.linalg.det(jmat)
        mass_mat += np.outer(N[gp, :], N[gp, :])*det_jmat*ww[gp]
    return mass_mat


def calcEllipticTerm(nodes_coords, ngpt, constitutive_mat=None, dtype=np.float64):
    x = nodes_coords[:, 0]
    y = nodes_coords[:, 1]
    if nodes_coords.shape[1] == 3:
        z = nodes_coords[:, 2]
        return calcEllipticTerm2D3D(x, y, z, ngpt, constitutive_mat, dtype)
    else:
        return calcEllipticTerm2D2D(x, y, ngpt, constitutive_mat, dtype)


def calcEllipticTerm2D3D(x, y, z, ngpt, constitutive_mat=None, dtype=np.float64):
    """Calculate stiffness matrix for elliptic term:
       grad (constitutive_mat grad u)
    """
    ksi, eta, ww = getQuadrature(ngpt)
    stiffness_mat = np.zeros((4, 4), dtype=dtype)
    # noinspection PyPep8Naming
    dN = calcShapeDerivFun(ksi, eta)
    jacobian = calcJacobian2D3D(x, y, z, dN)

    D_inv = None
    if constitutive_mat is not None:
        D_inv = np.linalg.inv(constitutive_mat)

    for gp in range(len(ww)):
        jmat = jacobian[gp, :].reshape(3, 2)
        if D_inv is not None:
            jjt = np.linalg.inv(np.linalg.multi_dot([jmat.T, D_inv, jmat]))
        else:
            jjt = np.linalg.inv(np.matmul(jmat.T, jmat))
        volume_form = math.sqrt(np.linalg.det(np.dot(jmat.T, jmat)))
        derivs_mat = dN[gp, :].reshape(4, 2)
        stiffness_mat += np.linalg.multi_dot([derivs_mat, jjt, derivs_mat.T]) * volume_form * ww[gp]
    return stiffness_mat


def calcEllipticTerm2D2D(x, y, ngpt, constitutive_mat=None, dtype=np.float64):
    """Calculate stiffness matrix for elliptic term:
       grad (constitutive_mat grad u)
    """
    ksi, eta, ww = getQuadrature(ngpt)

    stiffness_mat = np.zeros((4, 4), dtype=dtype)
    # noinspection PyPep8Naming
    dN = calcShapeDerivFun(ksi, eta)
    jacobian = calcJacobian2D2D(x, y, dN)
    for gp in range(len(ww)):
        jmat = jacobian[gp, :].reshape(2, 2)
        jmat_inv = np.linalg.inv(jmat)
        det_jmat = np.linalg.det(jmat)
        # noinspection PyPep8Naming
        BT = np.matmul(dN[gp, :].reshape(4, 2), jmat_inv)
        if constitutive_mat is None:
            stiffness_mat += np.linalg.multi_dot([BT, BT.transpose()]) * det_jmat * ww[gp]
        else:
            stiffness_mat += np.linalg.multi_dot([BT, constitutive_mat, BT.transpose()]) * det_jmat * ww[gp]
    return stiffness_mat


def getQuadrature(ngpt):
    """Return vectors of ksi,eta,ww being respectively
    Gauss point nodes (kis,eta) and weights of the 2D
    quadrature being the product of 1D quadratures with ngpt points each
    """
    ngpt = math.ceil(math.sqrt(ngpt))
    xi, w = np.polynomial.legendre.leggauss(ngpt)
    ij = np.array(list(itertools.product(range(ngpt), range(ngpt))))

    ksi = xi[ij[:, 0]]
    eta = xi[ij[:, 1]]
    ww = w[ij[:, 0]]*w[ij[:, 1]]
    return ksi, eta, ww


# Aliases for the function defined above.
# Here we can use our C++ names

calcMeasure = calcArea
