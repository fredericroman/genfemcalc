FEMCALC_CONFIG_DIR=$(shell pwd)/config

include config/Makefile.mk

all: 
	@echo $(FEMCALC_CONFIG_DIR)
	@echo "Targets are: testall(aka: alltest) allexamples"

alltest: testall

alltests: testall

testall:
	@echo "Run all tests"
	$(PYTHON) -m unittest discover

list: 
	cd examples && make list

allexamples:
	cd examples && make allexamples
