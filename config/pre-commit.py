#!/usr/bin/env python
# -*- coding: utf-8 -*-
""""
This script illustrates command line handling
"""

import argparse
from datetime import datetime
import os
from packaging import version
import re
import shutil
import sys

def make_parser():
    """Build command line parser"""
    parser = argparse.ArgumentParser(description='Sample CLI application')
    return parser


def set_parser_arguments(parser):
    """Add specific command line arguments to the parser"""
    parser.add_argument('filename')


def run():
    """ Main function - parser command line and prints some messages"""
    parser = make_parser()
    set_parser_arguments(parser)
    args = parser.parse_args()

    file = args.filename
    if not os.path.exists(file):
        print(f"ERROR: file {file} does not exist")
        sys.exit(22)
    tmp_file = file + '_hook_copy'
    shutil.copy(file, tmp_file)
    with open(file, 'r') as content_file:
        content = content_file.read()
        content = re.sub("__date__[ ]?=[ ]?'.*'",
                         f"__date__ = '{datetime.now()}'", content)
        v_pattern = "__version__[ ]?=[ ]?'(.*)'"
        m = re.search(v_pattern, content)
        if m:
            vstr = version.parse(m.group(1))
            release = vstr.release
            vstr = "__version__ = '%d.%d.%d'" % (release[0], release[1]+1, 0)
            content = re.sub(v_pattern, vstr, content)
        content_file.close()
        with open(file, 'w') as out_file:
            out_file.write(content)
            out_file.close()
    os.remove(tmp_file)


if __name__ == '__main__':
    run()
