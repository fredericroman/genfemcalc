genfemcalc.meshgrid package
===========================

.. automodule:: genfemcalc.meshgrid
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::

   genfemcalc.meshgrid.graphics

Submodules
----------

.. toctree::

   genfemcalc.meshgrid._meshmrg
   genfemcalc.meshgrid.adjacency
   genfemcalc.meshgrid.advancedmrg
   genfemcalc.meshgrid.cellsgmsh
   genfemcalc.meshgrid.geometrymrg
   genfemcalc.meshgrid.iomrg
   genfemcalc.meshgrid.modelsgmsh
   genfemcalc.meshgrid.modelsmrg
   genfemcalc.meshgrid.refcells
   genfemcalc.meshgrid.utilsgmsh
   genfemcalc.meshgrid.utilsshapely
   genfemcalc.meshgrid.wrappers
