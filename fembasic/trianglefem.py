# -*- coding: utf-8 -*-
"""Module for calculating matrices and vectors on triangle element.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/5/2019'
__version__ = '0.1.0'

import numpy as np

from genfemcalc import fem
from genfemcalc import meshgrid

def calcMassTerm(nodes, dtype=np.float64):
    cell = meshgrid.refcells.getRefCell('vtk', meshgrid.ElemType.TRIANGLE)
    mapper = fem.manifolds.GeomMapper(cell, ambient_dim=3)
    quadrature = fem.quadratures.Quadrature(cell, ngpt=4)
    mapper.setRefPoints(quadrature.points)

    mapper.setNodes(nodes)
    N = mapper.refcell.N(quadrature.points)
    mass_matrix = np.einsum('gi,gj,g,g->ij', N, N,
                            mapper.volumeForm,
                            quadrature.weights,
                            dtype=dtype)
    return mass_matrix

