"""
Evaluate solution on a mesh
---------------------------
"""
import os

import genfemcalc
from genfemcalc import benchmarks
from genfemcalc import examples
from genfemcalc import meshgrid

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoSolutionOnMesh(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        model_context = {'xa': 0.0,
                         'xb': 1.0,
                         'ya': 0.0,
                         'yb': 1.0,
                         'edge_size': 0.4}
        rectangle = meshgrid.modelsgmsh.Rectangle()
        domain = rectangle.discretize(model_context)
        mymesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh)
        solution = benchmarks.alpha_one
        solution.addToMeshAs('temperature', mymesh)
        outfile = os.path.join(OUTDIR, 'sol_on_mesh.vtk')
        meshgrid.iomrg.writeVtkFromMrg(mymesh, outfile)


if __name__ == '__main__':
    demo = DemoSolutionOnMesh()
    demo.run()
