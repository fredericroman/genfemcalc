# -*- coding: utf-8 -*-
"""
Show submesh selection
----------------------

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/8/2019'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import benchmarks
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics
import genfemcalc.benchmarks.meshes

PKGDIR = os.path.dirname(os.path.abspath(genfemcalc.__file__))
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoBoundarySubmeshes(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        mesh = benchmarks.meshes.getStructuredMeshInRectangle((5, 5))
        skinner = meshgrid.adjacency.Skinner.fromMesh(mesh)

        boundary_mesh = skinner.buildMesh()

        south_box = [[-0.1, -0.1, 0], [1.1, 0.1, 0]]
        north_box = [[-0.1, 0.9, 0], [1.1, 1.1, 0]]
        west_box = [[-0.1, -0.1, 0], [0.1, 1.1, 0]]

        submesher = meshgrid.adjacency.MeshMaskFilter(boundary_mesh)

        south_mask = meshgrid.adjacency.getMeshMaskFromBox(submesher.mesh, south_box)
        submesher.setMask(south_mask)
        south_mesh = submesher.buildMesh()

        north_mask = meshgrid.adjacency.getMeshMaskFromBox(submesher.mesh, north_box)
        submesher.setMask(north_mask)
        north_mesh = submesher.buildMesh()

        west_mask = meshgrid.adjacency.getMeshMaskFromBox(submesher.mesh, west_box)
        submesher.setMask(west_mask)
        west_mesh = submesher.buildMesh()

        submesher.resetMask(value=False)
        submesher.makeUnionMask(south_mask, north_mask, west_mask)
        submesher.negateMask()
        east_mesh = submesher.buildMesh()

        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerMesh(south_mesh, 'south')
        viewer.registerMesh(east_mesh, 'east')
        viewer.registerMesh(north_mesh, 'north')
        viewer.registerMesh(west_mesh, 'west')
        viewer.show('south', color='red', facecolor='red', linewidth=3)
        viewer.show('east', color='blue', facecolor='blue', linewidth=3)
        viewer.show('north', color='green', facecolor='green', linewidth=3)
        viewer.show('west', color='black', facecolor='black', linewidth=3)
        viewer.keep()


if __name__ == '__main__':
    demo = DemoBoundarySubmeshes()
    demo.run()
