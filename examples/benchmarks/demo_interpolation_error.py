# -*- coding: utf-8 -*-
"""
Interpolation error on single element
-------------------------------------
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/25/2019'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import benchmarks
from genfemcalc import fem

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoInterpolationError(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        xside, yside = 1.0, 1.0
        mesh = benchmarks.meshes.getStructuredMeshInRectangle(resolution=(2, 2),
                                                              xside=xside, yside=yside)
        solution = benchmarks.exact_solutions.ParsedSolution('x+y+x*y')
        solution.addToMeshAs('u', mesh)
        integrator = fem.meshcalc.MeshIntegrator(mesh, ngpt=4)
        error = benchmarks.errors.computeL2ErrorVersusExact(integrator, 'u', solution)
        print(f"L2 interpolation error: {error}")


if __name__ == '__main__':
    demo = DemoInterpolationError()
    demo.run()
