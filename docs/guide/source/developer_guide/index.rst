.. include:: ../../replaces.txt
.. _developer_guide-part:

#########################
Genfemcalc Developers' Guide
#########################

*************************************
Advanced topics in Python programming
*************************************


**********************
Unit tests for Genfemcalc
**********************

***************************
Adding new geometric models
***************************

**************************
Adding new reference cells
**************************

*****************************************
Wrapping third party mesh data structures
*****************************************

*******************************
Programming with VTK Python API
*******************************

*********************************
Writing documentation for Genfemcalc
*********************************

**************************
Buildserver infractructure
**************************
