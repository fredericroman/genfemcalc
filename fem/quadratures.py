"""Provide quadrature and quadrature factory
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.13 11:19:5'
__version__ = '0.2.0'

import numpy as np

from genfemcalc import fem
from genfemcalc import meshgrid


class QuadratureRegistry(object):
    """Holds map from reference cell type to quadrature over the cell
    
    The role of his object is too keep association between reference cell
    type and the quadrature to integrate over the cell.
    """
    __valid_shapes = meshgrid.cellsgmsh.getCellsShapes()

    def __init__(self):
        self.quadratures = dict()

    def add(self, quadrature):
        self.quadratures[quadrature.cell_type] = quadrature

    def setForShape(self, shape, ngpt):
        if shape not in self.__valid_shapes:
            raise KeyError(f"Invalid cell shape: {shape}. Expected one of {self.__valid_shapes}")
        for cell_type in meshgrid.cellsgmsh.filterCellsInfo({'shape': shape}):
            self.setForRefcell(meshgrid.refcells.getRefCell('gmsh', cell_type), ngpt)

    def setForCellType(self, family, cell_type, ngpt):
        type_translator = meshgrid.refcells.CellIdTranslator(family, 'gmsh')
        gmsh_type = type_translator(cell_type)
        return self.setForRefcell(meshgrid.refcells.getRefCell('gmsh', gmsh_type), ngpt)

    def setForRefcell(self, refcell, ngpt):
        quadrature = fem.quadratures.Quadrature(refcell, ngpt)
        self.quadratures[refcell.type] = quadrature
        return quadrature

    def __getitem__(self, cell_type):
        return self.quadratures[cell_type]

    def get(self, cell_type):
        return self.quadratures.get(cell_type)


class Quadrature(object):
    """Holds Gauss points and their weight plus information on which kind
    of cell it is applicable
    """

    def __init__(self, ref_cell, ngpt):
        self._points, self._weights = getQuadratureData(ref_cell, ngpt)
        self._cell_type = ref_cell.type
        self._cell_family = ref_cell.family

    @property
    def cell_type(self):
        return self._cell_type

    @property
    def cell_family(self):
        return self._cell_family

    @property
    def points(self):
        return self._points

    @property
    def numb_points(self):
        return len(self._weights)

    @property
    def weights(self):
        return self._weights


def getQuadratureData(ref_cell, ngpt):
    """Retrun points and weights for quadrature specific to reference cell shape

    Parameters
    ----------
    ref_cell : femcal.meshgrid.refcells.RefCell
        Reference cell object
    ngpt : int or int sequence
        Number of Gauss points. One can give single number for all directions
        or sequence for respective directions. The length of the sequence must
        be equal the dimension of reference cell. CAUTION: for triangle and
        tetrahedron shapes only single number (or one-sequence)  is accepted.
    Returns
    -------
    points : ndarray
        Array of Gauss points
    weights : ndarray
        Array of weights
    """
    builders = {'line': getQuadratureForLine,
                'quadrangle': getQuadratureForQuadrilateral,
                'triangle': getQuadratureForTriangle,
                'hexahedron': getQuadratureForHexahedron,
                'tetrahedron': getQuadratureForTetrahedron}
    build_func = builders.get(ref_cell.shape)
    if build_func is None:
        raise ValueError(f"Unsupported cell shape: {ref_cell.shape}")
    return build_func(ngpt)


def getQuadratureForLine(ngpt):
    num_points = np.asarray(ngpt).reshape(1)
    if num_points.shape[0] not in (1,):
        raise ValueError("Number of Gauss point should be scalar or one-sequence")
    points, weights = np.polynomial.legendre.leggauss(num_points[0])
    points = (points.reshape(points.size, 1)+1.0)/2.0
    weights = weights/2.0
    return points, weights


def getQuadratureForTriangle(ngpt):
    if ngpt == 1:
        points = np.array([1.0/3, 1.0/3])
        points.reshape(1, 2)
        weights = np.array([1.0/2])
    elif ngpt == 3:
        points = np.array([[1.0/6, 1.0/6],
                           [2.0/3, 1.0/6],
                           [1.0/6, 2.0/3]])
        weights = np.array([1, 1, 1])/6.0
    elif ngpt == 4:
        points = np.array([[1.0/3, 1.0/3],
                           [1.0/5, 1.0/5],
                           [3.0/5, 1.0/5],
                           [1.0/5, 3.0/5]])
        weights = np.array([-27, 25, 25, 25]) / 96.0
    elif ngpt == 6:
        a = 0.445948490915965
        b = 0.091576213509771
        c = 0.111690794839005
        d = 0.054975871827661
        points = np.array([[a, a],
                           [1 - 2 * a, a],
                           [a, 1 - 2 * a],
                           [b, b],
                           [1 - 2 * b, b],
                           [b, 1 - 2 * b]], dtype='float64')
        weights = np.array([c, c, c, d, d, d], dtype='float64')
    else:
        raise NotImplementedError(('Sorry for triangles only 1,2,4,6 '
                                   'point quadratures are implemented'))
    return points, weights


def getQuadratureForQuadrilateral(ngpt):
    num_points = np.asarray(ngpt).reshape(1)
    if num_points.shape[0] not in (1, 2):
        raise ValueError('Number of Gauss point should be scalar or two-sequence')
    if num_points.shape[0] == 1:
        num_points[0] = np.ceil(np.sqrt(num_points))
    num_points = np.resize(num_points, (2,))
    x, wx = np.polynomial.legendre.leggauss(num_points[0])
    x = (x+1.0)/2.0
    wx = wx/2.0
    y, wy = x, wx
    if num_points[0] != num_points[1]:
        y, wy = np.polynomial.legendre.leggauss(num_points[1])
        y = (y+1.0)/2.0
        wy = wy/2.0

    # Array indices of all gauss points
    i_x = np.tile(np.arange(num_points[0]), num_points[1])
    j_y = np.repeat(np.arange(num_points[1]), num_points[0])

    points = np.array([x[i_x], y[j_y]]).T
    weights = wx[i_x]*wy[j_y]
    return points, weights


def getQuadratureForHexahedron(ngpt):
    num_points = np.asarray(ngpt).reshape(1)
    if num_points.shape[0] not in (1, 3):
        raise ValueError('Number of Gauss point should be scalar or three-sequence')
    if num_points.shape[0] == 1:
        num_points[0] = np.ceil(np.sqrt(num_points))
    num_points = np.resize(num_points, (3,))
    x, wx = np.polynomial.legendre.leggauss(num_points[0])
    x = (x+1.0)/2.0
    wx = wx/2.0
    y, wy = x, wx
    z, wz = x, wx
    if num_points[1] != num_points[0]:
        y, wy = np.polynomial.legendre.leggauss(num_points[1])
        y = (y+1.0)/2.0
        wy = wy/2.0
    if num_points[2] != num_points[1]:
        z, wz = np.polynomial.legendre.leggauss(num_points[2])
        z = (z+1.0)/2.0
        wz = wz/2.0

    # Array indices of all gauss points
    i_x = np.tile(np.arange(num_points[0]), num_points[1]*num_points[2])
    j_y = np.repeat(np.arange(num_points[1]), num_points[0]*num_points[2])
    k_z = np.repeat(np.arange(num_points[2]), num_points[0]*num_points[1])

    points = np.array([x[i_x], y[j_y], z[k_z]]).T
    weights = wx[i_x] * wy[j_y] * wz[k_z]
    return points, weights


def getQuadratureForTetrahedron(ngpt):
    raise NotImplementedError(f"Sorry, have to wait: call RP {ngpt}")
