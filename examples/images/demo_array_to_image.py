# -*- coding: utf-8 -*-
"""
Illustrate how to convert array to image
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/27/2019'
__version__ = '0.1.0'

import matplotlib.pyplot as plt
import matplotlib.cm
import matplotlib.colors
import numpy
import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
import meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoArrayToImage(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        array = numpy.zeros((20, 20), dtype=numpy.float)
        for i in range(0, 20):
            array[i, i] = 2.0
        for i in range(0, 17):
            array[i, i+3] = -1.0

        file_path = os.path.join(OUTDIR, 'two_lines.png')
        image = meshgrid.graphics.images.writeArray(file_path, array)

        file_path = os.path.join(OUTDIR, 'two_lines_bw.png')
        norm = matplotlib.colors.Normalize(0.0, 1.0, clip=True)
        cmap = matplotlib.cm.get_cmap('binary')
        image = meshgrid.graphics.images.writeArray(file_path, numpy.abs(array), cmap=cmap,
                                                    norm=norm)
        fig, ax = plt.subplots()
        ax.imshow(image)
        if self.show_images:
            plt.show()

if __name__ == '__main__':
    demo = DemoArrayToImage()
    demo.run()
