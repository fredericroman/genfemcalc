"""Unittest for element calculations
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.08.22  9:8:43'
__version__ = '0.1.0'

import unittest
import numpy as np

from fem import manifolds
from fem import quadratures
from fem import elemcalc
from fembasic import quadfem
from meshgrid import refcells


class TestQuadCalculation(unittest.TestCase):
    def setUp(self):
        self.cell = refcells.getRefCell('gmsh', 3)
        self.mapper22 = manifolds.GeomMapper(self.cell, 2)
        self.mapper23 = manifolds.GeomMapper(self.cell, 3)
        self.ngpt = 2
        self.quadrature = quadratures.Quadrature(self.cell, self.ngpt)
        self.nodes = np.array([[1, 1, 0], [3, 1, 0], [4, 3, 0], [0, 2, 0]], dtype='float64')

        self.mapper22.precomputeForQuadrature(self.quadrature)
        self.mapper22.setNodes(self.nodes)
        self.mapper23.precomputeForQuadrature(self.quadrature)
        self.mapper23.setNodes(self.nodes)

    def get_load_vector(self, mapper):
        load_vec = [1.0, 2.0, 3.0, 4.0]
        load_elemcalc = elemcalc.calcSourceTerm(mapper, load_vec)
        load_quadfem = quadfem.calcSourceTerm(self.nodes[:, 0:2], self.ngpt, load_vec)
        return load_elemcalc, load_quadfem

    def get_mass_matrices(self, mapper):
        mass_elemcalc = elemcalc.calcMassTerm(mapper)
        mass_quadfem = quadfem.calcMassTerm(self.nodes[:, 0:2], self.ngpt)
        return mass_elemcalc, mass_quadfem

    def get_stiffness_matrices(self, mapper):
        material_matrix = np.array([[2.0, 3.0], [4.0, 5.0]])
        stiffness_elemcalc = elemcalc.calcEllipticTerm(mapper, material_matrix)
        stiffness_quadfem = quadfem.calcEllipticTerm(self.nodes[:,0:2], self.ngpt, material_matrix)
        return stiffness_elemcalc, stiffness_quadfem

    def test_mass_term_22(self):
        mat_elemcalc, mat_quadfem = self.get_mass_matrices(self.mapper23)
        self.assertTrue(np.allclose(mat_elemcalc, mat_quadfem),
                        "Matrices from elemcalc and quadfem differ")

    def test_mass_term_23(self):
        mat_elemcalc, mat_quadfem = self.get_mass_matrices(self.mapper23)
        self.assertTrue(np.allclose(mat_elemcalc, mat_quadfem),
                        "Matrices from elemcalc and quadfem differ")

    def test_elliptic_term_22(self):
        mat_elemcalc, mat_quadfem = self.get_stiffness_matrices(self.mapper22)
        self.assertTrue(np.allclose(mat_elemcalc, mat_quadfem),
                        "Matrices from elemcalc and quadfem differ")

    def test_elliptic_term_23(self):
        mat_elemcalc, mat_quadfem = self.get_stiffness_matrices(self.mapper22)
        self.assertTrue(np.allclose(mat_elemcalc, mat_quadfem),
                        "Matrices from elemcalc and quadfem differ")

    def test_source_term_22(self):
        vec_elemcalc, vec_quadfem = self.get_load_vector(self.mapper22)
        self.assertTrue(np.allclose(vec_elemcalc, vec_quadfem),
                        "Matrices from elemcalc and quadfem differ")

    def test_source_term_23(self):
        vec_elemcalc, vec_quadfem = self.get_load_vector(self.mapper22)
        self.assertTrue(np.allclose(vec_elemcalc, vec_quadfem),
                        "Matrices from elemcalc and quadfem differ")
