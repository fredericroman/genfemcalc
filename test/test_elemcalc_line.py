"""Unittest for element calculations
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.08.32  9:8:43'
__version__ = '0.1.0'

import unittest
import numpy as np

from fem import manifolds
from fem import quadratures
from fem import elemcalc
from fembasic import linefem
from meshgrid import refcells

class TestQuadCalculation(unittest.TestCase):
    def setUp(self):
        self.cell = refcells.getRefCell('gmsh', 1)
        self.mapper12 = manifolds.GeomMapper(self.cell, 2)
        self.mapper13 = manifolds.GeomMapper(self.cell, 3)
        self.ngpt = 2
        self.quadrature = quadratures.Quadrature(self.cell, self.ngpt)
        self.nodes = np.array([[1, 1, 0], [4, 5, 0]], dtype='float64')

        self.mapper12.precomputeForQuadrature(self.quadrature)
        self.mapper12.setNodes(self.nodes)
        self.mapper13.precomputeForQuadrature(self.quadrature)
        self.mapper13.setNodes(self.nodes)

    def get_load_vector(self, mapper):
        load_vec = [2.0, 2.0]
        load_elemcalc = elemcalc.calcSourceTerm(mapper, load_vec)
        load_linefem = linefem.calcSourceTerm(self.nodes, load_vec)
        return load_elemcalc, load_linefem

    def get_mass_matrices(self, mapper):
        mass_elemcalc = elemcalc.calcMassTerm(mapper)
        mass_linefem = linefem.calcMassTerm(self.nodes)
        return mass_elemcalc, mass_linefem

    def get_stiffness_matrices(self, mapper):
        stiffness_elemcalc = elemcalc.calcEllipticTerm(mapper)
        stiffness_linefem = linefem.calcEllipticTerm(self.nodes)
        return stiffness_elemcalc, stiffness_linefem

    def test_mass_term_12(self):
        mat_elemcalc, mat_linefem = self.get_mass_matrices(self.mapper13)
        self.assertTrue(np.allclose(mat_elemcalc, mat_linefem),
                        "Matrices from elemcalc and linefem differ")

    def test_mass_term_13(self):
        mat_elemcalc, mat_linefem = self.get_mass_matrices(self.mapper13)
        self.assertTrue(np.allclose(mat_elemcalc, mat_linefem),
                        "Matrices from elemcalc and linefem differ")

    def test_elliptic_term_12(self):
        mat_elemcalc, mat_linefem = self.get_stiffness_matrices(self.mapper12)
        self.assertTrue(np.allclose(mat_elemcalc, mat_linefem),
                        "Matrices from elemcalc and linefem differ")

    def test_elliptic_term_13(self):
        mat_elemcalc, mat_linefem = self.get_stiffness_matrices(self.mapper13)
        self.assertTrue(np.allclose(mat_elemcalc, mat_linefem),
                        "Matrices from elemcalc and linefem differ")

    def test_source_term_12(self):
        vec_elemcalc, vec_linefem = self.get_load_vector(self.mapper12)
        self.assertTrue(np.allclose(vec_elemcalc, vec_linefem),
                        "Matrices from elemcalc and linefem differ")

    def test_source_term_13(self):
        vec_elemcalc, vec_linefem = self.get_load_vector(self.mapper13)
        self.assertTrue(np.allclose(vec_elemcalc, vec_linefem),
                        "Matrices from elemcalc and linefem differ")
