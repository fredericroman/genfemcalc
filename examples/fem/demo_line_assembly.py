# -*- coding: utf-8 -*-
"""
Assembly on 1D mesh
===================

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.04  19:13:54'
__version__ = '0.1.0'

import numpy as np
import os

# MRG modules
import genfemcalc
from genfemcalc import meshgrid
from genfemcalc import fem
from genfemcalc import examples

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class ElementEvaluator(object):
    def __init__(self, source_term):
        self._source_term = source_term.reshape((-1,))

    def __call__(self, mesh, elem, edofs, dtype=np.float64):
        nodes = mesh.getCellNodes(elem)
        points = mesh.node_coords[nodes, :]
        if mesh.elem_geotype[elem] == meshgrid.ElemType.LINE:
            lhs = genfemcalc.fembasic.linefem.calcEllipticTerm(points, dtype=dtype)
            f = self._source_term[nodes]
            rhs = genfemcalc.fembasic.linefem.calcSourceTerm(points, f, dtype=dtype)
            return lhs, rhs
        else:
            return None


class DemoAssemblyOnSegment(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        model = meshgrid.modelsgmsh.Segment()
        domain = model.discretize(xa=0, xb=5, ya=0, yb=0, tsdensity=[5])
        mymesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh)
        dtype = np.complex128
        assembler = fem.assembly.MrgAssembler(mymesh, dtype=dtype)
        source_term = 2 * np.ones((mymesh.numb_nodes,), dtype=dtype) + complex(0, 1)
        evaluator = ElementEvaluator(source_term)
        assembler.preassemble(matrix_names=['K'],
                              vector_names=['F'],
                              element_routine=evaluator)

        rhs = assembler.assemble('F', 'dense')
        print('--------')
        print(rhs)
        print('--------')


if __name__ == '__main__':
    demo = DemoAssemblyOnSegment()
    demo.run()
