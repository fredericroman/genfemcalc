# -*- coding: utf-8 -*-
"""
Show cells adjacency graph
--------------------------

"""
__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/8/2019'
__version__ = '0.1.0'

import os
import graphviz
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
from genfemcalc import benchmarks

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoShowCellsAdjacency(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        mesh = benchmarks.meshes.getStructuredMeshInRectangle((4, 4))
        structure = meshgrid.adjacency.DynamicAdjacency.fromMesh(mesh)
        dot = graphviz.Digraph(engine='neato', comment='Cells neighbourhood')

        meshgeom = meshgrid.geometrymrg.MeshGeometry.fromMrgMesh(mesh)

        dot.attr('node', shape='box')
        for cell in structure:
            pos = meshgeom.getCellApproxBarycenter(cell.type)
            x, y = pos[0:2]
            dot.node(f"{cell.type}", pos="%f,%f!" % (8 * x, 8 * y))
        for cell in structure:
            for c in cell.neighbours:
                dot.edge(f"{cell.type}", f"{c.type}")
        filepath = os.path.join(OUTDIR, 'mesh_structure')
        dot.format = 'png'
        dot.render(filepath,  view=self.show_images)
        img = mpimg.imread(filepath + '.' + dot.format)
        if self.show_images: 
            plt.imshow(img)
            plt.show()


if __name__ == '__main__':
    demo = DemoShowCellsAdjacency()
    demo.run()
