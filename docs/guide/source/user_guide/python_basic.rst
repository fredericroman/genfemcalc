.. include:: ../../replaces.txt
.. _python_basic-sec:

****************************
Topics in Python programming
****************************

Built-in data structures
-------------------------

Using iterators and generators
------------------------------

Handling matrices with `numpy`
------------------------------

Handling sparse matrices with `scipy.sparse`
--------------------------------------------