genfemcalc.fembasic package
===========================

.. automodule:: genfemcalc.fembasic
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::

   genfemcalc.fembasic.hexfem
   genfemcalc.fembasic.linefem
   genfemcalc.fembasic.quadfem
   genfemcalc.fembasic.trianglefem
