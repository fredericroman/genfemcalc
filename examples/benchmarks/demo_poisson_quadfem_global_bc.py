"""
Poisson routine for quad element
--------------------------------
"""
import os
import numpy
import scipy
import scipy.sparse.linalg

from genfemcalc import benchmarks
from genfemcalc import examples
from genfemcalc import fem
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class PoissonElementRoutineForQuadfem(object):
    def __init__(self, what, source_term=None):
        self.source_term = source_term
        self.what = what

    def __call__(self, mesh, elem, edofs, dtype):
        if mesh.elem_geotype[elem] == meshgrid.ElemType.QUAD:
            dim = 3  # put 2 for 2D -> 2D
            nodes = mesh.node_coords[edofs, 0:dim]
            D = numpy.identity(dim, dtype=dtype)
            if self.what == 'K':
                K = genfemcalc.fembasic.quadfem.calcEllipticTerm(nodes, ngpt=4, constitutive_mat=D, dtype=dtype)
                return [K]
            elif self.what == 'F':
                if self.source_term is not None:
                    f = self.source_term[edofs]
                else:
                    f = [0]*len(edofs)
                F = genfemcalc.fembasic.quadfem.calcSourceTerm(nodes, ngpt=4, f=f, dtype=dtype)
                return [F]
            elif self.what == 'M':
                M = genfemcalc.fembasic.quadfem.calcMassTerm(nodes, ngpt=4, dtype=dtype)
                return [M]
            else:
                raise ValueError(f"*** ERROR invalid term to compute {self.what}")
        else:
            return None


class DemoPoissonProblemQuadfemBC(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        solution = benchmarks.exact_solutions.alpha_one
        problem = benchmarks.problems.PoissonInRectangle(solution)
        viewer = meshgrid.graphics.MeshViewer()

        domain = problem.getDomain(edge_size=0.5, quadsonly=True)

        mrgmesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh)

        # build boundary condition map for Dirichlet boundary conditions
        # this map maps nodes (dofs)  to their values.
        bc_map = dict()
        for region_name in ['b_east', 'b_north', 'b_west']:
            ids = meshgrid.utilsgmsh.getRegionsNodesIds(domain.mesh, names=(region_name,))
            points = mrgmesh.node_coords[ids, :]
            values = problem.evalBoundaryTermOn('dirichlet', region_name, points)
            bc_map.update(dict(zip(ids, values)))

        source_term = problem.evalSourceTermOn(mrgmesh.node_coords)

        routine_for_k = PoissonElementRoutineForQuadfem('K')
        routine_for_f = PoissonElementRoutineForQuadfem('F', source_term)

        assembler = fem.assembly.MrgAssembler(mrgmesh)
        assembler.preassemble(['K'], [], routine_for_k)
        assembler.preassemble([], ['F'], routine_for_f)
        storage_type = 'dense'
        K = assembler.assemble('K', storage_type)
        F = assembler.assemble('F', storage_type)

        K, F = fem.assembly.applyDirichletBC(storage_type, K, F, bc_map)
        if storage_type == 'dense':
            x = scipy.linalg.solve(K, F)
        else:
            x = scipy.sparse.linalg.spsolve(K, F)

        mrgmesh.addNodeField('solution', x)
        filename = os.path.join(OUTDIR, 'poisson_solution_quadfem_global_bc.vtk')
        meshgrid.iomrg.writeVtkFromMrg(mrgmesh, filename)


if __name__ == '__main__':
    demo = DemoPoissonProblemQuadfemBC()
    demo.demonstrate()
