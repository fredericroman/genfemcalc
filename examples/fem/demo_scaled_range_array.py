# -*- coding: utf-8 -*-
"""
Scaled range array
==================

TODO : Add description of demo demo_scaled_range_array.py
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '11/10/2019'
__version__ = '0.1.0'

import os
import numpy as np

import genfemcalc
from genfemcalc import examples
from genfemcalc import fem


PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoScaledRangeArray(examples.Demo):
    def demonstrate(self):
        a = np.ones((10,), dtype='float64')
        borders = np.array([0, 3, 5, 10])
        factors = np.array([2, 6, 7])
        sra = fem.assembly.ScaledRangeArray(a, borders=borders, factors=factors)
        print('sra[1], sra[6]', sra[1], sra[6])
        print('sra[0,6]', sra[0:6])


if __name__ == '__main__':
    demo = DemoScaledRangeArray()
    demo.run()
