"""
Test cellApproxBBox routine
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '11/18/2019'
__version__ = '0.1.0'

import unittest
import os
import numpy

import genfemcalc
from genfemcalc import meshgrid

PKGDIR = os.path.dirname(os.path.abspath(genfemcalc.__file__))
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class TestCellApproxBBox(unittest.TestCase):
    def setUp(self):
        filepath = os.path.join(DATADIR, 'quad_triangle_line.vtk')
        self.mesh = meshgrid.iomrg.readMrgFromVtk(filepath)

    def test_something(self):
        bbox = meshgrid.geometrymrg.cellApproxBBox(self.mesh, 3)
        expected = numpy.array([1.0, 0.5, 0.0, 1.5, 1.0, 0.0])
        self.assertTrue(numpy.allclose(bbox, expected), 'Invalid bounding box for 3')
        bbox = meshgrid.geometrymrg.cellApproxBBox(self.mesh, 5)
        expected = numpy.array([1.5, 0.0, 0.0, 4.0, 1.5, 0.0])
        self.assertTrue(numpy.allclose(bbox, expected), 'Invalid bounding box for 5')
