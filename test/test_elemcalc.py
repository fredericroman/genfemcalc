"""Unittest for element calculations.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.08.22  9:8:43'
__version__ = '0.1.0'

import unittest
import numpy as np

from fem import manifolds
from fem import quadratures
from fem import elemcalc
from meshgrid import refcells


class TestQuadComplexCalculation(unittest.TestCase):
    def setUp(self):
        self.cell = refcells.getRefCell('gmsh', 3)
        self.mapper22 = manifolds.GeomMapper(self.cell, 2)
        self.mapper23 = manifolds.GeomMapper(self.cell, 3)
        self.ngpt = 2
        self.quadrature = quadratures.Quadrature(self.cell, self.ngpt)
        self.nodes = np.array([[1, 1, 0], [3, 1, 0], [4, 3, 0], [0, 2, 0]], dtype='float64')

        self.mapper22.precomputeForQuadrature(self.quadrature)
        self.mapper22.setNodes(self.nodes)
        self.mapper23.precomputeForQuadrature(self.quadrature)
        self.mapper23.setNodes(self.nodes)

    @staticmethod
    def get_load_vector(mapper, dtype):
        load_vec = [1.0, 2.0, 3.0, 4.0]
        load_elemcalc = elemcalc.calcSourceTerm(mapper, load_vec, dtype)
        return load_elemcalc

    @staticmethod
    def get_mass_matrix(mapper, dtype):
        mass_elemcalc = elemcalc.calcMassTerm(mapper, dtype)
        return mass_elemcalc

    @staticmethod
    def get_stiffness_matrix(mapper, dtype):
        material_matrix = np.array([[2.0, 3.0], [4.0, 5.0]])
        stiffness_elemcalc = elemcalc.calcEllipticTerm(mapper, material_matrix, dtype)
        return stiffness_elemcalc

    def test_mass_term_23(self):
        mat_elemcalc_real = self.get_mass_matrix(self.mapper23, np.float64)
        mat_elemcalc_complex = self.get_mass_matrix(self.mapper23, np.complex128)
        self.assertTrue(np.allclose(mat_elemcalc_real, np.real(mat_elemcalc_complex)),
                        "Matrices for real and complex calculation differ")
        self.assertTrue(np.allclose(0.0, np.imag(mat_elemcalc_complex)),
                        "Imaginary part of matrix not 0.0 as expected")

    def test_mass_term_22(self):
        mat_elemcalc_real = self.get_mass_matrix(self.mapper22, np.float64)
        mat_elemcalc_complex = self.get_mass_matrix(self.mapper22, np.complex128)
        self.assertTrue(np.allclose(mat_elemcalc_real, np.real(mat_elemcalc_complex)),
                        "Matrices for real and complex calculation differ")
        self.assertTrue(np.allclose(0.0, np.imag(mat_elemcalc_complex)),
                        "Imaginary part of matrix not 0.0 as expected")
