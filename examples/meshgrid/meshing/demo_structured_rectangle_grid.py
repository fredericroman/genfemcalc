# -*- coding: utf-8 -*-
"""
Structured grid in rectangle
============================

Illustrate generation of structured grid in rectangular domain.

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/8/2019'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoStructuredRectangleGrid(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        model = meshgrid.modelsgmsh.Rectangle()
        context = {'xa': 0.0, 'xb': 2.0, 'ya': 0.0, 'yb': 2.0,
                   'quadsonly': True, 'transfinite': True,
                   'tsdensity': [4, 4]}
        domain = model.discretize(context)
        mesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh)

        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerMesh(mesh, 'main_mesh')
        viewer.show('main_mesh', color='black', facecolor='None')
        viewer.showNodeLabels('main_mesh', fontsize=20, fontname='Sans', style='italic', rotation=45)
        viewer.showCellLabels('main_mesh', fontsize=20, color='blue')
        viewer.keep()


if __name__ == '__main__':
    demo = DemoStructuredRectangleGrid()
    demo.run()
