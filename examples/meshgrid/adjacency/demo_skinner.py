# -*- coding: utf-8 -*-
"""
Build skin mesh
---------------

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/9/2019'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import benchmarks
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics
import genfemcalc.benchmarks.meshes

PKGDIR = os.path.dirname(os.path.abspath(genfemcalc.__file__))
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoSkinner(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        mesh = benchmarks.meshes.getStructuredMeshInRectangle((3, 3))
        skinner = meshgrid.adjacency.Skinner.fromMesh(mesh)

        boundary_mesh = skinner.buildMesh()

        filename = os.path.join(OUTDIR, 'rectangle_skin.vtk')
        meshgrid.iomrg.writeVtkFromMrg(boundary_mesh, filename)
        self.report(f"Writing file: {filename}")

        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerMesh(boundary_mesh, 'bmesh')
        viewer.show('bmesh', color='black', facecolor='black')
        viewer.showNodeLabels('bmesh', fontsize=20, fontname='Sans', style='italic', rotation=45)
        viewer.showCellLabels('bmesh', fontsize=20, color='blue')
        viewer.keep()

if __name__ == '__main__':
    demo = DemoSkinner()
    demo.run()
