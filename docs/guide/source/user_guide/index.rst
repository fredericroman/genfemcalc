.. include:: ../../replaces.txt
.. _user_guide-part:

####################
Genfemcalc Users' Guide
####################

.. toctree::
    :maxdepth: 3

    concepts
    python_basic
    meshing
    visualisation
    images
    fem_overview
    fem_operations
    bvp
    writing_demos



