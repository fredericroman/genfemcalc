genfemcalc.fem.quadratures module
=================================

.. automodule:: genfemcalc.fem.quadratures
   :members:
   :undoc-members:
   :show-inheritance:
