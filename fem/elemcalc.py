# -*- coding: utf-8 -*-
"""
Element level calculations
--------------------------
FIXME : add module description
"""
__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.22 7:21:48'
__version__ = '0.3.0'

import numpy as np
from genfemcalc import fem


def calcSourceTerm(geom_mapper, f, dtype=np.float64):
    r"""Calculate vector for the source term for a  finite element.

    The source term corresponds to :math:`\int_{\Omega}{fv}`.

    Parameters
    ----------
        geom_mapper : GeomMapper
            It must be precomputed on Gauss' points and updated on the element element.
        f : array like
            sequence of nodal values of the load function f
        dtype
            Type of assembled matrix
    Returns
    -------
        ndarray
           Returns vector of the source term. It has shape (n,)
           where n is the number of element nodes.
    """
    numb_nodes = geom_mapper.refcell.numb_nodes
    load_vec = np.asarray(f)
    if load_vec.shape[0] != numb_nodes:
        numb_loads = load_vec.shape[0]
        raise ValueError(f"Invalid number of nodal values. Expected {numb_nodes} got {numb_loads}")
    # noinspection PyPep8Naming
    N = geom_mapper.precomputed_N    # (g,n) shaped matrix
    load_interpolated = np.einsum('gn,n->g', N, load_vec, dtype=dtype)
    volume_form = geom_mapper.volumeForm
    return np.einsum('gn,g,g,g->n', N, load_interpolated, volume_form,
                     geom_mapper.quadrature.weights, dtype=dtype)


def calcMassTerm(geom_mapper, dtype=np.float64):
    r"""Calculate mass matrix.

    Mass matrix corresponds to the term :math:`\int_{\Omega}{uv}`

    Parameters
    ----------
        geom_mapper : GeomMapper
            It must be precomputed on Gauss' points and updated on the element element.
        dtype
            Type of calculated matrix
    Returns
    -------
        ndarray
           Returns matrix of shape (n,n) where n is the number of element nodes.
    """
    # noinspection PyPep8Naming
    N = geom_mapper.precomputed_N
    mass_matrix = np.einsum('gi,gj,g,g->ij', N, N, geom_mapper.volumeForm,
                            geom_mapper.quadrature.weights, dtype=dtype)
    return mass_matrix


def calcEllipticTerm(geom_mapper, constitutive_matrix=None, dtype=np.float64):
    if geom_mapper.ambient_dim == geom_mapper.refcell.dim:
        return calcEllipticTermProper(geom_mapper, constitutive_matrix, dtype)
    else:
        return calcEllipticTermGeneric(geom_mapper, constitutive_matrix, dtype)


def calcEllipticTermProper(geom_mapper, constitutive_matrix=None, dtype=np.float64):
    r"""Calculate stiffness matrix for elliptic term for the case of reference element
    having the same dimension as the physical space.

    CAUTION: It is assumed that `geom_mapper.ambient_dim == geom_mapper.refcell.dim` however
    this condition is not checked for efficiency.

    The calculated matrix corresponds the term :math:`\int_{\Omega}{\div(D\grad{u})v}`
    where D is called constitutive matrix.

    Parameters
    ----------
        geom_mapper : GeomMapper
            It must be precomputed on Gauss' points and updated on the element element.

        constitutive_matrix : ndarray (optional)
            Matrix of shape (k,k) wher k = geom_mapper.ambient_dim
            If not given unit matrix is assumed (and calculations simplified)
        dtype
            Type of elements of calculated matrix
    Returns
    -------
        ndarray
           Returns matrix of shape (n,n) where n is the number of element nodes.
    """
    jacobian = geom_mapper.jacobian  # shape (g,d,c)
    # noinspection PyPep8Naming
    dN = geom_mapper.precomputed_dN  # shape (g,d,n)

    def b_matrix(d, j):
        return np.einsum('cd,dn->cn', np.linalg.inv(j), d)

    # B matrix at every Gauss point
    b_mat = np.array([b_matrix(derivs, jmat) for (derivs, jmat) in zip(dN, jacobian)])

    if constitutive_matrix is not None:
        stiffness_matrix = np.einsum('gai,ab,gbj,g,g->ij', b_mat, constitutive_matrix, b_mat,
                                     geom_mapper.volumeForm, geom_mapper.quadrature.weights,
                                     dtype=dtype)
    else:
        stiffness_matrix = np.einsum('gai,gaj,g,g->ij', b_mat, b_mat, geom_mapper.volumeForm,
                                     geom_mapper.quadrature.weights, dtype=dtype)
    return stiffness_matrix


def calcEllipticTermGeneric(geom_mapper, constitutive_matrix=None, dtype=np.float64):
    r"""Calculate stiffness matrix for elliptic term.

    The calculated matrix corresponds the term :math:`\int_{\Omega}{\div(D\grad{u})v}`
    where D is called constitutive matrix

    Parameters
    ----------
        geom_mapper : GeomMapper
            It must be precomputed on Gauss' points and updated on the element element.
        constitutive_matrix : ndarray (optional)
            Matrix of shape (k,k) where k = geom_mapper.ambient_dim
            If not given unit matrix is assumed (and calculations simplified)
        dtype :
            Type of matrix elements
    Returns
    -------
        ndarray
           Returns matrix of shape (n,n) where n is the number of element nodes.
    """
    jacobian = geom_mapper.jacobian

    constitutive_inv = None
    if constitutive_matrix is not None:
        constitutive_inv = np.linalg.inv(constitutive_matrix)

    def jjtinv(jmat):
        if constitutive_inv is None:
            return np.linalg.inv(np.matmul(jmat, jmat.T))
        else:
            return np.linalg.inv(np.linalg.multi_dot([jmat, constitutive_inv, jmat.T]))

    jjt_inv = np.array([jjtinv(jmat) for jmat in jacobian])
    # noinspection PyPep8Naming
    dN = geom_mapper.precomputed_dN
    stiffness_matrix = np.einsum('gai,gab,gbj,g,g->ij', dN, jjt_inv, dN, geom_mapper.volumeForm,
                                 geom_mapper.quadrature.weights, dtype=dtype)
    return stiffness_matrix


class TermMeta(type):
    """This metaclass allows to set categories of various Term classes.
    Categories are like property tags, for instance symmertic, linear, etc
    which are invariant, that is allways true for all instances of give term.

    Example: MassTerm has the category of 'symmetric'.
    """
    def __new__(mcs, *args):
        x = super().__new__(mcs, 'TermMeta', (type,), {})
        x.categories = frozenset(args)
        return x

    def __init__(cls, *_args):
        super().__init__(cls)


class BaseTerm(object):
    """FIXME : add docstring
    """
    def __init__(self, name, integrator):
        self._name = name
        self.integrator = integrator

    @property
    def name(self):
        return self._name

    @property
    def mesh(self):
        return self.integrator.mesh

    @classmethod
    def isIt(cls, of_category):
        """FIXME : add docstring
        """
        # noinspection PyUnresolvedReferences
        return of_category in cls.categories

    @property
    def characteristics(self):
        # noinspection PyUnresolvedReferences
        return self.__class__.categories


class SourceTerm(BaseTerm, metaclass=TermMeta('linear')):
    """FIXME : add docstring
    """
    def __init__(self, mesh_integrator, coef, name="F"):
        super().__init__(name, mesh_integrator)
        self.coef = fem.meshcalc.MeshFieldEvaluator(mesh_integrator, field_data=coef)

    def __call__(self, _mesh, elem, _edofs, dtype):
        geom_mapper = self.integrator.getMapper(elem)
        if geom_mapper.dim != self.integrator.dim:
            return None
        # noinspection PyPep8Naming
        N = geom_mapper.precomputed_N    # (g,n) shaped matrix
        coef = self.coef.evalUatStoredPoints(elem, dtype=dtype)
        volume_form = geom_mapper.volumeForm
        vector = np.einsum('gn,g,g,g->n', N, coef, volume_form, geom_mapper.quadrature.weights,
                           dtype=dtype)
        return vector,


class MassTerm(BaseTerm, metaclass=TermMeta('bilinear', 'symmetric')):
    """FIXME : add docstring
    """
    def __init__(self, mesh_integrator, coef=None, name="M"):
        super().__init__(name, mesh_integrator)
        if coef is None:
            self.coef = fem.meshcalc.ConstFieldEvaluator(mesh_integrator, value=1.0)
        else:
            self.coef = fem.meshcalc.MeshFieldEvaluator(mesh_integrator, field_data=coef)

    def __call__(self, _mesh, elem, edofs, dtype):
        geom_mapper = self.integrator.getMapper(elem)
        if geom_mapper.dim != self.integrator.dim:
            return None
        # noinspection PyPep8Naming
        N = geom_mapper.precomputed_N
        coef = self.coef.evalUatStoredPoints(elem, dtype=dtype)
        mass_matrix = np.einsum('g,gi,gj,g,g->ij', coef, N, N, geom_mapper.volumeForm,
                                geom_mapper.quadrature.weights,
                                dtype=dtype)
        return mass_matrix,


class LaplaceTerm(BaseTerm, metaclass=TermMeta('bilinear', 'symmetric')):
    """FIXME : add docstring
    """
    def __init__(self, mesh_integrator, name="K"):
        super().__init__(name, mesh_integrator)

    def __call__(self, _mesh, elem, edofs, dtype):
        geom_mapper = self.integrator.getMapper(elem)
        if geom_mapper.dim != self.integrator.dim:
            return None
        jacobian = geom_mapper.jacobian

        def jjtinv(jmat):
            return np.linalg.inv(np.matmul(jmat, jmat.T))

        jjt_inv = np.array([jjtinv(jmat) for jmat in jacobian])
        # noinspection PyPep8Naming
        dN = geom_mapper.precomputed_dN
        stiffness_matrix = np.einsum('gai,gab,gbj,g,g->ij', dN, jjt_inv, dN,
                                     geom_mapper.volumeForm,
                                     geom_mapper.quadrature.weights, dtype=dtype)
        return stiffness_matrix,

