"""
Solving Poisson problem
-----------------------
"""
import os
import numpy
import matplotlib.pyplot as plt
import scipy.sparse
import scipy.sparse.linalg
import timeit

from genfemcalc import benchmarks
from genfemcalc import examples
from genfemcalc import meshgrid
from genfemcalc import fem
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoPoissonProblemByTerms(examples.Demo):
    def __init__(self):
        super().__init__()
        #self.solution = benchmarks.exact_solutions.alpha_one
        self.solution = benchmarks.ParsedSolution('x*x+y*y')
        self.problem = benchmarks.problems.PoissonInRectangle(self.solution)

    def solve(self, edge_size, i):
        timing = {}
        start_time = timeit.default_timer()
        domain = self.problem.getDomain(edge_size=edge_size, quadsonly=False)
        mrgmesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh)
        meshing_time = timeit.default_timer()
        timing['meshing'] = meshing_time - start_time

        # build boundary condition map for Dirichlet boundary conditions
        # this map maps nodes (dofs)  to their values.
        dirichlet_bc_map = dict()
        for region_name in ['b_east', 'b_north', 'b_west']:
            ids = meshgrid.utilsgmsh.getRegionsNodesIds(domain.mesh, names=(region_name,))
            points = mrgmesh.node_coords[ids, :]
            values = self.problem.evalBoundaryTermOn('dirichlet', region_name, points)
            dirichlet_bc_map.update(dict(zip(ids, values)))

        south_submesh = meshgrid.utilsgmsh.getSubmesh(domain.mesh, names=('b_south',))
        south_mrgmesh = meshgrid.iomrg.buildMrgFromGmsh(south_submesh)

        south_integrator = fem.meshcalc.MeshIntegrator(south_mrgmesh, ambient_dim=2, ngpt=2)
        integrator = fem.meshcalc.MeshIntegrator(mrgmesh, ambient_dim=2, ngpt=4)

        # Build the terms for the problem.
        K_term = fem.elemcalc.LaplaceTerm(integrator, name='K')
        source_nfv = self.problem.evalSourceTermOn(integrator.mesh.points)
        F_term = fem.elemcalc.SourceTerm(integrator, source_nfv, name='F')
        neumann_nfv = self.problem.evalBoundaryTermOn('neumann', 'b_south', south_integrator.mesh.points)
        NBC_term = fem.elemcalc.SourceTerm(south_integrator, neumann_nfv, name='NBC')
        south_assembler = fem.assembly.TermAssembler(NBC_term)
        assembler = fem.assembly.TermAssembler(K_term, F_term, dtype=numpy.float64)

        assembler.preassemble()
        south_assembler.preassemble()

        south_assembler.remap(south_submesh.point_data['l2g'], assembler.total_ndofs)

        preassembly_time = timeit.default_timer()
        timing['preassembly'] = preassembly_time - meshing_time

        storage = 'csr'
        K = assembler.assemble('K', storage)
        F = assembler.assemble('F', storage)
        NBC = south_assembler.assemble('NBC', storage)
        RHS = F+NBC

        Kr, RHSr = fem.assembly.applyDirichletBC(storage, K, RHS, dirichlet_bc_map)

        assembly_time = timeit.default_timer()
        timing['assembly'] = assembly_time - preassembly_time

        if storage == 'dense':
            x = scipy.linalg.solve(Kr, RHSr)
        else:
            x = scipy.sparse.linalg.spsolve(Kr, RHSr)

        solver_time = timeit.default_timer()
        timing['solver'] = solver_time - assembly_time

        mrgmesh.addNodeField('approx_solution', x)
        self.solution.addToMeshAs('exact_solution', mrgmesh)
        # outfile = os.path.join(OUTDIR, f"poisson_solution_{i}.vtk")
        # meshgrid.iomrg.writeVtkFromMrg(mrgmesh, outfile)
        error = benchmarks.errors.computeL2ErrorVersusExact(integrator, 'approx_solution', self.solution)
        exact = mrgmesh.node_fields['exact_solution']
        approx = mrgmesh.node_fields['approx_solution']
        nodal_error = numpy.linalg.norm(exact-approx)

        error_time = timeit.default_timer()
        timing['error'] = error_time - solver_time
        timing['total'] = error_time - start_time

        return error, nodal_error, mrgmesh.numb_nodes, edge_size, timing

    def demonstrate(self):
        #edge_sizes = [0.5, 0.2, 0.1, 0.05, 0.01, 0.008, 0.005]
        edge_sizes = [0.5, 0.2, 0.1, 0.05]
        error = []
        nodal_error = []
        numb_nodes = []
        timing = {'meshing': [], 'preassembly': [], 'assembly': [], 'solver': [], 'error': [], 'total': []}
        for i, edge_size in enumerate(edge_sizes):
            err, noderr, nn, es, tm = self.solve(edge_size, i)
            for key, val in tm.items():
                timing[key].append(val)
            error.append(err)
            nodal_error.append(noderr)
            numb_nodes.append(nn)
            print(f"L2 interpolation error: {err}")
            print(f"Number of nodes: {nn}")
        fig, ax = plt.subplots()
        ax.set_yscale('log')
        ax.set_xscale('log')
        ax.plot(edge_sizes, error, '-bo', edge_sizes, nodal_error, '-gx')
        filename = os.path.join(OUTDIR, 'convergence.png')
        fig.savefig(filename)
        fig.show()
        fig, ax = plt.subplots()
        ax.set_title('Timing')
        ax.set_xlabel('Number of nodes')
        ax.set_ylabel('Time [s]')
        ax.set_xscale('log')
        ax.plot(numb_nodes, timing['meshing'], '-o', label='meshing')
        ax.plot(numb_nodes, timing['preassembly'], '-x', label='preassembly')
        ax.plot(numb_nodes, timing['assembly'], '-d', label='assembly')
        ax.plot(numb_nodes, timing['solver'], '-s', label='solver')
        ax.plot(numb_nodes, timing['error'], '-*', label='error')
        ax.plot(numb_nodes, timing['total'], '-X', label='total')
        ax.legend()
        filename = os.path.join(OUTDIR, 'timin.png')
        fig.savefig(filename)
        fig.show()
            

if __name__ == '__main__':
    demo = DemoPoissonProblemByTerms()
    demo.run()
