# -*- coding: utf-8 -*-
"""
MRG mesh from GMSH
==================

TODO : Add description of demo demo_mrg_from_gmsh.py
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '10/9/2019'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoMrgFromGmsh(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        model_context = {'xa': 0.0, 'xb': 3.0, 'ya': 0.0, 'yb': 2.0,
                         'transfinite': True, 'quadsonly': True, 'tsdensity': [4, 3]}
        rectangle = meshgrid.modelsgmsh.Rectangle()
        domain = rectangle.discretize(model_context, verbose=False)

        mesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh, names=['b_south', 'b_north'])
        print(f"Number of elements: {mesh.numb_elems}")
        print(f"Number of nodes: {mesh.numb_nodes}")

        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerMesh(mesh, 'main')
        viewer.show('main', color='black')
        viewer.showNodes('main', color='yellow')
        viewer.showNodeLabels('main', fontsize=20, fontname='Sans', style='italic', rotation=45)
        viewer.showCellLabels('main', fontsize=20, color='blue')
        viewer.keep()


if __name__ == '__main__':
    demo = DemoMrgFromGmsh()
    demo.run()
