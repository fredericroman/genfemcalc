"""This module provides function for calculation of FEM matrices
and vectors on hexahedral element.

Reference quad element : [0,1]x[0,1]x[0,1]
Vertex numbering

.. code-block:: text
    :caption:
            7-------6
           /|      /|      Z
          / |     / |      |  Y
         /  3 ---/--2      | /
        4-------5  /       |/
        | /     | /        o----X
        |/      |/
        0-------1

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.08.22  11:22:10'
__version__ = '0.1.0'

import numpy as np
import itertools

from genfemcalc import fem


def calcVolume(x, y, z):
    """Calculate area of quad with nodes (x_i,y_i)
    The implementation uses Shoelance formula:
    https://en.wikipedia.org/wiki/Shoelace_formula

    CAUTION: coordinates should be in counterclockwise order
    """
    return 0.5*np.abs(np.dot(x, np.roll(y, 1))-np.dot(y, np.roll(x, 1)))


def calcShapeFun(ksi, eta,  zeta):
    """Calculate shape function at points with reference coordinates (ksi, eta, zeta)

    Parameters
    ----------
        ksi : array like
        eta : array like
        zeta : array like

    The parameters are treated by `np.asarray`

    Returns
    -------
        ndarray
            Array of shape `(n, 8)` of shape function values.
    """
    x, y, z = np.asarray(ksi), np.asarray(eta), np.asarray(zeta)
    N = np.array([-x*y*z + x*y + x*z - x + y*z - y - z + 1,
                  x*y*z - x*y - x*z + x,
                  -x*y*z + x*y,
                  x*y*z - x*y - y*z + y,
                  x*y*z - x*z - y*z + z,
                  -x*y*z + x*z,
                  x*y*z,
                  -x*y*z + y*z], dtype='float64')
    return N.T

def calcShapeDerivFun(ksi, eta, zeta):
    """Calculate shape functions derivatives at points with reference coordinates (ksi, eta, zeta)

    Parameters
    ----------
        ksi : array like
        eta : array like
        zeta : array like

    The parameters are treated by `np.asarray`

    Returns
    -------
        ndarray
            Array of shape `(g,3,8)` of shape function values where `g` is the number
            of evaluation points
    """
    x, y, z = ksi, eta, zeta
    dN = np.array([[-y * z + y + z - 1, -x * z + x + z - 1, -x * y + x + y - 1],
                   [y * z - y - z + 1, x * z - x, x * y - x],
                   [-y * z + y, -x * z + x, -x * y],
                   [y * z - y, x * z - x - z + 1, x * y - y],
                   [y * z - z, x * z - z, x * y - x - y + 1],
                   [-y * z + z, -x * z, -x * y + x], [y * z, x * z, x * y],
                   [-y * z, -x * z + z, -x * y + y]], dtype='float64')
    dN = np.moveaxis(dN, [0, 2], [2, 0])
    return dN


def calcJacobian(x, y, z, dN):
    """Calculate jacobian matrix for hexahedron element at some points in reference element.
    
    The points in reference element are specified implicitly by providing the values of shape
    function derivatives in the argument `dN`

    Parameters
    ----------
        x,y,z : array like
            Nodal coordinates of hexahedron element
        dD : ndarray
            3D ndarray of shape (g,3,8) where g is the number of points in reference element
            containing the values of shape functions derivatives
    Returns
    -------
        ndarray : 
            3D array of Jacobian matrices at points in the reference element.

    """
    pts = np.c_[x, y, z]
    jacobian = np.einsum('gdn,nc->gdc', dN, pts)
    return jacobian


def calcSourceTerm(x, y, z, ngpt, f=np.ones((8,))):
    r"""Calculate vector for the source term,
    that is the term \int f v.

    Arguments:
        f - sequence of f nodal values
    """
    ksi, eta, zeta, ww = getQuadrature(ngpt)
    N = calcShapeFun(ksi, eta, zeta)
    dN = calcShapeDerivFun(ksi, eta, zeta)
    J = calcJacobian(x, y, z, dN)
    det_jmat = np.array([np.linalg.det(jmat) for jmat in J])
    f_interpolated = np.einsum('gn,n->g', N, np.asarray(f))
    F = np.einsum('gn,g,g,g->n', N, f_interpolated, det_jmat, ww)
    return F


def calcMassTerm(x, y, z, ngpt):
    r"""Calculate the term :math:`\int N_i N_j`
    """
    ksi, eta, zeta, ww = getQuadrature(ngpt)
    N = calcShapeFun(ksi, eta, zeta)
    dN = calcShapeDerivFun(ksi, eta, zeta)
    J = calcJacobian(x, y, z, dN)
    det_jmat = np.array([np.linalg.det(jmat) for jmat in J], dtype='float64')
    M = np.einsum('gi,gj,g,g->ij', N, N, det_jmat, ww)
    return M

def calcEllipticTerm(x, y, z, ngpt, D=None):
    """Calculate stiffness matrix for eliptic term:
       grad (D grad u)
    """
    ksi, eta, zeta, ww = getQuadrature(ngpt)
    N = calcShapeFun(ksi, eta, zeta)
    dN = calcShapeDerivFun(ksi, eta, zeta)
    J = calcJacobian(x, y, z, dN)
    det_jmat = np.array([np.linalg.det(jmat) for jmat in J])
    
    if D is None:
        inv_jjt = np.array([np.linalg.inv(np.matmul(jmat, jmat.T)) for jmat in J])
        K = np.einsum('gai,gab,gbj,g,g->ij', dN, inv_jjt, dN, det_jmat, ww)
    else:
        inv_jmat = np.array([np.linalg.inv(jmat) for jmat in J])
        K = np.einsum('gai,gsa,st,gtb,gbj,g,g->ij',
                      dN, inv_jmat, D, inv_jmat, dN, det_jmat, ww)
    return K 

def getQuadrature(ngpt):
    """Return vectors of ksi,eta,ww being respectively
    Gauss point nodes (kis,eta) and weights of the 2D
    quadrature being the produc of 1D quadratures with ngpt points each
    """
    points, weights = fem.quadratures.getQuadratureForHexahedron(ngpt)
    ksi, eta, zeta = points[:, 0], points[:, 1], points[:, 2]
    return ksi, eta, zeta, weights
