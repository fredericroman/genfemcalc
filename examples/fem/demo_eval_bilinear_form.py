# -*- coding: utf-8 -*-
"""
Evaluate bilinear form on mesh
------------------------------
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/13/2019'
__version__ = '0.1.0'

import os
import sympy
import sympy.vector
import math

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
from genfemcalc import fem

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoEvalBilinearForm(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        model = meshgrid.modelsgmsh.Rectangle()
        context = {'xa': 0.0, 'xb': 2.0, 'ya': 0.0, 'yb': 2.0,
                   'quadsonly': True, 'transfinite': True,
                   'tsdensity': [10, 10]}
        domain = model.discretize(context)
        mesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh)
        expression = 'x'
        field_data = mesh.evalNodeField(field_name='u', expression=expression)
        integrator = fem.meshcalc.MeshIntegrator(mesh, ngpt=2, ambient_dim=2)
        term = fem.elemcalc.MassTerm(integrator, name="M")
        norm = fem.meshcalc.computeFieldNormL2(integrator, field_data)
        assembler = fem.assembly.TermAssembler(term)
        assembler.preassemble()
        form_value = assembler.evalBilinearForm(field_data)
        dummy = math.sqrt(form_value)
        print(f"Field norm: {norm}, {dummy}")

if __name__ == '__main__':
    demo = DemoEvalBilinearForm()
    demo.run()
