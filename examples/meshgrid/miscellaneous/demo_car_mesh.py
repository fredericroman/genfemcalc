# -*- coding: utf-8 -*-
"""
Meshing car compartment
=======================

Illustrate meshing of domain for mesh compartment
"""
__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.08.28  0:14:20'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import meshgrid
from genfemcalc import examples

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoMeshCar(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        # -- Meshing domain
        context = {'quadsonly': True,
                   'edge_size': 0.5}

        generator = meshgrid.modelsgmsh.CarCompartment()
        domain = generator.discretize(context)

        print('Model regions:')
        for region in domain.regions:
            print(region)

        # Wrapping in MRG
        mesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh)
        outfile = os.path.join(OUTDIR, 'car_compartment.vtk')
        meshgrid.iomrg.writeVtkFromMrg(mesh, outfile, binary=False)

        mesh.listElemCount()


if __name__ == '__main__':
    demo = DemoMeshCar()
    demo.run()
