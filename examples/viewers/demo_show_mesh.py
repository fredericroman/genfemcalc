# -*- coding: utf-8 -*-
"""
Mesh viewer
-----------

Show basic usage of mesh viewer.
"""
import os

import genfemcalc
from genfemcalc import meshgrid
from genfemcalc import examples
import genfemcalc.meshgrid.graphics
import matplotlib.colors
import numpy

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoShowMesh(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        file_name = os.path.join(DATADIR, 'mesh_with_fields.vtk')
        mesh = meshgrid.iomrg.readMrgFromVtk(file_name)
        mesh.evalNodeField("temperature", "2*x")
        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerMesh(mesh, 'main')
        viewer.showNodeField('main', 'temperature')
        viewer.showNodes('main', color='yellow')
        viewer.showNodeLabels('main', fontsize=20, fontname='Sans', style='italic', rotation=45)
        viewer.showCellLabels('main', fontsize=20, color='blue')
        viewer.keep()


if __name__ == '__main__':
    demo = DemoShowMesh()
    demo.run()
