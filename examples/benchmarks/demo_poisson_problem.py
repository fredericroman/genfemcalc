"""
Poisson element routine
-----------------------
"""
import os
import numpy

from genfemcalc import meshgrid
from genfemcalc import benchmarks
from genfemcalc import examples
from genfemcalc import fem
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')

class PoissonElementRoutine(object):
    def __init__(self, geom_mapper, source_term):
        self.mapper = geom_mapper
        self.source_term = source_term

    def __call__(self, mesh, elem, edofs, dtype):
        if mesh.elem_geotype[elem] == meshgrid.ElemType.TRIANGLE:
            points = mesh.node_coords[edofs, :]
            self.mapper.setNodes(points)
            K = fem.elemcalc.calcEllipticTerm(self.mapper, dtype=dtype)
            f = self.source_term[edofs]
            F = fem.elemcalc.calcSourceTerm(self.mapper, f, dtype=dtype)
            return K,F
        else:
            return None


class DemoPoissonProblem(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        solution = benchmarks.exact_solutions.alpha_one
        problem = benchmarks.problems.PoissonInRectangle(solution)
        viewer = meshgrid.graphics.MeshViewer()

        domain = problem.getDomain(edge_size=0.05, quadsonly=False)

        mrgmesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh)

        viewer.registerMesh(mrgmesh, 'default')
        viewer.show('default', color='black', facecolor='None', linewidth=0.5)
        viewer.showNodes('default')

        south_nodes = meshgrid.utilsgmsh.getRegionsNodesIds(domain.mesh, names=('b_south',))
        south_points = mrgmesh.node_coords[south_nodes, :]
        viewer.registerPoints(south_points, 'south')
        viewer.showNodes('south')
        viewer.keep()

        # build boundary condition map for Dirichlet boundary conditions
        # this map maps nodes (dofs)  to their values.
        bc_map = dict()
        for region_name in ['b_east', 'b_north', 'b_west']:
            ids = meshgrid.utilsgmsh.getRegionsNodesIds(domain.mesh, names=(region_name,))
            points = mrgmesh.node_coords[ids, :]
            values = problem.evalBoundaryTermOn('dirichlet', region_name, points)
            bc_map.update(dict(zip(ids, values)))

        cell = meshgrid.refcells.getRefCell('vtk', meshgrid.ElemType.convert('triangle'))
        quadrature = fem.quadratures.Quadrature(cell, 4)

        mapper = fem.manifolds.GeomMapper(cell, ambient_dim=2)
        mapper.precomputeForQuadrature(quadrature)

        source_term = problem.evalSourceTermOn(mrgmesh.node_coords)

        element_routine = PoissonElementRoutine(mapper, source_term)

        assembler = fem.assembly.MrgAssembler(mrgmesh)
        assembler.preassembleWithDirichletBC('K', 'F', element_routine, bc_map)
        K = assembler.assemble('K', 'dense')
        F = assembler.assemble('F', 'dense')
        x = numpy.linalg.solve(K,F)
        mrgmesh.addNodeField('dummy', x)
        outfile = os.path.join(OUTDIR, 'poisson_solution.vtk')
        meshgrid.iomrg.writeVtkFromMrg(mrgmesh, outfile)


if __name__ == '__main__':
    demo = DemoPoissonProblem()
    demo.run()
