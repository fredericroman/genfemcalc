genfemcalc.fembasic.trianglefem module
======================================

.. automodule:: genfemcalc.fembasic.trianglefem
   :members:
   :undoc-members:
   :show-inheritance:
