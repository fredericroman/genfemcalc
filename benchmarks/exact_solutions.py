# -*- coding: utf-8 -*-
"""Definition of functions serving purpose of exact solutions.

The classes representing functions are named using codes from NATO phonetic alphabet:
alpha, bravo, charlie, etc
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.08.22  19:33:59'
__version__ = '0.1.0'

import abc
import sympy
import numpy
import sympy.utilities.lambdify

from genfemcalc import utils
from genfemcalc import meshgrid

class Solution(abc.ABC):
    """Base class for all symbolic solutions.
    """
    coord_vars = frozenset(sympy.symbols('x:z'))

    def __init__(self):
        super().__init__()

    @property
    @abc.abstractmethod
    def expression(self):
        ...

    @property
    def dim(self):
        return len(self.coord_vars.intersection(self.expression.free_symbols))

    def __str__(self):
        return str(self.expression)

    def print(self):
        print(self.expression)

    def toNumeric(self):
        symbols = self.expression.free_symbols
        return sympy.lambdify(symbols, self.expression)

    def eval(self, points):
        fun = self.toNumeric()
        if self.dim > 0:
            pts = points.reshape((-1, self.dim))
            return fun(*pts.T)
        else:
            return numpy.tile(self.expression[:], (points.shape[0],1))

    def addToMeshAs(self, name, mesh):
        fun = self.toNumeric()
        mesh = meshgrid.wrappers.MrgMesh(mesh)
        field_data = self.eval(mesh.points[:, 0:self.dim])
        mesh.addNodeField(name, field_data)


class ParsedSolution(Solution):
    """This class provides way to construct exact solution by parsing some expression string.
    The expression string can utilize variable x, y z.
    """
    def __init__(self, expression_string):
        super().__init__()
        self._expression = sympy.parsing.sympy_parser.parse_expr(expression_string)

    @property
    def expression(self):
        return self._expression


class SolutionAlpha(Solution):
    reference_url = r'http://ammar-hakim.org/sj/je/je11/je11-fem-poisson.html'

    def __init__(self, xparams, yparams, make_symbolic=True):
        super().__init__()
        if len(xparams) != 3:
            raise ValueError("Expected four parametres for x factor")
        if len(yparams) != 3:
            raise ValueError("Expected four parameters for y factor")
        if make_symbolic:
            self._xparams = [sympy.nsimplify(x, rational=True) for x in xparams]
            self._yparams = [sympy.nsimplify(y, rational=True) for y in yparams]
        else:
            self._xparams = xparams.copy()
            self._yparams = yparams.copy()

        x, y = sympy.symbols('x y')
        fx = x**2/2 - x**4/12 * self._xparams[0] + x * self._xparams[1] + self._xparams[2]
        fy = y**2/2 - y**4/12 * self._yparams[0] + y * self._yparams[1] + self._yparams[2]
        self._expression = fx * fy

    @property
    def expression(self):
        return self._expression

    def show(self, xrange=(0.0, 1.0), yrange=(0.0, 1.0)):
        x, y = sympy.symbols('x y')
        sympy.plotting.plot3d(self._expression, (x, xrange), (y, yrange))


alpha_one = SolutionAlpha([2, 2/12-1/2, 0], [5, 0, 5/12-1/2])
