# -*- coding: utf-8 -*-
"""
Assembly on simple mesh
=======================

.. code-block:: text

                      6
                    / |
                  /   |
                 /  2 |
       3 ------ 2 --- 5
       |        |     |
       |   0    |  1  |
       |        |     |
       0 ------ 1 --- 4

"""

import numpy
import os

# MRG modules
import genfemcalc
from genfemcalc import meshgrid
from genfemcalc import fem
from genfemcalc import examples

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class ElementEvaluator(object):
    def __init__(self):
        pass

    def __call__(self, mesh, elem, edofs, dtype):
        nd = len(edofs)
        K = numpy.ones((nd, nd))*(elem+1) + (elem+1)*numpy.identity(nd)
        f = numpy.ones((nd,))*(elem+1)
        return K, f


class DemoAssembly(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        fname = os.path.join(DATADIR, 'simple_quad_tri_mesh.vtk')
        mymesh = meshgrid.iomrg.readMrgFromVtk(fname)

        assembler = fem.assembly.MrgAssembler(mymesh, dtype=numpy.complex128)
        evaluator = ElementEvaluator()
        assembler.preassemble(matrix_names=['K'],
                             vector_names=['F'],
                              element_routine=evaluator)

        K = assembler.assemble('K', 'csr')
        F = assembler.assemble('F', 'csr')
        print(K)
        print('--------')
        print(F)
        bc_map = {1: 10, 6: 20}
        assembler.preassembleWithDirichletBC('K', 'F',element_routine=evaluator, bc_map=bc_map, overwrite=True)
        K = assembler.assemble('K', 'dense')
        F = assembler.assemble('F', 'dense')
        print('Determinant of K : ', numpy.linalg.det(K))
        print('--------')
        #print(F)
        print('--------')
        x = numpy.linalg.solve(K,F)
        print(x)


if __name__ == '__main__':
    demo = DemoAssembly()
    demo.run()
