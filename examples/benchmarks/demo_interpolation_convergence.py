# -*- coding: utf-8 -*-
"""
Illustrate interpolation convergence
------------------------------------

Calculate L2 interpolation error on a series of meshes.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/25/2019'
__version__ = '0.1.0'

import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

import genfemcalc
from genfemcalc import examples
from genfemcalc import benchmarks
from genfemcalc import fem
from genfemcalc import meshgrid

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoInterpolationConvergence(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        xside, yside = 4.0, 4.0
        resolutions = np.array([2,3,4,8,12,20,40])
        errors = np.zeros((resolutions.size,), dtype=np.float64)
        solution = benchmarks.exact_solutions.ParsedSolution('x*x+y*y')
        for i, res in enumerate(resolutions):
            mesh, l2g = meshgrid.modelsmrg.buildRectangle(-xside, xside, -yside, yside, res, res)
            solution.addToMeshAs('u', mesh)
            integrator = fem.meshcalc.MeshIntegrator(mesh, ngpt=4)
            errors[i] = benchmarks.errors.computeL2ErrorVersusExact(integrator, 'u', solution)
        for res, error in zip(resolutions, errors):
            print(f"Resolution: {res},  error: {error}")
        fig, ax = plt.subplots()
        ax.set_yscale('log')
        ax.set_xscale('log')
        ax.plot(resolutions**2, errors, '-bo')
        if self.show_images:
            fig.show()

if __name__ == '__main__':
    demo = DemoInterpolationConvergence()
    demo.run()
