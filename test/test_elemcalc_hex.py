"""Unittest for element calculations
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.08.22  9:8:43'
__version__ = '0.1.0'

import unittest
import numpy as np

from fem import manifolds
from fem import quadratures
from fem import elemcalc
from fembasic import hexfem
from meshgrid import refcells

class TestHexCalculation(unittest.TestCase):
    def setUp(self):
        self.cell = refcells.getRefCell('gmsh', 5)
        self.mapper = manifolds.GeomMapper(self.cell, 3)
        self.ngpt = 2
        self.quadrature = quadratures.Quadrature(self.cell, self.ngpt)
        self.nodes = np.array([[1, 1, 0],
                               [3, 1, 0],
                               [4, 3, 0],
                               [0, 2, 0],
                               [0, 0, 3],
                               [3, 1, 2],
                               [4, 3, 3],
                               [0, 2, 3]], dtype='float64')

        self.mapper.precomputeForQuadrature(self.quadrature)
        self.mapper.setNodes(self.nodes)

    def get_load_vector(self, mapper):
        load_vec = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]
        load_elemcalc = elemcalc.calcSourceTerm(mapper, load_vec)
        x = self.nodes[:, 0]
        y = self.nodes[:, 1]
        z = self.nodes[:, 2]
        load_quadfem = hexfem.calcSourceTerm(x, y, z, self.ngpt, load_vec)
        return load_elemcalc, load_quadfem

    def get_mass_matrices(self, mapper):
        mass_elemcalc = elemcalc.calcMassTerm(mapper)
        x = self.nodes[:, 0]
        y = self.nodes[:, 1]
        z = self.nodes[:, 2]
        mass_quadfem = hexfem.calcMassTerm(x, y, z, self.ngpt)
        return mass_elemcalc, mass_quadfem

    def get_stiffness_matrices(self, mapper, material_matrix):
        stiffness_elemcalc = elemcalc.calcEllipticTerm(mapper, material_matrix)
        x = self.nodes[:, 0]
        y = self.nodes[:, 1]
        z = self.nodes[:, 2]
        stiffness_quadfem = hexfem.calcEllipticTerm(x, y, z, self.ngpt, material_matrix)
        return stiffness_elemcalc, stiffness_quadfem

    def test_mass_term(self):
        mat_elemcalc, mat_quadfem = self.get_mass_matrices(self.mapper)
        self.assertTrue(np.allclose(mat_elemcalc, mat_quadfem),
                        "Matrices from elemcalc and quadfem differ")

    def test_elliptic_term(self):
        mat_elemcalc, mat_quadfem = self.get_stiffness_matrices(self.mapper, None)
        self.assertTrue(np.allclose(mat_elemcalc, mat_quadfem),
                        "Matrices from elemcalc and quadfem differ")

    def test_elliptic_term_with_material(self):
        material_matrix = np.array([[2.0, 3.0, 4.0], [4.0, 5.0, 6.0],
                                    [1.0, 1.0, 10.0]])
        mat_elemcalc, mat_quadfem = self.get_stiffness_matrices(self.mapper, material_matrix)
        self.assertTrue(np.allclose(mat_elemcalc, mat_quadfem),
                        "Matrices from elemcalc and quadfem differ for ")
    def test_source_term(self):
        vec_elemcalc, vec_quadfem = self.get_load_vector(self.mapper)
        self.assertTrue(np.allclose(vec_elemcalc, vec_quadfem),
                        "Matrices from elemcalc and quadfem differ")
