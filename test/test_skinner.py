"""Test MeshSkinner
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/10/2019'
__version__ = '0.1.0'

import numpy
import os
import unittest

import genfemcalc
import genfemcalc.meshgrid

PKGDIR = os.path.dirname(os.path.abspath(genfemcalc.__file__))
DATADIR = os.path.join(PKGDIR, 'test', 'data')

class TestSkinner(unittest.TestCase):
    def setUp(self):
        filename = os.path.join(DATADIR, 'two_holes_grid.png')
        mesh = genfemcalc.meshgrid.iomrg.readMrgFromImage(filename)
        self.skinner = genfemcalc.meshgrid.adjacency.Skinner.fromMesh(mesh)

    def test_skin_size(self):
        boundary_mesh = self.skinner.buildMesh()
        self.assertEqual(boundary_mesh.numb_nodes, 58)

    def test_skin_l2g(self):
        boundary_mesh = self.skinner.buildMesh()
        l2g = self.skinner.l2g_nodes()
        for i in range(boundary_mesh.numb_nodes):
            gi = l2g[i]
            local_point = boundary_mesh.node_coords[i, :]
            global_point = self.skinner.mesh.node_coords[gi, :]
            self.assertTrue(numpy.allclose(local_point, global_point))

    def test_skin_geotype(self):
        boundary_mesh = self.skinner.buildMesh()
        status = numpy.all(boundary_mesh.elem_geotype == int(genfemcalc.meshgrid.ElemType.LINE))
        self.assertTrue(status)

