"""This module provides functions for calculation of FEM matrices
and vectors on 2D segment element

Reference line element : [-1,1]
Vertex numbering

.. code-block : text

        0-------1

"""

import numpy as np
import itertools

from genfemcalc import fem

def calcLength(nodes):
    """Calculate segment length
    """
    p1 = nodes[0,:]
    p2 = nodes[1,:]
    return np.linalg.norm(p1-p2)

def calcJacobian(x,y, dN):
    """Calculate jacobian matrix for isoparametrix segment element
    with nodal coodinates (x_i, y_i) at specific points (ksi)
    in reference element
    The arragement of a row is:

    .. math::
        J[i,:] = dx/dksi dy/dksi

    thus the row correspods to jacobian matrix at i-th point (ksi_i)
    """
    npt = dN.shape[0]
    assert len(x) == 2 and len(y) == 2
    J = np.zeros((npt, 2), dtype=np.float64)
    J[:, 0] = np.matmul(dN,x)
    J[:, 1] = np.matmul(dN,y)
    return J


def calcShapeFun(ksi):
    """Calculate shape function at points with reference
    coordinates in ksi

    Returns
    -------
        ndarray
            Matrix (n by 2) of shape function values
    """
    npt = len(ksi)
    N = np.zeros((npt, 2), dtype=np.float64)
    N[:,0]=(1-ksi)/2
    N[:,1]=(1+ksi)/2
    return N

def calcShapeDerivFun(ksi):
    """Calculate derivatives of shape function at points with reference
    coordinates in ksi

    Returns
    -------
        ndarray
            dN (n by 2*2) of derivatives values
            the arrangement in a row is: dN_0/dksi, ... dN_i/dkis,  ...
    """
    npt= len(ksi)
    dN = np.zeros((npt, 2*2), dtype=np.float64)
    dN[:,0]=-0.5
    dN[:,1]= 0.5
    return dN

def calcSourceTerm(nodes,f=(1.0,1.0),dtype=np.float64):
    """Calculate vector for the source term,
    that is the term \int f v.

    Arguments:
        f - sequence of values of f at nodes.
    """
    L = calcLength(nodes)
    f0 = 2*f[0]+f[1]
    f1 =   f[0]+2*f[1]
    F = np.array([f0,f1], dtype=dtype)
    F *= (L/6)
    return F

def calcMassTerm(nodes, dtype=np.float64):
    """Calculate the term \int N_i N_j
    """
    L = calcLength(nodes)

    M = np.array([[1.0/3, 1.0/6],[1.0/6, 1.0/3]], dtype=dtype)
    M *= L
    return M

def calcEllipticTerm(nodes, dtype=np.float64):
    """Calculate stiffness matrix for eliptic term:
       div (D grad u)
    """
    L = calcLength(nodes)
    K = np.array([[1.0, -1.0],[-1.0, 1.0]], dtype=dtype)
    K /= L
    return K

def getLineQuadrature(ngpt):
    """Return vectors of ksi,ww being respectively
    Gauss point nodes and weights of the 1D
    """
    ksi, w = np.polynomial.legendre.leggauss(ngpt)
    return ksi, ww

calcMeasure = calcLength

if __name__ == "__main__":
   print("Testing linefem module")

   print("Test partition of unity for shape functions (10 points)")
   npt = 10
   ksi = np.random.rand(npt)*2 - 1
   N = calcShapeFun(ksi)
   with np.printoptions(precision=5):
       print(N.sum(axis=1))

   x = np.array([0,5])
   y = np.array([0,3])
   K = calcEllipticTerm(x, y)
   print('Elliptic stiffness matrix:\n', K)
   print('Symmetric: ', fem.matrixquery.isSymmetric(K))
   print('Invertible: ', fem.matrixquery.isInvertible(K, tol=1.e-5))
   print('Determinant: ', np.linalg.det(K))

   F = calcSourceTerm(x,y)
   print('Source term for constant f=1:\n', F)
   print('Sum F : ', np.sum(F))

   M = calcMassTerm(x, y)
   print('Mass matix:\n', M)
   print('Sum M:\n', np.sum(M))
   print('Symmetric :', fem.matrixquery.isSymmetric(M))
   print('Invertible :', fem.matrixquery.isInvertible(M, tol=1.e-5))
