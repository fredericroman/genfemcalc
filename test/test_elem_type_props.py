"""
Test properties of ElemType enum
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '10/26/2019'
__version__ = '0.1.0'

import unittest

from genfemcalc.meshgrid import ElemType


class TestElemTypeProps(unittest.TestCase):
    def setUp(self):
        pass

    def test_something(self):
        dims = {ElemType.VERTEX: 0,
                ElemType.LINE: 1,
                ElemType.TRIANGLE: 2,
                ElemType.QUAD: 2,
                ElemType.TETRA: 3,
                ElemType.HEXAHEDRON: 3}
        for et in ElemType:
            self.assertEqual(et.dim, dims[et])
