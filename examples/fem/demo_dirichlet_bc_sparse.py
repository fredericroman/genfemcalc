# -*- coding: utf-8 -*-
"""
Handling Dirichlet BC in sparse system
======================================

This demo illustrated how one can tranform sparse system to include
Dirichlet type constraints, that is the case of some unknowns having
prescribed values.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/7/2019'
__version__ = '0.1.0'

import os
import scipy.sparse.linalg
import numpy

import genfemcalc
from genfemcalc import examples
from genfemcalc import fem
from genfemcalc.benchmarks import matrices


PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoDirichletBcSparse(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        storage_type = 'csr'
        A, b, x_ref = matrices.getTridiagSystem(dim=6, format=storage_type)
        bc_map = {1: 1.0, 5: 1.0}
        A_bc, b_bc = fem.assembly.applyDirichletBC(storage_type, A, b, bc_map)
        if storage_type == 'dense':
            x = scipy.linalg.solve(A_bc, b_bc)
        else:
            x = scipy.sparse.linalg.spsolve(A_bc, b_bc)
        if numpy.allclose(x, x_ref):
            print('Reference solution equal to actuall')
        else:
            print('CAUTION reference and actual solutin differ')
            print(x)
            print(x_ref)


if __name__ == '__main__':
    demo = DemoDirichletBcSparse()
    demo.run()
