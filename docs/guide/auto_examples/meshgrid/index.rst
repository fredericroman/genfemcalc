.. include:: ../../replaces.txt
.. _meshgrid_examples-sec:

Examples for meshgrid package
=============================

.. toctree::
   :maxdepth: 3

   geometry/index
   adjacency/index
   meshing/index
   miscellaneous/index
