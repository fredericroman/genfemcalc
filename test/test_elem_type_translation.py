"""Unit test for translation between element type IDs from different families.

Element types are recognised by element ID. This test checks if the translation
between them works properly.

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/5/2019'
__version__ = '0.1.0'

import unittest
import genfemcalc

class TestElemTypeTranslation(unittest.TestCase):
    def setUp(self):
        self.vtk_gmsh = genfemcalc.meshgrid.refcells.CellIdTranslator('vtk', 'gmsh')
        self.gmsh_vtk = genfemcalc.meshgrid.refcells.CellIdTranslator('gmsh', 'vtk')
        self.vtk_meshio = genfemcalc.meshgrid.refcells.CellIdTranslator('vtk', 'meshio')
        self.meshio_vtk = genfemcalc.meshgrid.refcells.CellIdTranslator('meshio', 'vtk')

    def test_vtk_to_gmsh(self):
        self.assertEqual(self.vtk_gmsh(1), 15)  # VERTEX
        self.assertEqual(self.vtk_gmsh(3), 1)  # LINE
        self.assertEqual(self.vtk_gmsh(5), 2)  # TRIANGLE
        self.assertEqual(self.vtk_gmsh(9), 3)  # QUAD
        self.assertEqual(self.vtk_gmsh(10), 4)  # TETRAHEDRON
        self.assertEqual(self.vtk_gmsh(12), 5)  # HEXAHEDRON

    def test_vtk_to_gmsh_idempotence(self):
        tr = self.vtk_gmsh
        itr = self.gmsh_vtk
        for type_id in [1, 3, 5, 9, 10, 12]:
            self.assertEqual(itr(tr(type_id)), type_id)

    def test_vtk_to_meshio_idempotence(self):
        tr = self.vtk_meshio
        itr = self.meshio_vtk
        for type_id in [1, 3, 5, 9, 10, 12]:
            self.assertEqual(itr(tr(type_id)), type_id)
