# -*- coding: utf-8 -*-
"""
Module to take advantage of shapely package by transforming 2D mesh
into shapely.MultiPolygon
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '10/26/2019'
__version__ = '0.1.0'

import math
import munch
import numpy
import random
import shapely.geometry

from genfemcalc import meshgrid


def convertCell2Polygon(mesh, cell_id):
    """Convert 2D mesh cell to a polygon.
    Parameters
    ----------
    mesh: :obj:`Mesh`
        MRG mesh
    cell_id: int
        cell index
    Returns
    -------
        :obj:`shapely.Polygon` is cell is 2D or None otherwise
    """
    mrgmesh = meshgrid.wrappers.MrgMesh(mesh)
    cell_type = meshgrid.ElemType(mrgmesh.elem_geotype[cell_id])
    if cell_type.dim != 2:
        return None
    else:
        return shapely.geometry.Polygon(mesh.getCellPoints(cell_id))


def makeMultiPolygon(mesh):
    """Convert MRG mesh to shapely.MultiPolygon
    """
    mrgmesh = meshgrid.wrappers.MrgMesh(mesh)
    polys = [convertCell2Polygon(mrgmesh, c) for c in mrgmesh.cells() if mrgmesh.getCellDim(c) == 2]
    return shapely.geometry.MultiPolygon(polys)


def buildRandomPolygon(center, out_radius=1.0, radius_factor=1.0, nsides=3,
                       start_angle=None, randomize_angle=True, z=0.0,
                       seed=None):
    """Construct random polygon. The polygon is created by randomizing number of
    sides and positions on regular polygon inscribed in circle of (center, out_radius)

    Parameters
    ----------
    center : :obj:`tuple` of floats
        Incribed radius center (x,y)
    out_radius : :obj:`float`
        Inscribed radius
    radius_factor : :obj:`float`, optional
        Factor to calculate in_radius = radius_factor * out_radius.
    nsides : :obj:`int`, optional
        Number of polygon sides
    start_angle : :obj:`float', optional
        Angle coordinate (in degrees) for the first vertex
    randomize_angle : :obj:`bool`, optional
        If true angle position of vertices is randomised
    z : :obj:`float`, optional
        The Z-coordinate of polygon points
    seed : :obj:`int` or :obj:`None`
        Seed for random number generator
    Returns
    -------
    :obj:`shapely.geometry.Polygon`
    """
    random.seed(seed)
    numpy.random.seed(seed)
    if isinstance(nsides, tuple):
        nsides = random.randint(*nsides)
    if nsides < 3:
        raise ValueError(f"Number of sides ({nsides}) must be >= 3")
    if start_angle is None:
        start_angle = random.uniform(0, 2*math.pi*((nsides-1.0)/nsides))
    else:
        start_angle = math.radians(start_angle)
    end_angle = start_angle + 2*math.pi*((nsides-1.0)/nsides)
    angles = numpy.linspace(start_angle, end_angle, nsides)
    if randomize_angle:
        deltas = 2*math.pi/nsides*(numpy.random.uniform(size=nsides) - 0.5)
        angles += deltas
    if radius_factor < 1:
        radii = out_radius*(1.0 - (1.0 - radius_factor)*numpy.random.uniform(size=nsides))
    else:
        radii = numpy.full((nsides,), fill_value=out_radius)
    points = numpy.empty((nsides, 3), dtype=numpy.float64)
    points[:, 0] = radii * numpy.cos(angles) + center[0]
    points[:, 1] = radii * numpy.sin(angles) + center[1]
    points[:, 2] = numpy.full((nsides,), fill_value=z)
    return shapely.geometry.Polygon(points)


def getPolySoupStats(poly_soup):
    """Return polygonal soup statistics:
        * pts - total number of points
        * pts_ext - total number of points on external contours
        * pts_int - total number of ponts on internal contours
        * polys - total number of polygons (including holes)
        * polys_int - number of external contours
        * polys_ext - number of internal contorus
        * polys_multi - number of multipolygons
        * max_polys_multi - maximum number of polygons in mulitipolygon
        * max_pts_ext - maximum number of points in exterior polygon
        * max_pts_int - maximum number of points in interior polygon
        * max_holes = maximum number of holes in polygons
    Parameters
    ----------
    poly_soup :
        sequence of shapely Polygon or MultiPolygon objects
    Return
    ------
    :obj:`munch.Munch`
        statistics dictionary
    """
    stat = munch.Munch(pts=0, pts_ext=0, pts_int=0,
                       polys=0, polys_ext=0, polys_int=0, polys_multi=0,
                       max_polys_multi=0, max_pts_ext=0, max_pts_int=0,
                       max_holes=0)

    def count_poly(_poly, out_stat):
        pts_count = len(_poly.exterior.coords)-1
        out_stat.max_pts_ext = max(pts_count, out_stat.max_pts_ext)
        out_stat.pts_ext += pts_count
        out_stat.pts += pts_count
        out_stat.polys += 1
        out_stat.polys_ext += 1
        hc = len(_poly.interiors)
        out_stat.max_holes = max(hc, out_stat.max_holes)
        out_stat.polys += hc
        out_stat.polys_int += hc
        for hole in _poly.interiors:
            hole_points_count = len(hole.coords)-1
            out_stat.max_pts_int = max(hole_points_count, out_stat.max_pts_int)
            out_stat.pts += hole_points_count
            out_stat.pts_int += hole_points_count

    for poly in poly_soup:
        if isinstance(poly, shapely.geometry.MultiPolygon):
            stat.polys_multi += 1
            stat.max_polys_multi = max(len(poly), stat.max_polys_multi)
            for subpoly in poly:
                count_poly(subpoly, stat)
        else:
            count_poly(poly, stat)
    return stat


def countPolySoup(poly_soup, count_holes=True):
    """Return total number of polygons and total number of points in a poly soup.
    Poly soup is an unordered collection of polygons.
    Parameters
    ----------
    poly_soup :
        sequence of shapely Polygon or MultiPolygon objects
    count_holes : :obj:`bool`, optional
        If true consider also points of holes
    Return
    ------
    (poly_count, pts_count)
    """
    def count_poly(_poly):
        pts_cnt = len(_poly.exterior.coords)-1
        poly_cnt = 1
        if count_holes:
            poly_cnt += len(_poly.interiors)
            for hole in _poly.interiors:
                pts_cnt += len(hole.coords)-1
        return poly_cnt, pts_cnt
    poly_count = 0
    pts_count = 0
    for poly in poly_soup:
        if isinstance(poly, shapely.geometry.MultiPolygon):
            for subpoly in poly:
                cnt = count_poly(subpoly)
                poly_count += cnt[0]
                pts_count += cnt[1]
        else:
            cnt = count_poly(poly)
            poly_count += cnt[0]
            pts_count += cnt[1]
    return poly_count, pts_count


def countPolysInPolySoup(poly_soup, count_holes=True):
    return countPolySoup(poly_soup, count_holes)[0]


def countPointsInPolySoup(poly_soup, count_holes=True):
    return countPolySoup(poly_soup, count_holes)[1]


def getPointsOfPolySoup(poly_soup, with_holes=True):
    """Return array of point in poly soup.
    Poly soup is an unordered collection of polygons.
    Parameters
    ----------
        poly_soup : sequence of shapely Polygon or MultiPolygon objects
    Return
    ------
        :obj:`ndarray`
    """
    i = 0
    count = countPointsInPolySoup(poly_soup, count_holes=with_holes)
    pts = numpy.zeros((count, 3), dtype=numpy.float64)


    def set_pts(_poly):
        nonlocal i
        nonlocal pts
        dim = len(_poly.exterior.coords[0])
        for k in range(len(_poly.exterior.coords)-1):
            pts[i, 0:dim] = _poly.exterior.coords[k]
            i += 1
        if with_holes:
            for hole in _poly.interiors:
                for k in range(len(hole.coords)-1):
                    pts[i, 0:dim] = hole.coords[k]
                    i += 1
    for poly in poly_soup:
        if isinstance(poly, shapely.geometry.MultiPolygon):
            for subpoly in poly:
                set_pts(subpoly)
        else:
            set_pts(poly)
    return pts


def getPolySoupData(poly_soup, with_holes=True):
    """
    Return poly data as a dictionary of arrays and lists.

    Point data : [polygon index, internal/external]
    Cell data : [multipoly index, internal/external]
    """
    numb_polys, numb_pts = countPolySoup(poly_soup, count_holes=with_holes)
    pts = numpy.zeros((numb_pts, 3), dtype=numpy.float64)
    pts_data = numpy.zeros((numb_pts, 2), dtype=numpy.int)
    polys_data = numpy.zeros((numb_polys, 2), dtype=numpy.int)

    poly_data = munch.Munch(pts=pts, pts_data=pts_data, polys=[], polys_data=polys_data)
    pt_idx = 0
    poly_idx = 0

    def handle_poly(poly, data, multi_idx=0):
        nonlocal pt_idx
        nonlocal poly_idx
        poly_pts_idx = []
        for k in range(len(poly.exterior.coords)-1):
            data.pts[pt_idx, 0:2] = poly.exterior.coords[k][0:2]
            data.pts_data[pt_idx, 0] = poly_idx
            data.pts_data[pt_idx, 1] = 1
            poly_pts_idx.append(pt_idx)
            pt_idx += 1
        data.polys_data[poly_idx, 1] = 1
        data.polys_data[poly_idx, 0] = multi_idx
        data.polys.append(poly_pts_idx)
        poly_idx += 1
        if with_holes:
            for hole in poly.interiors:
                poly_pts_idx = []
                for k in range(len(hole.coords)-1):
                    data.pts[pt_idx, 0:2] = hole.coords[k][0:2]
                    data.pts_data[pt_idx, 0] = poly_idx
                    data.pts_data[pt_idx, 1] = -1
                    poly_pts_idx.append(pt_idx)
                    pt_idx += 1
                data.polys_data[poly_idx, 1] = -1
                data.polys_data[poly_idx, 0] = multi_poly_idx
                data.polys.append(poly_pts_idx)
                poly_idx += 1
    multi_poly_idx = 0
    for ply in poly_soup:
        if isinstance(ply, shapely.geometry.MultiPolygon):
            multi_poly_idx += 1
            for subpoly in ply:
                handle_poly(subpoly, poly_data, multi_poly_idx)
        else:
            handle_poly(ply, poly_data)
    return poly_data


def writeVtkFromPolySoup(filename, poly_soup, with_holes=True):
    """Save polygonal soup in VTK legacy format
    """
    data = getPolySoupData(poly_soup, with_holes=with_holes)
    with open(filename, 'w') as fhandle:
        fhandle.write('# vtk DataFile Version 4.1\n')
        fhandle.write('written by genfemcalc\n')
        fhandle.write('ASCII\n')
        fhandle.write('DATASET POLYDATA\n')
        fhandle.write(f"POINTS {data.pts.shape[0]} double\n")
        numpy.savetxt(fhandle, data.pts, delimiter=' ', newline='\n')
        numb_polys = len(data.polys)
        cells_size = data.pts.shape[0]+numb_polys
        fhandle.write(f"\nPOLYGONS {numb_polys} {cells_size}\n")
        for poly in data.polys :
            fhandle.write("%d %s\n" % (len(poly), ' '.join([str(x) for x in poly])))
        fhandle.write(f"POINT_DATA {data.pts.shape[0]}\n")
        fhandle.write('SCALARS point_data int 2\n')
        fhandle.write('LOOKUP_TABLE default\n')
        data.pts_data.tofile(fhandle, sep=' ')
        fhandle.write(f"\nCELL_DATA {numb_polys}\n")
        fhandle.write('SCALARS cell_data int 2\n')
        fhandle.write('LOOKUP_TABLE default\n')
        data.polys_data.tofile(fhandle, sep=' ')
