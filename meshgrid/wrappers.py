# -*- coding: utf-8 -*-
"""Define mesh wrappers and mesh protocol.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019-10-27 01:19:53.171581'
__version__ = '0.2.0'

import abc

from genfemcalc import meshgrid


class MeshProtocol(abc.ABC):
    """Create
    """
    def __init__(self):
        super().__init__()

    @property
    @abc.abstractmethod
    def numb_nodes(self):
        pass

    @property
    @abc.abstractmethod
    def numb_elems(self):
        pass

    @property
    @abc.abstractmethod
    def numb_cells(self):
        pass

    @property
    @abc.abstractmethod
    def points(self):
        pass

    @property
    @abc.abstractmethod
    def unique_cell_types(self):
        pass

    @property
    @abc.abstractmethod
    def cells_family(self):
        pass

    @property
    @abc.abstractmethod
    def cell2type(self):
        pass

    @property
    @abc.abstractmethod
    def elem_fields(self):
        pass

    @property
    @abc.abstractmethod
    def node_fields(self):
        pass

    @abc.abstractmethod
    def getCellDim(self, cell_id):
        pass

    @abc.abstractmethod
    def getCellNodes(self, cell_id):
        pass

    def getCellPoints(self, cell_id):
        return self.points[self.getCellNodes(cell_id), :]

    def cell_data(self):
        return dict()

    def cells(self):
        for i in range(self.numb_cells):
            yield i

    def nodes(self):
        for i in range(self.numb_nodes):
            yield i


class MrgMesh(MeshProtocol):
    __cells_family = meshgrid.refcells.CellFamily.VTK

    def __new__(cls, mesh=None, **kwargs):
        if isinstance(mesh, meshgrid.wrappers.MrgMesh):
            return mesh
        else:
            return super(MrgMesh, cls).__new__(cls)

    def __init__(self, mesh):
        if self is not mesh:
            super().__init__()
            if isinstance(mesh, meshgrid.wrappers.MeshProtocol):
                self._mesh = mesh.mesh
            else:
                self._mesh = mesh
            self.elem2nodes = meshgrid.adjacency.Adjacency.fromMesh(self._mesh)

    @classmethod
    def wrap(cls, mesh):
        if isinstance(mesh, cls):
            return mesh
        else:
            return cls(mesh)

    @property
    def mesh(self):
        return self._mesh

    @property
    def node_coords(self):
        return self._mesh.node_coords

    @property
    def cells_family(self):
        return self.__cells_family

    @property
    def numb_elems(self):
        return self.mesh.numb_elems

    @property
    def numb_cells(self):
        return self.mesh.numb_elems

    @property
    def numb_nodes(self):
        return self.mesh.numb_nodes

    @property
    def points(self):
        return self.mesh.node_coords

    @property
    def cell2type(self):
        """:obj:`ndarray`: vector of numeric cell type id.
        """
        return self.mesh.elem_geotype

    @property
    def elem_geotype(self):
        return self.mesh.elem_geotype

    @property
    def unique_cell_types(self):
        """:obj:`ndarray`: vector of cell types ids present in the mesh
        """
        return self.mesh.unique_elem_types

    @property
    def cell_data(self):
        """:obj:`dict` of :obj:`ndarray`: dictionary of element data arrays
        It is an alias to ``elem_fields`` property
        """
        return self._mesh.elem_fields

    @property
    def elem_fields(self):
        """:obj:`dict` of :obj:`ndarray`: dictionary of element data arrays
        """
        return self._mesh.elem_fields

    @property
    def node_fields(self):
        """:obj:`dict` of :obj:`ndarray`: dictionary of node data arrays
        """
        return self._mesh.node_fields

    def getCellDim(self, cell_id):
        return meshgrid.ElemType(self.elem_geotype[cell_id]).dim

    def getCellNodes(self, cell_id):
        return self.elem2nodes[cell_id]

    def evalNodeField(self, field_name, expression, overwrite=False, store=True):
        return self._mesh.evalNodeField(field_name, expression, overwrite=overwrite,
                                        store=store)

    def addNodeField(self, field_name, data_array, overwrite=False, copy=False):
        return self._mesh.addNodeField(field_name, data_array, overwrite=overwrite, copy=copy)

    def addElemField(self, field_name, data_array, overwrite=False, copy=False):
        return self._mesh(field_name, data_array, overwrite=overwrite, copy=copy)

    @property
    def ambient_dim(self):
        """:obj:`int` : (read only) dimension of the mesh ambient space.

        The ambient space is the physical space the in which mesh 'lives'.
        The dimension of ambient space is equal to the number of coordinates
        of mesh points.
        """
        return self._mesh.space_dim
