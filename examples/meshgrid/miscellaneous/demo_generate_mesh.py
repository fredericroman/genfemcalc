# -*- coding: utf-8 -*-
"""
Generate mesh
=============

"""

import os

import genfemcalc
from genfemcalc import meshgrid
from genfemcalc import examples

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoMeshRectangle(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        # -- Meshing domain
        context = {'xa': 2.0,
                   'xb': 4.0,
                   'ya': 1.0,
                   'yb': 3.0,
                   'quadsonly': False,
                   'edge_size': 0.3}

        generator = meshgrid.modelsgmsh.Rectangle()
        domain = generator.discretize(context)
        regions = domain.regions

        print('Model regions:')
        for region in domain.regions:
            print(region)

        # Wrapping in MRG
        mesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh)

        mesh.listElemCount()

        print('Boundary regions:')
        i = 0
        for region in regions:
            if region.dim == 1:
                submesh = meshgrid.utilsgmsh.getSubmesh(domain.mesh, regions=(region,))
                mrg_mesh_bc = meshgrid.iomrg.buildMrgFromGmsh(submesh, ['line'])
                vtk_mesh_bc = meshgrid.iomrg.buildVtkFromMrg(mrg_mesh_bc)
                print('Number of nodes in VTK mesh', vtk_mesh_bc.GetNumberOfPoints())
                i += 1
                file_name = os.path.join(OUTDIR, f"boundary_part{i}.vtk")
                meshgrid.iomrg.writeVtkFromMrg(mrg_mesh_bc, file_name, binary=False)


if __name__ == '__main__':
    demo = DemoMeshRectangle()
    demo.run()
