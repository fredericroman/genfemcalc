.. include:: ../../replaces.txt
.. _meshing-sec:

********************
Operations on meshes
********************

Mesh as data structure
----------------------

Classical mesh data structure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Extending mesh adjacency structure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Purely geometric operations
---------------------------

Purely combinatorial operations
-------------------------------

Finding mesh bounding box
-------------------------

Affine transformations
----------------------

Approximated area and surface
-----------------------------


Combinatorial structure of reference cell
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Inverting adjacency relation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Enumerating mesh components
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Finding mesh boundaries
^^^^^^^^^^^^^^^^^^^^^^^

Handling mesh regions
^^^^^^^^^^^^^^^^^^^^^

General operations
------------------

Geometric embeddings of reference cells
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Interfacing other mesh-like data structures
-------------------------------------------

Mesh protocol
^^^^^^^^^^^^^

Mesh wrappers
^^^^^^^^^^^^^





