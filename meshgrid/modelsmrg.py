# -*- coding: utf-8 -*-
"""The purpose of this module is to provide functions to generate some
simple mesh models.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.10.15 20:21:38'
__version__ = '0.10.0'

# Python packages
import numpy as np 
import scipy.io as sio
import sys

# MRG packages
from genfemcalc import meshgrid


def buildCubeMesh(lbf, rtb, cell_resolution=(1, 1, 1)):
    """
    Parameters
    ----------
    lbf : :obj:`array like`
        left-bottom-front corner
    rtb: :obj:`array like`
        right-top-back corner
    cell_resolution : :obj:`array like`
        number of cells in X,Y,Z directions
    """
    cellres = np.asarray(cell_resolution)
    noderes = cellres+1
    numb_elems, numb_nodes = np.product((cellres, noderes), axis=1)
    mesh = meshgrid.Mesh(numb_nodes, numb_elems, elem_type=meshgrid.ElemType.HEXAHEDRON)
    xyz = [None, None, None]
    for i in (0, 1, 2):
        xyz[i] = np.linspace(lbf[i], rtb[i], noderes[i], dtype=np.float64)
    xx, yy, zz = np.meshgrid(*xyz, indexing='ij')
    mesh.node_coords[:, :] = np.c_[xx.flatten(), yy.flatten(), zz.flatten()]
    cell_nodes = np.array([[0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0],
                           [0, 0, 1], [1, 0, 1], [1, 1, 1], [0, 1, 1]])
    nodes = np.empty((8,), dtype=np.int64)
    for cell_index in range(numb_elems):
        ijk = np.unravel_index(cell_index, cellres, order='F')
        for p, local_node in enumerate(cell_nodes):
            nodes[p] = np.ravel_multi_index(local_node + ijk, noderes)
        cs, ce = mesh.p_elem2nodes[cell_index:cell_index+2]
        mesh.elem2nodes[cs:ce] = nodes
    return mesh

def buildRectangle(xmin,xmax,ymin,ymax,nx,ny):
    """This function builds a mesh of a rectangle with quad finite
    elements. This mesh is stored in an unstructured mesh structure
    even if the mesh is composed with regular quad finite elements.
    The nodes and elements of this mesh are numbered rows by rows
    from the bottom left corner to the top right corner.    
    """
    out_mesh = meshgrid._meshmrg.Mesh()
    
    out_mesh.space_dim = 3
    out_mesh.numb_nodes = (nx + 1) * (ny + 1)
    out_mesh.node_coords= np.empty((out_mesh.numb_nodes, out_mesh.space_dim))

    node_per_elem = 4
    out_mesh.numb_elems = (nx * ny)
    out_mesh.p_elem2nodes = np.empty((out_mesh.numb_elems + 1,), dtype='int64')
    out_mesh.p_elem2nodes[0] = 0
    for i in range(0, out_mesh.numb_elems):
        out_mesh.p_elem2nodes[i + 1] = out_mesh.p_elem2nodes[i] + node_per_elem
    out_mesh.elem2nodes = np.empty((out_mesh.numb_elems * node_per_elem,), dtype='int64')
    out_mesh.elem_geotype = np.empty((out_mesh.numb_elems,), dtype='int')
    
    # nx*ny quad elements and (nx+1)*(ny+1) nodes of cartesian grid
    # quad elements contains nodes: (i,j), (i+1,j), (i+1,j+1), (i,j+1)
    k = 0
    kk = 0
    for j in range(0,ny):
        for i in range(0,nx):
            out_mesh.elem2nodes[k + 0] = (j) * (nx + 1) + i
            out_mesh.elem2nodes[k + 1] = (j) * (nx + 1) + i + 1
            out_mesh.elem2nodes[k + 2] = (j + 1) * (nx + 1) + i + 1
            out_mesh.elem2nodes[k + 3] = (j + 1) * (nx + 1) + i
            out_mesh.elem_geotype[kk] = meshgrid._meshmrg.ElemType.QUAD
            k += 4
            kk += 1

    # coordinates of nodes of the cartesian grid
    k = 0
    for j in range(0,ny+1):
        yy = ymin+((j)*(ymax-ymin)/ny)
        for i in range(0,nx+1):
            xx = xmin+((i)*(xmax-xmin)/nx)
            out_mesh.node_coords[k,:] = xx, yy, 0.0
            k += 1

    # local to global numbering
    out_mesh_l2g = np.arange(0, out_mesh.numb_nodes, 1, dtype='int64')
    return out_mesh, out_mesh_l2g


def buildRectangleBoundary(xmin,xmax,ymin,ymax,nx,ny):
    """  
    """   
    def extractBoundary(space_dim,numb_node,node_per_elem,numb_elem):
        out_mesh = meshgrid._meshmrg.Mesh()
        
        out_mesh.space_dim = space_dim
        out_mesh.numb_nodes = numb_node
        out_mesh.node_coords= np.empty((out_mesh.numb_nodes, out_mesh.space_dim))
        
        out_mesh.numb_elems = numb_elem
        out_mesh.p_elem2nodes = np.empty((out_mesh.numb_elems + 1,), dtype='int64')
        out_mesh.p_elem2nodes[0] = 0
        for i in range(0, out_mesh.numb_elems):
            out_mesh.p_elem2nodes[i + 1] = out_mesh.p_elem2nodes[i] + node_per_elem
        out_mesh.elem2nodes = np.empty((out_mesh.numb_elems * node_per_elem,), dtype='int64')
        k = 0        
        for i in range(0, out_mesh.numb_elems):
            out_mesh.elem2nodes[k + 0] = i + 0
            out_mesh.elem2nodes[k + 1] = i + 1
            k += 2
        out_mesh.elem_geotype = np.empty((out_mesh.numb_elems,), dtype='int')
        out_mesh.elem_geotype[:] = meshgrid._meshmrg.ElemType.LINE
        return out_mesh
    
    # dictionnary { key : (space_dim,numb_nodes,node_per_elem,numb_elems,fname) }
    meshspec = {'south' : (3, (nx+1), 2, (nx), '0'),
                'east' : (3, (ny+1), 2, (ny), '1'),
                'north' : (3, (nx+1), 2, (nx), '2'),
                'west' : (3, (ny+1), 2, (ny), '3')
           }
    mymeshes = []  
    for name, (space_dim,numb_node,node_per_elem,numb_elem,fname) in meshspec.items():
        mymesh = extractBoundary(space_dim,numb_node,node_per_elem,numb_elem)
        mymeshes.append(mymesh)  
        
    # coordinates of nodes of cartesian grid
    # *** TOTO: xcoord = np.linspace(xmin, xmax, nx)
    for i in range(0,nx+1):
        mymeshes[0].node_coords[i,:] = xmin+i*(xmax-xmin)/nx, ymin, 0.0
    for j in range(0,ny+1):
        mymeshes[1].node_coords[j,:] = xmax, ymin+j*(ymax-ymin)/ny, 0.0
    for i in range(0,nx+1):
        mymeshes[2].node_coords[i,:] = xmin+i*(xmax-xmin)/nx, ymax, 0.0
    for j in range(0,ny+1):
        mymeshes[3].node_coords[j,:] = xmin, ymin+j*(ymax-ymin)/ny, 0.0

    # local to global numbering
    # dictionnary { key : (space_dim,numb_nodes,node_per_elem,numb_elems,fname) }
    l2gspec = {'south': (3, (nx+1), 2, (nx), '0'),
                'east': (3, (ny+1), 2, (ny), '1'),
                'north': (3, (nx+1), 2, (nx), '2'),
                'west': (3, (ny+1), 2, (ny), '3')
           }
    myl2gs = []  
    k = 0
    for name, (space_dim,numb_node,node_per_elem,numb_elem,fname) in l2gspec.items():
        myl2g = np.empty((mymeshes[k].numb_nodes,), dtype='int64')
        myl2gs.append(myl2g) 
        k += 1
    for i in range(0,nx+1):
        myl2gs[0][i] = i
    for j in range(0,ny+1):
        myl2gs[1][j] = (j)*(nx+1)+nx
    for i in range(0,nx+1):
        myl2gs[2][i] = (ny)*(nx+1)+i
    for j in range(0,ny+1):
        myl2gs[3][j] = (j)*(nx+1)+0
    return mymeshes, myl2gs
