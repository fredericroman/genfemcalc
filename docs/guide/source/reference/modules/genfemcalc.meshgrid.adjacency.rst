genfemcalc.meshgrid.adjacency module
====================================

.. automodule:: genfemcalc.meshgrid.adjacency
   :members:
   :undoc-members:
   :show-inheritance:
