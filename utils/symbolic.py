# -*- coding: utf-8 -*-
"""
Module to make make utilisation of symbolic functions easier
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/24/2019'
__version__ = '0.1.0'

import sympy
import numpy

def lambdify_wrapper(symbols, expression):
    """Specialized lambdify routine avoiding the unused argument issue.
    A common sympy/python lambda expression 'f = lambda x: 2' has the
    problem that f(x) returns a float 2 even if x was a numpy array.
    Thus we loose all information on the shape of x.
    """
    g = sympy.lambdify(symbols, expression, "numpy")
    if len(expression.free_symbols) == 0:
    #if not expression.is_symbolic():
        return lambda *x: numpy.repeat(g(*x)[..., numpy.newaxis], len(x[0]), axis=2)
    return g
    #def broadcast(fun):
    #    return lambda *x: numpy.broadcast_arrays(fun(*x), *x)[0]
    # return broadcast(g)
