# -*- coding: utf-8 -*-
"""
Show bounding box of rotated mesh
---------------------------------

In this demo as single element mesh is crated, then rotated and final its 
bounding box is calcualated and visualised along with the mesh.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/23/2019'
__version__ = '0.1.0'

import os

from genfemcalc import examples
from genfemcalc import meshgrid
from genfemcalc.meshgrid import graphics
from genfemcalc import benchmarks

PKGDIR = os.path.dirname(meshgrid.__file__)
DATADIR = os.path.abspath(os.path.join(PKGDIR, '..', 'test', 'data'))


class DemoShowBoundingBox(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        mesh = benchmarks.meshes.getSingleQuadMesh()
        meshgrid.geometrymrg.rotate_z(mesh, center=(1.0, 1.0), angle=65.0, unit='deg', inplace=True)

        box = meshgrid.geometrymrg.BoundingBox.fromMrgMesh(mesh)

        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerMesh(mesh, 'single')
        viewer.show('single', facecolor='yellow', edgecolor='black')
        viewer.showBox(box, facecolor='none', edgecolor='red')
        viewer.keep()

if __name__ == '__main__':
    demo = DemoShowBoundingBox()
    demo.run()
