# -*- coding: utf-8 -*-
"""
Mesh generation
===============

"""
__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.04  19:18:53'
__version__ = '0.1.0'

import os
import sys
import meshio
import scipy.io as sio

import genfemcalc
from genfemcalc.meshgrid import iomrg

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


def to_fixme():
    # -- Meshing domain
    generator = models_factory.Rectangle()
    domain = generator.discretize(context)
    meshgmsh = domain.mesh
    print(domain.regions)
    
    # Saving in VTK
    fname = 'rectangle.vtk'
    meshio.write(fname, meshgmsh, file_format='vtk-ascii')
    
    # Wrapping in MRG
    #mymesh = iomrg.MrgFromPyGmsh(domain.mesh,['triangle','line'])
    mymesh = iomrg.MrgFromPyGmsh(domain.mesh,['triangle'])
    mymesh.listElemsCount(mymesh)
    print('mymesh : ',mymesh.numb_nodes)
    print('mymesh : ',mymesh.numb_elems)
    
    #mymesh2 = iomrg.ReadMrgFromVtk(fname)
    #mesh_utils.ListElemsCount(mymesh2)
    
    # Extracting boundary (manually)
    submeshSelector = {'id' : 2, 'type' : 'line'}
    [submeshgmsh, l2g ]= pygmsh_utils.GetSubmesh(meshgmsh, submeshSelector)
    mymeshbc = iomrg.buildMrgFromPyGmsh(submeshgmsh,['line'])
    mymeshbc.listElemsCount(mymeshbc)
    print('mymeshbc : ', mymeshbc.numb_nodes)
    print('mymeshbc : ', mymeshbc.numb_elems)
    
    fname = 'rectangle_south_north.vtk'
    meshio.write(fname, submeshgmsh, file_format='vtk-ascii')
    
    #l2gbis = numpy.array([ range(len(l2g)), l2g])
    #print('l2gbis : ',l2gbis)
    #sio.mmwrite(fname,l2gbis.transpose())
    
    # -- Moving one node of the boundary (manually)
    
    mymesh.node_coords[subl2g[5],0] -= 0.2
    mymesh.node_coords[subl2g[5],1] -= 0.2
    mymesh.node_coords[subl2g[5],2] -= 0.2
    
    mymesh.listElemCount()
    
    mymeshbc.node_coords[5,0] -= 0.2
    mymeshbc.node_coords[5,1] -= 0.2
    mymeshbc.node_coords[5,2] -= 0.
    fname = 'rectangle_south_north_after.vtk'
    iomrg.writeVtkFromMrg(mymeshbc, fname, binary=False)
    
    # -- Re-meshing domain
    






