.. include:: ../../replaces.txt
.. _intro-sec:

************
Introduction
************

Genfemcalc is package providing basic support for geometric  modelling,
mesh generation, element calculations and equations assembly, particularly
in the context of Finite Element Method (FEM). Genfemcalc is implemented in
Python on top of several packages, among them  Numpy, Scipy, Sympy, PyGmsh
meshio, quadpy, Matplotlib and Python bindings for VTK.

Genfemcalc is itself a part of bigger package  PyMRG but it can be used as
independent package and in this guide it is treated as stand-alone one.

Motivation
==========

Why to implement yet another FEM package if there are so many of them already
implemented? This is simple yet very important question because it touches the
issue of efficacy and role of research work, especially in academic context.

|Genfemcalc| can be classified as YAFEM package - with YAFEM standing for
Yet Another Finite Element Method. Thus quit rightly one may ask why don't we
grab some existing FEM software instead of putting effort into writing our own?
Isn't it the waste of resources? Well, while the question is straight the answer
is not so simple and has technical, contextual, and psychological dimensions [#nih]_. To
sketch the answer we should look at the question what we need FEM package for.
While there might be plenty of motivations, generally they fal into three categories

1. we need to perform some FEM analysis;
2. we need to understand how things work;
3. we would like to develop new tools on top of FEM or to experiment with some new
   ideas within FEM itself.

In the first case we are interested first of all in the simulation results and not
as much as in details how they were obtained. Of course we do not prise or advocate
"black box" approach to any numerical simulations. What we mean in this case
is that as long as correctness of efficiency of the simulations are not affected
we do not care how things in FEM package are implemented

In the second case we want to learn how thins work in order to match a theory
behind calculations, that is world of mathematics, with the way things are
handled in computational world (mostly in numerical methods). Thus we are interested
in performing simple to medium difficulty simulations and are interested in being
able to track them and if necessary in looking under the hood. We may want to stude
the code or even to modify it as some places to enhance our insight.

In the third case we are interested in developing new ideas. We may want to
modify the software in substantial way and most of the time we need full
control over the source code. We are interested in the results of specific
simulations often only as much as they can be used to verify the correctness
of our algorithmic development.

Having sketched three general profiles of potential users let us confront them
with the issue of taking ready software versus building a custom one. Of course
this is to large extent crude generalisation because "software != software" and
each application and library has a specific characteristics. Nevertheless speaking
from our experience we observe that the more mature the software package the more
robust and efficient it is but the same time software transparency decreases with
time. As the software matures the bugs are fixed and performance bottlenecks
are fixed but the same time the code grows with layers, special cases, etc. To keep
initial design clarity, the design have to be really good to seamlessly accommodate
features and patches. The documentation (if present) can improve with time, but
this is most of the time user documentation. The ideas behind design and especially
behind implementation are seldom documented, the most of the time remain a tactic
knowledge of the developers. In case of commercial software, such knowledge if
writen down is often carefully guarded as the key asset of the company.
Besides it is needles to say that documenting software is hard endeavour, eating a lot
of resources. If we define software transparency as the amount of effort to see
the motives behind particular implementation or design, then it is rather no wonder
that with time the transparency decreases. The above pertain to third party software.
On the other hand if we develop software ourselves then the chances are we will
quite long remember why we have written it in particular way. We say there are chances
because this also depends how much effort we put into documentation and how good
are we at remembering things.

From the above discussion we can drive not surprising conclusion that the first
group of user will more likely go for third party software while the third will
develop their own. We classify ourselves as the third group and after considering
various factors we decided we will be better off with writing our own package.
So Genfemcalc was born.

Package organisation
====================

Genfemcalc is divided into couple of smaller packages grouped more or less thematically
The are:

  * fem - package for calculations related to meshes and mesh elements. This inclueds
          interpolation in elements, calculation of forms in elements, integration of
          fields over meshes, etc.
  * meshgrid - package for handling geometric modeling and mesh generation and manipulation
  * msm - utilities for Manufactured Solution Method, used for code veryfication
  * examples - packages with demonstration code
  * test - packages for unit testing

On the directories level the structure of Genfemcalc is illustrated in Listing

.. code-block:: text
   :caption: Structure of Genfemcalc directories

     genfemcalc
      |
      ├── docs
      │   ├── guide
      │   └── notebooks
      ├── examples
      ├── fem
      ├── meshgrid
      │   └── graphics
      ├── testing
      ├── viewers
      └── test
          └── data

.. rubric:: Footnotes

.. [#nih] The example of cultural/psychological issue is NIH syndrome -
   Not Implemented/Invented Here.
