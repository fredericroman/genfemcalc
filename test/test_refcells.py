import unittest
from meshgrid import refcells 
import numpy as np

class TestCellFamily(unittest.TestCase):
    def test_convert_from_str(self):
        family = refcells.CellFamily.convert('vtk')
        self.assertEqual(family, refcells.CellFamily.VTK, 
                'Family shoul be VTK')
        family = refcells.CellFamily.convert('gmsh')
        self.assertEqual(family, refcells.CellFamily.GMSH, 
                'Family should be GMSH')
    def test_convert_from_int(self):
        family = refcells.CellFamily.convert(1)
        self.assertEqual(family, refcells.CellFamily.VTK, 
                'Family shoul be VTK')
        family = refcells.CellFamily.convert(0)
        self.assertEqual(family, refcells.CellFamily.GMSH, 
                         'Family should be GMSH')

    def test_convert_from_CellFamily(self):
        family = refcells.CellFamily.convert(refcells.CellFamily.VTK)
        self.assertEqual(family, refcells.CellFamily.VTK, 
                         'Family shoul be VTK')
        family = refcells.CellFamily.convert(refcells.CellFamily.GMSH)
        self.assertEqual(family, refcells.CellFamily.GMSH, 
                         'Family should be GMSH')

    def test_convert_from_sequence(self):
        fam = refcells.CellFamily.VTK
        family = refcells.CellFamily.convert(('vtk', 'gmsh', fam))
        self.assertEqual(family[0], refcells.CellFamily.VTK, 
                         'Family should be VTK')
        self.assertEqual(family[1], refcells.CellFamily.GMSH, 
                         'Family should be GMSH')
        self.assertEqual(family[2], refcells.CellFamily.VTK, 
                         'Family should be VTK')

class TestLinearLineRefCell(unittest.TestCase):
    def setUp(self):
        self.cell = refcells.getRefCell('gmsh', 1)

    def test_dim(self):
        self.assertEqual(self.cell.dim, 1, 
            f"Line dimension should be 1 got {self.cell.dim}")

    def test_nodes(self):
        expected_nodes = np.array([[0,0,0], [1,0,0]]);
        self.assertTrue(np.allclose(self.cell.points, expected_nodes),
                        "Node coords are not as expected")

    def test_shape_functions(self):
        n_points = 5
        x = np.linspace(0.0, 1.0, n_points)
        nodes = x.reshape(n_points, 1)
        expected_values = np.array([1-x, x]).T
        actual_values = self.cell.N(nodes)
        self.assertTrue(np.allclose(actual_values, expected_values))
        self.assertEqual(actual_values.shape, (n_points, self.cell.numb_nodes))

    def test_shape_functions_derivatives(self):
        n_points = 5
        x = np.linspace(0.0, 1.0, n_points)
        nodes = x.reshape(n_points, 1)
        expected_values = np.array([[[-1, 1]],
                                    [[-1, 1]],
                                    [[-1, 1]],
                                    [[-1, 1]],
                                    [[-1, 1]]])
        actual_values = self.cell.dN(nodes)
        self.assertTrue(np.allclose(actual_values, expected_values))
        expected_shape = (n_points, self.cell.dim, self.cell.numb_nodes)
        self.assertEqual(actual_values.shape, expected_shape)

class TestLinearQuadRefCell(unittest.TestCase):
    def setUp(self):
        self.cell = refcells.getRefCell('gmsh', 3)

    def test_dim(self):
        self.assertEqual(self.cell.dim, 2,
                         f"Quad dimension should be 2 got {self.cell.dim}")

    def test_nodes(self):
        expected_nodes = np.array([[0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0]])
        self.assertTrue(np.allclose(self.cell.points, expected_nodes),
                        "Node coords are not as expected")

    def test_shape_functions(self):
        n_points = 5
        x = np.linspace(0.0, 1.0, n_points)
        y = np.linspace(0.0, 1.0, n_points)
        nodes = np.array([x, y]).T
        expected_values = np.array([x*y - x - y + 1, -x*y + x, x*y, -x*y + y]).T
        actual_values = self.cell.N(nodes)
        self.assertTrue(np.allclose(actual_values, expected_values))
        self.assertEqual(actual_values.shape, (n_points, self.cell.numb_nodes))

    def test_shape_functions_derivatives(self):
        n_points = 5
        x = np.linspace(0.0, 1.0, n_points)
        y = np.linspace(0.0, 1.0, n_points)
        nodes = np.array([x, y]).T
        expected_values = np.moveaxis(np.array([[y - 1, x - 1],
                                                [1 - y, -x], 
                                                [y, x], 
                                                [-y, 1 - x]]),[2, 0], [0, 2])
        actual_values = self.cell.dN(nodes)
        self.assertTrue(np.allclose(actual_values, expected_values))
        expected_shape = (n_points, self.cell.dim, self.cell.numb_nodes)
        self.assertEqual(actual_values.shape, expected_shape)

if __name__ == '__main__':
    unittest.main()
