# -*- coding: utf-8 -*-
"""
Scaled assembly
---------------
"""

import os
import numpy
import matplotlib.pyplot as plt
import scipy.sparse
import timeit

from genfemcalc import benchmarks
from genfemcalc import examples
from genfemcalc import meshgrid
from genfemcalc import fem
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoScaledAssembly(examples.Demo):
    def demonstrate(self):
        filepath = os.path.join(DATADIR, 'three_segments.vtk')
        mrgmesh = meshgrid.iomrg.readMrgFromVtk(filepath)
        integrator = fem.meshcalc.MeshIntegrator(mrgmesh, ambient_dim=2, ngpt=4)

        mass_term = fem.elemcalc.MassTerm(integrator, name='M')
        assembler = fem.assembly.TermAssembler(mass_term, dtype=numpy.float64)

        assembler.preassemble()
        storage = 'dense'
        for elem in range(mrgmesh.numb_elems):
            m = assembler.getPreassembled('M', elem)
            print(f"Element matrix for element {elem}")
            print(m)
        factors = numpy.array([6, 12, 18])
        M = assembler.assemble('M', storage, factors=factors)
        print(M)

if __name__ == '__main__':
    demo = DemoScaledAssembly()
    demo.run()
