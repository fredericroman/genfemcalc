"""
Poisson routine for quad element
--------------------------------
"""
import os
import numpy

from genfemcalc import benchmarks
from genfemcalc import examples
from genfemcalc import fem
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class PoissonElementRoutineForQuadfem(object):
    def __init__(self, source_term):
        self.source_term = source_term

    def __call__(self, mesh, elem, edofs, dtype):
        if mesh.elem_geotype[elem] == meshgrid.ElemType.QUAD:
            dim = 3  # put 2 for 2D -> 2D
            nodes = mesh.node_coords[edofs, 0:dim]
            D = numpy.identity(dim, dtype='float64')
            K = genfemcalc.fembasic.quadfem.calcEllipticTerm(nodes, ngpt=4, constitutive_mat=D)
            f = self.source_term[edofs]
            F = genfemcalc.fembasic.quadfem.calcSourceTerm(nodes, ngpt=4, f=f)
            return K, F
        else:
            return None


class DemoPoissonProblemQuadfem(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        solution = benchmarks.exact_solutions.alpha_one
        problem = benchmarks.problems.PoissonInRectangle(solution)
        viewer = meshgrid.graphics.MeshViewer()

        domain = problem.getDomain(edge_size=0.05, quadsonly=True)

        mrgmesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh)

        # build boundary condition map for Dirichlet boundary conditions
        # this map maps nodes (dofs)  to their values.
        bc_map = dict()
        for region_name in ['b_east', 'b_north', 'b_west']:
            ids = meshgrid.utilsgmsh.getRegionsNodesIds(domain.mesh, names=(region_name,))
            points = mrgmesh.node_coords[ids, :]
            values = problem.evalBoundaryTermOn('dirichlet', region_name, points)
            bc_map.update(dict(zip(ids, values)))

        source_term = problem.evalSourceTermOn(mrgmesh.node_coords)

        element_routine = PoissonElementRoutineForQuadfem(source_term)

        assembler = fem.assembly.MrgAssembler(mrgmesh, dtype=numpy.complex128)
        assembler.preassembleWithDirichletBC('K', 'F', element_routine, bc_map)
        K = assembler.assemble('K', 'dense')
        F = assembler.assemble('F', 'dense')
        x = numpy.linalg.solve(K,F)
        mrgmesh.addNodeField('solution', x)
        outfile = os.path.join(OUTDIR, 'poisson_solution_quadfem.vtk')
        meshgrid.iomrg.writeVtkFromMrg(mrgmesh, outfile)


if __name__ == '__main__':
    demo = DemoPoissonProblemQuadfem()
    demo.run()
