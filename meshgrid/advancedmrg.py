# -*- coding: utf-8 -*-
"""This module contains the mesh class.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.13 11:19:46'
__version__ = '0.10.0'

# Python packages
import numpy as np
from genfemcalc import meshgrid


class MeshAdv(meshgrid.Mesh):
    def __init__(self):
        super().__init__(self)

        self.node2elem = np.array([0], dtype='int64')
        self.p_node2elem = np.array([0], dtype='int64')

        self.node2subdom = np.array([0], dtype='int64')
        self.p_node2subdom = np.array([0], dtype='int64')
        return


    def buildNode2Elem(self):
        temp = meshgrid.adjacency.Adjacency.fromMesh(self)
        self.node2elem = temp.adj
        self.p_node2elem = temp.p_adj
        return


    def buildElem2Elem(self):
        temp = meshgrid.adjacency.DynamicAdjacency.fromMesh(self)
        temp2 = temp.getCell2Cell()
        self.elem2elem = temp2.adj
        self.p_elem2elem = temp2.p_adj
        return