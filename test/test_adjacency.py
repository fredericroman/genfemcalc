"""Unit tests of adjacency facilities
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/6/2019'
__version__ = '0.1.0'

import unittest
from meshgrid import adjacency


class TestMapOnGlobalIds(unittest.TestCase):
    def setUp(self):
        self.edges = [[0, 1], [1, 2], [2, 3], [3, 0]]
        self.nodes = [10, 11, 12, 13]
        pass

    def test_mapping_edges(self):
        mapped_edges = adjacency.mapOnGlobalIds(self.edges, self.nodes)
        self.assertEqual(mapped_edges, [[10, 11], [11, 12], [12, 13], [13, 10]])

    def test_mapping_edges_as_set(self):
        mapped_edges = adjacency.mapOnGlobalIdsAsSet(self.edges, self.nodes)
        self.assertEqual(mapped_edges, [set([10, 11]), set([11, 12]), set([12, 13]), set([13, 10])])
