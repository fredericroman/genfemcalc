"""
The purpose of this package is to provide tools for finite
element assembly.

Created on Fri Aug  9 02:08:42 2019

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.08.17  12:43:19'
__version__ = '0.1.0'

import numpy as np
import itertools
import collections
import scipy.sparse

from genfemcalc import meshgrid

try:
    import alinea
except ImportError:
    _HAVE_ALINEA = False
else:
    _HAVE_ALINEA = True


class ScaledRangeArray(np.ndarray):
    """This is wrapper on 1D ndarray class that allows to scale continuous ranges
    of elements by given factor. This class is used in assembly to allow scalling
    assembled matrices
    """
    def __new__(cls, input_array, borders=None, factors=None):
        obj = np.asarray(input_array).view(cls)
        obj.borders = borders
        obj.factors = factors
        return obj

    def __array_finalize__(self, obj):
        if obj is None:
            return
        self.borders = getattr(obj, 'borders', None)
        self.factors = getattr(obj, 'factors', None)

    def __repr__(self):
        a = np.asarray(self)
        return a.__repr__()

    def __str__(self):
        a = np.asarray(self)
        return a.__str__()

    def __getitem__(self, index):
        val = super().__getitem__(index)
        if self.borders is None or self.factors is None:
            return val
        else:
            if isinstance(index, int):
                rng_idx = np.searchsorted(self.borders, index, side='right')
                return val*self.factors[rng_idx-1]
            elif isinstance(index, slice):
                rng_idx = np.searchsorted(self.borders, np.arange(*index.indices(self.size)),
                                          side='right')
                return val*np.take(self.factors, rng_idx-1)
            else:
                raise NotImplementedError(
                    f"Indexing for type: {type(index)} not implemented for ScaledRangeArray")

    def __copy__(self):
        newone = np.repeat(np.asarray(self.factors, dtype=self.dtype), np.diff(self.borders))
        newone *= self
        return newone

    def __deepcopy__(self):
        return self.__copy__()

    def copy(self):
        return self.__copy__()


class DirichletReducer(object):
    def __init__(self, assembler, element_routine, bc_map, factors=None):
        self._assembler = assembler
        self._routine = element_routine
        self._bc_map = bc_map
        self._factors = factors
        ndof = self._assembler.total_ndofs
        self._dof_tags = np.fromiter(map(lambda x: x in self._bc_map, range(ndof)), dtype='bool')

    def __call__(self, mesh, elem, edofs, dtype):
        arrays = self._routine(mesh, elem, edofs, dtype)
        if not arrays:
            return arrays
        if len(arrays) != 2:
            ValueError("Dirichlet BC reduction can be done only on two arrays")
        f = np.copy(arrays[1])
        K = np.copy(arrays[0])
        if self._factors is not None:
            K *= self._factors[elem]
        fixed_dofs = np.where(self._dof_tags[edofs])[0]
        if fixed_dofs.size > 0:
            cond_factor = np.trace(K)/self._assembler.total_ndofs
            fixed_vals = np.array([self._bc_map[v] for v in edofs[fixed_dofs]])
            f = f.reshape(f.size,1) - np.dot(K[:, fixed_dofs], fixed_vals).reshape(f.size,1)
            K[:, fixed_dofs] = 0.0
            K[fixed_dofs, :] = 0.0
            K[fixed_dofs, fixed_dofs] = cond_factor
            f[fixed_dofs,:] = cond_factor*fixed_vals.reshape((fixed_dofs.size,1))
        return K, f.reshape((f.size,))


class MrgAssembler(object):
    """
    MrgAssembler is object responsible for keeping data
    and executing two steps:

    1. pre-assembly - during this step storage to keep all
       elementary matrices is allocated and filled with
       values coming from elementary calculations
    2. assembly - elementary matrices are assembled into sparse
       matrix format
    """

    @property
    def qdim(self):
        """Retrun number of DOFs per node
        """
        return self._qdim

    @property
    def total_ndofs(self):
        """Return total number of dofs in the assembled matrix.
        The total number of dofs equals number of nodes times qdim
        """
        return self._total_ndofs

    @property
    def matrix_ndata(self):
        if self._matrix_ndata is None:
            self._matrix_ndata, self._vector_ndata = self.countStorageSize()
        return self._matrix_ndata

    @property
    def vector_ndata(self):
        if self._vector_ndata is None:
            self._matrix_ndata, self._vector_ndata = self.countStorageSize()
        return self._vector_ndata

    @property
    def mesh(self):
        """Return mesh over which assembly is done.
        """
        return self._mesh

    def __init__(self, mesh, qdim=1, dtype=np.float64):
        """
        Parameters
        ----------
        qdim - number of dofs per node
        """
        self._dtype = dtype
        self.row_mat = np.array([], dtype='int64')
        self.col_mat = np.array([], dtype='int64')
        self.row_vec = np.array([], dtype='int64')
        self.col_vec = np.array([], dtype='int64')
        self.data = {}
        self.p_elems_mat = np.array([], dtype='int64')
        self.p_elems_vec = np.array([], dtype='int64')
        self._qdim = qdim
        self._total_ndofs = 0
        self.__need_indices = True
        self._mesh = meshgrid.wrappers.MrgMesh(mesh)
        self._matrix_ndata = None
        self._vector_ndata = None
        self._matrix_names = set()
        self._vector_names = set()
        self._total_ndofs = self.mesh.numb_nodes * self.qdim

    def _get_element_dofs(self, elem):
        enodes = self.mesh.getCellNodes(elem)
        # get Dofs of element
        # edofs = [n+d for n in enodes for d in range(0,self.qdim)]
        # The above solution for edof uses list comprehension and loop
        # the solution below only numpy array operations.
        # Which actually is faster __SHOULD__ be tested.
        enodes_len = len(enodes)
        q = self.qdim
        # Generate array of element DOFs taking into account self.qdim
        edofs = np.repeat(enodes, q) * q + np.tile(range(q), enodes_len)
        return edofs

    def _check_overwrite(self, names, overwrite):
        already_in = set(names).intersection(self.data.keys())
        if already_in and overwrite is not True:
            raise ValueError("Data for: %s already in the AssemblyWorkspace" % already_in)

    def _preallocate_storage(self, matrix_names, vector_names):
        ndata_mat, ndata_vec = self.countStorageSize()
        if self.row_mat.size == 0:
            self.row_mat = np.empty((ndata_mat,), dtype=np.int64)
            self.col_mat = np.empty((ndata_mat,), dtype=np.int64)
            self.row_vec = np.empty((ndata_vec,), dtype=np.int64)
            self.col_vec = np.zeros((ndata_vec,), dtype=np.int64)
            self.p_elems_mat = np.empty((self.mesh.numb_elems+1,), dtype=np.int64)
            self.p_elems_vec = np.empty((self.mesh.numb_elems+1,), dtype=np.int64)
            self.__need_indices = True
        else:
            self.__need_indices = False
        # preallocate space for data arrays
        for name in matrix_names:
            self.data[name] = np.empty((ndata_mat,), dtype=self._dtype)
            self._matrix_names.add(name)
        for name in vector_names:
            self.data[name] = np.empty((ndata_vec,), dtype=self._dtype)
            self._vector_names.add(name)

    def preassembleWithDirichletBC(self, lhs_name, rhs_name, element_routine, bc_map, overwrite=False):
        """Do pre-assembly with element level transformation for Dirichlet boundary conditions
        """
        matrix_names = [lhs_name]
        vector_names = [rhs_name]
        reducer = DirichletReducer(self, element_routine, bc_map)
        self.preassemble(matrix_names, vector_names, reducer, overwrite)

    def preassemble(self, matrix_names=(), vector_names=(), element_routine=None, overwrite=False):
        """Do pre-assembly - iterate over all elements in the mesh
        calculate element matrix and store it.

        Parameters
        ----------
            arrayName: string
                name over which store element matrix
                If None then name of the assembly routine is used
            element_routine: functor
                function to calculate element matrix. This function
                must take two arguments (mesh, elem) where mesh is the
                mesh to assembly over and elem is number of element for
                which matrix is assembled. The routine should return
                assembled matrix.
            overwrite : bool
                If True silently overwrite existing data, otherwise raise
                ValueError if data under given name already exists
        """

        if not matrix_names and element_routine is None:
            matrix_names.append('__symbolic__')

        self._check_overwrite(matrix_names, overwrite)
        self._check_overwrite(vector_names, overwrite)
        self._preallocate_storage(matrix_names, vector_names)

        # Iterate over elements, calculate element matrix and store it along with global
        # row and column indices (if indices not already filled)
        estart_mat = 0
        estart_vec = 0
        for elem in range(self.mesh.numb_elems):
            edofs = self._get_element_dofs(elem)
            edofs_len = len(edofs)
            self.p_elems_mat[elem] = estart_mat
            self.p_elems_vec[elem] = estart_vec
            eend_mat = estart_mat+edofs_len**2
            eend_vec = estart_vec+edofs_len
            if self.__need_indices:
                self.row_mat[estart_mat:eend_mat] = np.repeat(edofs, edofs_len)
                self.col_mat[estart_mat:eend_mat] = np.tile(edofs, edofs_len)
                self.row_vec[estart_vec:eend_vec] = edofs
            self._on_element_evaluator(elem, edofs,
                                       estart_mat, eend_mat, matrix_names,
                                       estart_vec, eend_vec, vector_names,
                                       element_routine)
            estart_mat = eend_mat
            estart_vec = eend_vec
        self.p_elems_mat[-1] = estart_mat
        self.p_elems_vec[-1] = estart_vec

    def _set_data_to_zero(self, estart_mat, eend_mat, matrix_names,
                         estart_vec, eend_vec, vector_names):
        for i in range(len(matrix_names)):
            self.data[matrix_names[i]][estart_mat:eend_mat] = 0
        vec_offset = len(matrix_names)
        for i in range(len(vector_names)):
            self.data[vector_names[i]][estart_vec:eend_vec] = 0

    def _set_data_from_arrays(self, estart_mat, eend_mat, matrix_names,
                              estart_vec, eend_vec, vector_names, arrays):
        for i in range(len(matrix_names)):
            self.data[matrix_names[i]][estart_mat:eend_mat] = arrays[i].reshape(-1)
        vec_offset = len(matrix_names)
        for i in range(len(vector_names)):
            self.data[vector_names[i]][estart_vec:eend_vec] = arrays[i + vec_offset].reshape(-1)

    def _on_element_evaluator(self, elem, edofs,
                              estart_mat, eend_mat, matrix_names,
                              estart_vec, eend_vec, vector_names,
                              element_routine):
        if element_routine is None:
            self.data[matrix_names[0]][estart_mat:eend_mat] = 1
        else:
            arrays = element_routine(self.mesh, elem, edofs, self._dtype)
            if not arrays:
                self._set_data_to_zero(estart_mat, eend_mat, matrix_names,
                                       estart_vec, eend_vec, vector_names)
            else:
                expected_arrays_len = len(matrix_names) + len(vector_names)
                if len(arrays) == expected_arrays_len:
                    self._set_data_from_arrays(estart_mat, eend_mat, matrix_names,
                                               estart_vec, eend_vec, vector_names, arrays)
                else:
                    raise ValueError(
                        "Element routine expected to return list of %d arrays, got %d" %
                        (expected_arrays_len, len(arrays)))

    def remap(self, l2g, total_ndofs):
        """Remaps row and col vectors using using l2g index lookup.

        The new the remapping is of the form:
            row[j] = l2g[row[j]]
            col[j] = l2g[col[j]]
        """
        if self._total_ndofs > total_ndofs:
            raise ValueError("*** Error: Remapping to smaller matrices is not supporte yet.")
        self._total_ndofs = total_ndofs
        self.row_mat = np.array(l2g[self.row_mat], dtype=self.row_mat.dtype)
        self.col_mat = np.array(l2g[self.col_mat], dtype=self.col_mat.dtype)
        self.row_vec = np.array(l2g[self.row_vec], dtype=self.row_vec.dtype)
        self.col_vec = np.zeros((len(self.row_vec),), dtype=self.col_vec.dtype)

    def getPreassembled(self, name, elem_id):
        """Return preassembled data for given element
        Parameters
        ----------
        name : :obj:`string`
            name of pre-assembled matrix. Raise ValueError if matrix has not been pre-assembled
        elem_id : :obj:`int`
            element index
        sparse_type : :obj:`string`
            sparsity type. Accepted values are: 'csr', 'ijv', 'mrg', 'dense'
        """
        if name in self._matrix_names:
            sb = self.p_elems_mat[elem_id]
            se = self.p_elems_mat[elem_id+1]
            data = self.data[name][sb:se]
            locsize = int(np.sqrt(se-sb))
            out_matrix = data.reshape(locsize, locsize)
        elif name in self._vector_names:
            sb = self.p_elems_vec[elem_id]
            se = self.p_elems_vec[elem_id+1]
            data = self.data[name][sb:se]
            out_matrix = data.reshape(se-sb, 1)
        else:
            raise ValueError("Matrix or vector '%s' not pre-assembled" % name)
        return out_matrix

    def assemble(self, name, sparse_type, factors=None):
        """Assemble pre-assembled matrix into sparse matrix of give type.
        Parameters
        ----------
        name : :obj:`string`
            name of pre-assembled matrix. Raise ValueError if matrix has not been pre-assembled
        sparse_type : :obj:`string`
            sparsity type. Accepted values are: 'csr', 'ijv', 'mrg', 'dense'
        factors : :obj:`ndarray`, optional
            1D array of factors to multiply each elementary matrix before assembly. It does
            not alter stored elementary matrices.
        """
        if name in self._matrix_names:
            if factors is None:
                data = self.data[name]
            else:
                data = ScaledRangeArray(self.data[name], borders=self.p_elems_mat, factors=factors)
            out_matrix = scipy.sparse.coo_matrix((data.copy(), (self.row_mat, self.col_mat)),
                                                 shape=(self._total_ndofs, self._total_ndofs))
        elif name in self._vector_names:
            if factors is None:
                data = self.data[name]
            else:
                data = ScaledRangeArray(self.data[name], borders=self.p_elems_vec, factors=factors)
            out_matrix = scipy.sparse.coo_matrix((data.copy(), (self.row_vec, self.col_vec)),
                                                 shape=(self._total_ndofs, 1))
        else:
            raise ValueError("Matrix or vector '%s' not pre-assembled" % name)
        return convertToFormat(out_matrix, sparse_type)

    def factorAssemble(self, other_matrix, matrix_name, factor=None):
        """Assemble matrix of given name and add it to otherMatrix.

        Note: befor adding to the targetMatrix the assembled matrix
        is multipled by given factor.

        Example:
        K = assembler.assemble('K')

        K+two_M = assembler.factorAssemble(K, 'M', 2.0)

        """
        fmt = other_matrix.getformat()
        aux_mat = self.assemble(matrix_name, fmt)
        if factor is not None:
            return other_matrix + factor*aux_mat
        else:
            return other_matrix + aux_mat

    def countStorageSize(self):
        """Count how many entries are needed for storing data array
        for matrices and vectors respectively
        """
        types_count = collections.Counter(self.mesh.elem_geotype)
        ndata_matrix = 0
        ndata_vector = 0
        for etype in meshgrid.ElemType:
            dofs_per_elem = etype.numb_nodes*self.qdim
            ndata_matrix += types_count[etype] * dofs_per_elem**2
            ndata_vector += types_count[etype] * dofs_per_elem
        return ndata_matrix, ndata_vector


class TermList(object):
    def __init__(self, *terms):
        self._mesh = None
        self._terms = []
        self._bilinear_terms = []
        self._linear_terms = []
        for term in terms:
            self.append(term)

    def append(self, term):
        if self.getTerm(term.name) is not None:
            raise ValueError(f"Term called {term.name} already in the list of terms")
        pos = len(self._terms)
        self._terms.append(term)
        if pos == 0:
            self._mesh = term.mesh
        else:
            if self._mesh is not term.mesh:
                raise SystemError(f"Term {term.name} is not defined",
                                  "on the same mesh as other terms")

        if term.isIt('bilinear'):
            self._bilinear_terms.append(pos)
        elif term.isIt('linear'):
            self._linear_terms.append(pos)
        else:
            raise SystemError('The term {term.name} is neither linear or bilinear')

    def __len__(self):
        return len(self._terms)

    def __getitem__(self, item):
        return self._terms[item]

    @property
    def mesh(self):
        return self._mesh

    @property
    def matrix_names(self):
        return [self._terms[i].name for i in self._bilinear_terms]

    @property
    def vector_names(self):
        return [self._terms[i].name for i in self._linear_terms]

    @property
    def numb_linear_terms(self):
        return len(self._linear_terms)

    @property
    def numb_bilinear_terms(self):
        return len(self._bilinear_terms)

    def __call__(self, mesh, elem, edofs, dtype):
        arrays = []
        for i in self._bilinear_terms:
            term_arrays = self._terms[i](mesh, elem, edofs, dtype)
            if term_arrays is not None:
                arrays.extend(term_arrays)
        for i in self._linear_terms:
            term_arrays = self._terms[i](mesh, elem, edofs, dtype)
            if term_arrays is not None:
                arrays.extend(term_arrays)
        return arrays

    def iterBilinearTerms(self):
        for i in self._bilinear_terms:
            yield self._terms[i]

    def iterLinearTerms(self):
        for i in self._linear_terms:
            yield self._terms[i]

    def getTerm(self, name):
        for term in self._terms:
            if term.name == name:
                return term
        return None


class TermAssembler(object):
    def __init__(self, *terms, dtype=np.float64):
        self.terms = TermList(*terms)
        self.mrg_assembler = MrgAssembler(self.terms.mesh, dtype=dtype)

    @property
    def qdim(self):
        """Retrun number of DOFs per node
        """
        return self.mrg_assembler.qdim

    @property
    def total_ndofs(self):
        """Return total number of dofs in the assembled matrix.
        The total number of dofs equals number of nodes times qdim
        """
        return self.mrg_assembler.total_ndofs

    def preassemble(self, overwrite=False):
        self.mrg_assembler.preassemble(self.terms.matrix_names, self.terms.vector_names, self.terms,
                                       overwrite=overwrite)

    def getPreassembled(self, name, elem_id):
        return self.mrg_assembler.getPreassembled(name, elem_id)

    def assemble(self, name, sparse_type, factors=None):
        return self.mrg_assembler.assemble(name, sparse_type, factors=factors)

    def remap(self, l2g, total_ndofs):
        return self.mrg_assembler.remap(l2g, total_ndofs)

    def preassembleWithDirichletBC(self, bc_map, overwrite=False):
        matrix_names = self.terms.matrix_names
        vector_names = self.terms.vector_names
        if len(matrix_names) != 1 or len(vector_names) != 1:
            raise SystemError(('Terms preassemble with Dirichlet BC only possible'
                               ' for one LHS and 1 RHS'))
        self.mrg_assembler.preassembleWithDirichletBC(matrix_names[0], vector_names[0], self.terms,
                                                      bc_map, overwrite)

    def evalBilinearForm(self, dofs_value, name=None):
        if name is None:
            if self.terms.numb_bilinear_terms < 1:
                raise SystemError("There is no bilinear term in assembler terms")
            name = self.terms.matrix_names[0]
        term = self.terms.getTerm(name)

        if term.isIt('bilinear'):
            adjacency = term.mesh.elem2nodes
            integral = 0.0
            element_forms = self.mrg_assembler.data[term.name]
            for elem in range(len(adjacency)):
                dofs = adjacency.getElemDofs(elem)
                ndofs = len(dofs)
                ms, me = self.mrg_assembler.p_elems_mat[elem:elem + 2]
                matrix = element_forms[ms:me].reshape((ndofs, ndofs))
                values = dofs_value[dofs]
                #  integral += np.dot(values, np.dot(matrix, values))
                integral += np.linalg.multi_dot((values, matrix, values))
            return integral
        else:
            raise RuntimeError(f"The term {term.name} is not bilinear")


def multiassemble(what_to_assemble, sparse_type, dtype=np.float64):
    """Perform assembly on a sequence of matrices.

    Parameters
    ----------
    what_to_assemble : tuple
        a tuple of: (assembler, l2g, factor, matrix_name) where

             * assembler  - MrgAssembler object
             * l2g - local to global index mapping (possibly None)
             * factor - factor to scale assembled matrix (possibly None)
             * matrix_name - matrix preassembled in assembler
    sparse_type : :obj:`string`
        sparsity type. Accepted values are: 'csr', 'ijv', 'mrg', 'dense'
    dtype : :obj:`numpy type`, optional
        type of the array to be assembled
    """
    new_row = []  # list of iterators to mapped rows of assemblers
    new_col = []  # list of iterators to mapper cols of assembler
    new_data = []  # list of iterators to scaled matrix data
    for assembler, l2g, factor, name in what_to_assemble:
        index_mapper = lambda x, myl2g=l2g: myl2g[x] if myl2g is not None else x
        data_mapper = lambda x, fct=factor: fct*x if fct is not None else x
        new_row.append(map(index_mapper, assembler.row))
        new_col.append(map(index_mapper, assembler.col))
        data = assembler.data[name]
        new_data.append(map(data_mapper, data))

    r = np.fromiter(itertools.chain.from_iterable(new_row), dtype=np.int64)
    c = np.fromiter(itertools.chain.from_iterable(new_col), dtype=np.int64)
    d = np.fromiter(itertools.chain.from_iterable(new_data), dtype=dtype)
    sp_matrix = scipy.sparse.coo_matrix((d, (r, c)))

    return convertToFormat(sp_matrix, sparse_type)


def convertToFormat(sparse_matrix, out_type):
    """Convert matrix to given sparse format:
    """
    if out_type == 'csr':
        out_matrix = sparse_matrix.tocsr()
    elif out_type == 'mrg':
        if _HAVE_ALINEA:
            out_matrix = alinea.matrices.csr_Matrix(sparse_matrix.tocsr())
        else:
            raise ImportError("*** ERROR: Package aline is not available in stand alone genfemcalc")
    elif out_type == 'dense':
        out_matrix = sparse_matrix.todense()
    else:
        raise ValueError(f"Not supported output matrix type: {out_type}")
    return out_matrix


def applyDirichletBC(sparse_type, lhs, rhs, bc_map, inplace=False):
    """"Return transformed  LHS matrix and RHS vector to take into account
    Dirichlet boundary conditions.
    """
    if sparse_type == 'dense':
        return applyDirichletBCDense(lhs, rhs, bc_map, inplace)
    else:
        return applyDirichletBCSparse(sparse_type, lhs, rhs, bc_map, inplace)


def applyDirichletBCDense(lhs, rhs, bc_map, inplace=False):
    """"Return transformed  LHS matrix and RHS vector to take into account
    Dirichlet boundary conditions. This is for DENSE matrices
    """
    n_dofs = lhs.shape[1]
    fixed_dofs = list(bc_map.keys())
    bc_val = list(bc_map.values())
    if inplace:
        lhs_new, rhs_new = lhs.copy(), rhs.copy()
    else:
        lhs_new, rhs_new = lhs, rhs
    bc_values = np.asarray(bc_val, dtype=rhs_new.dtype)
    rhs_new -= np.dot(lhs_new[:, fixed_dofs], bc_values).T
    condition_factor = np.trace(lhs_new)/n_dofs   # Factor to improve matrix conditioning
    lhs_new[:, fixed_dofs] = 0
    lhs_new[fixed_dofs, :] = 0
    lhs_new[fixed_dofs, fixed_dofs] = condition_factor
    rhs_new[fixed_dofs] = condition_factor * bc_values.reshape(-1, 1)
    return lhs_new, rhs_new


def applyDirichletBCSparse(sparse_type, lhs, rhs, bc_map, inplace=False):
    """"Return transformed  LHS matrix and RHS vector to take into account
    Dirichlet boundary conditions. This is for SPARSE matrices

    Ths function performs handling of boundary conditions using higher level arithmetic operations
    For system K*x = F   with  bc having values at fixed dofs and zero otherwise one constructs
    a new system.

        * Kn =  I_i * K * I_i + I_b
        * Fn =  I_i * (F - K * I_b * bc) + I_b * bc

     where I_i and I_b are diagonal like matrices:
         * I_i has ones at non-constrained dofs
         * I_b has ones at constrained dofs
    """
    if inplace:
        raise NotImplemented("In-place Dirichlet BC reduction not implemented for sparse matrices")
    else:
        n_dofs = lhs.shape[1]
        fixed_dofs = list(bc_map.keys())
        bc_val = list(bc_map.values())

        bc_row = fixed_dofs
        bc_col = [0] * len(bc_row)
        fixed_val = scipy.sparse.coo_matrix((bc_val, (bc_row, bc_col)), shape=(n_dofs, 1),
                                            dtype=rhs.dtype)
        fixed_val.tocsr()

        condition_factor = lhs.diagonal().sum()/n_dofs
        free_dofs_selector = np.ones(n_dofs)
        free_dofs_selector[fixed_dofs] = 0.0
        eye_free = scipy.sparse.spdiags(free_dofs_selector, [0], n_dofs, n_dofs).tocsr()
        del free_dofs_selector

        fixed_dofs_selector = np.zeros(n_dofs)
        fixed_dofs_selector[fixed_dofs] = 1.0
        eye_fixed = scipy.sparse.spdiags(fixed_dofs_selector, [0], n_dofs, n_dofs).tocsr()
        del fixed_dofs_selector

        rhs_new = eye_free*(rhs - lhs * eye_fixed * fixed_val) \
            + eye_fixed * fixed_val * condition_factor
        lhs_new = eye_free * lhs * eye_free + eye_fixed*condition_factor
        return convertToFormat(lhs_new, sparse_type), rhs_new
