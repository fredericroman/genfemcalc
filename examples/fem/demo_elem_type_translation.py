# -*- coding: utf-8 -*-
"""
Translate between element types
===============================

This demo illustrates how to translate element type ids for different
family of elements, that is GMSH, VTK and Meshio.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/4/2019'
__version__ = '0.1.0'

import os

from genfemcalc import examples
from genfemcalc import fem
from genfemcalc import meshgrid


class DemoElemTypeTranslation(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        tr = meshgrid.refcells.CellIdTranslator('vtk', 'gmsh')
        tr2 = meshgrid.refcells.CellIdTranslator('vtk', 'meshio')
        vtk_id = 3
        gmsh_id = tr(vtk_id)
        print(f"VTK cell type {vtk_id} is equivalent to GMSH type {gmsh_id}")
        # Translation of MRG element type - they are equal to VTK types
        print("Translation of MRG cell types")
        for mrg_id in meshgrid.ElemType:
            gmsh_id = tr(mrg_id)
            meshio_id = tr2(mrg_id)
            print((f"MRG cell type {mrg_id} is equivalent to GMSH type {gmsh_id},"
                  f" and Meshio type {meshio_id}"))


if __name__ == '__main__':
    demo = DemoElemTypeTranslation()
    demo.run()
