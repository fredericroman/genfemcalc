# -*- coding: utf-8 -*-
"""
Color cells by field
--------------------

Plot cells colored by specific field

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/16/2019'
__version__ = '0.1.0'

import os

from genfemcalc import examples
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoShowField(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        model = meshgrid.modelsgmsh.Rectangle()
        context = {'xa': 0.0, 'xb': 2.0, 'ya': 0.0, 'yb': 2.0,
                   'quadsonly': True, 'transfinite': False,
                   'edge_size': 0.05,
                   'tsdensity': [60, 60]}
        domain = model.discretize(context)
        mesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh, ['quad', 'triangle'])
        mesh.evalNodeField('temperature', 'x')
        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerMesh(mesh, 'main_mesh')
        viewer.show('main_mesh', antialiased='False')
        viewer.showNodeField('main_mesh', 'temperature', linewidths=-1, edgecolor='face')
        viewer.keep()


if __name__ == '__main__':
    demo = DemoShowField()
    demo.run()
