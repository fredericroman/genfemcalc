# -*- coding: utf-8 -*-
"""
Calculations on quad element
============================

Demonstrate the use of quadfem module for element calculations on QUAD element.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.08.22  11:6:55'
__version__ = '0.1.0'

import numpy as np

import genfemcalc
from genfemcalc import fem


def testCalcArea(x,y, ngpt=4):
    """Calculate quad area by integrating unit function over the quad
    This is to test jacobian calculation
    """
    ksi, eta, ww = genfemcalc.fembasic.quadfem.getQuadrature(ngpt)
    dN = genfemcalc.fembasic.quadfem.calcShapeDerivFun(ksi, eta)
    J = genfemcalc.fembasic.quadfem.calcJacobian2D2D(x, y, dN)
    area = 0.0
    for gp in range(len(ww)):
        detJ = np.linalg.det(J[gp, :].reshape(2, 2))
        area += ww[gp]*detJ
    return area


def getTestQuad(version=0):
    """Return x and y coordinates of a test quad
    """
    if version == 1:
        x = np.array([1, 2, 3, 1], dtype=np.float64)
        y = np.array([1, 1, 4, 2], dtype=np.float64)
    elif version == 2:
        x = np.array([1, 2, 2, 1], dtype=np.float64)
        y = np.array([1, 1, 2, 2], dtype=np.float64)
    else:
        x = np.array([-1, 1, 1, -1], dtype=np.float64)
        y = np.array([-1, -1, 1, 1], dtype=np.float64)
    return x, y


def demo_quadfem():
    print("Testing quadfem module")

    print("Test partition of unity for shape functions (10 points)")
    npt = 10
    ksi = np.random.rand(npt) * 2 - 1
    eta = np.random.rand(npt) * 2 - 1
    N = genfemcalc.fembasic.quadfem.calcShapeFun(ksi, eta)
    with np.printoptions(precision=5):
        print(N.sum(axis=1))

    for i in [0, 2, 1]:
        x, y = getTestQuad(i)
        print("Test area calculation for test quad no:", i)
        print("by calcArea : ", genfemcalc.fembasic.quadfem.calcArea(x, y))
        print("by testCalcArea : ", testCalcArea(x, y))
        print("------------------")

    D = np.identity(2, dtype=np.float64)

    x, y = getTestQuad(1)
    K = genfemcalc.fembasic.quadfem.calcEllipticTerm2D2D(x, y, 3, D)
    print('Elliptic stiffness matrix:\n', K)
    print('Symmetric: ', fem.matrixquery.isSymmetric(K))
    print('Invertible: ', fem.matrixquery.isInvertible(K, tol=1.e-5))
    print('Determinant: ', np.linalg.det(K))

    F = genfemcalc.fembasic.quadfem.calcSourceTerm2D2D(x, y, 2)
    print('Source term for constant f=1:\n', F)
    print('Sum F : ', np.sum(F))

    M = genfemcalc.fembasic.quadfem.calcMassTerm2D2D(x, y, 2)
    print('Mass matix:\n', M)
    print('Sum M:\n', np.sum(M))
    print('Symmetric :', fem.matrixquery.isSymmetric(M))
    print('Invertible :', fem.matrixquery.isInvertible(M, tol=1.e-5))


if __name__ == '__main__':
    demo_quadfem()
