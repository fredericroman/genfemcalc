# -*- coding: utf-8 -*-
"""Geometric calculations involving manifolds and manifolds mappings

The parametric finite elements introduce the concept of reference cell, and
mapping from reference element to physical cell. Both reference cell and
physical cell can be treated as N-dimensional manifolds embedded in 3D space.
Also transformation from reference cell to physical cell is most conveniently
seen as the transformation between manifolds, especially if one has to perform integration
over physical element or its parts (which in general case can be curved ones).

The main thing this module define is GeoMap which encapsulates mapping between
reference and real cell.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.13 11:18:5'
__version__ = '0.2.1'

import math
import itertools
import numpy as np


def volumeform(jmat):
    """Calculate volume form from coordinate transformation Jacbian matrix
    """
    if jmat.shape[0] == jmat.shape[1]:
        return math.fabs(np.linalg.det(jmat))
    else:
        return math.sqrt(np.linalg.det(np.dot(jmat, jmat.T)))


class GeomMapper(object):
    """Represents mapping between reference cell and physical cell.
    """
    def __init__(self, reference_cell, ambient_dim=None, cell_nodes=None, points=None):
        self.__needs_update = True
        self.__needs_precompute = True

        self._precomp_N: np.ndarray = np.array([])
        self._precomp_dN: np.ndarray = np.array([])
        self._stored_jacobian: np.ndarray = np.array([])
        self._stored_volume_form: np.ndarray = np.array([])

        if ambient_dim is None:
            self._ambient_dim = reference_cell.dim
        else:
            self._ambient_dim = ambient_dim

        self._refcell = reference_cell
        if self._refcell.dim > self._ambient_dim:
            raise ValueError('Dimension of ambient space cannot be smaller than dimension of reference cell')
        self._nodes: np.ndarray = np.array([])
        self._ref_points: np.ndarray = np.array([])

        self._quadrature = None

        if cell_nodes is not None:
            self.setNodes(cell_nodes)
        if points is not None:
            self.setRefPoints(points)

    @property
    def is_proper(self):
        return self._ambient_dim == self._refcell.dim

    @property
    def quadrature(self):
        return self._quadrature

    @property
    def nodes(self):
        return self._nodes

    @property
    def ref_points(self):
        return self._ref_points

    @property
    def numb_ref_points(self):
        return self._ref_points.shape[0]

    @property
    def dim(self):
        return self._refcell.dim

    @property
    def ambient_dim(self):
        return self._ambient_dim

    @property
    def refcell(self):
        return self._refcell

    def precompute(self):
        """Precompute shape functions and their derivatives at stored reference points.

        It does nothing if precomputed already
        """
        if self.__needs_precompute:
            if self._ref_points.size == 0:
                raise RuntimeError("You have forgot to set mapper points")
            self._precomp_N = self.refcell.N(self.ref_points)
            self._precomp_dN = self.refcell.dN(self.ref_points)
            self.__needs_precompute = False
            self.__needs_update = True

    def precomputeForQuadrature(self, quadrature):
        """Precompute shape functions and their derivatives at quadrature and set quadrature points
        as stored reference points.

        """
        self._quadrature = quadrature
        self.setRefPoints(quadrature.points, precompute_now=True)

    def update(self):
        """Update quantities kept at reference points

        This method updates the following quantities:
            * Jacobian matrix
            * volume form
        """
        if self.__needs_update:
            if self._nodes.size == 0:
                raise RuntimeError("You have forgot to set mapper nodes")
            # noinspection PyPep8Naming
            dN = self._precomp_dN
            self._stored_jacobian = np.einsum('gdn,nc->gdc', dN, self.nodes[:, 0:self._ambient_dim])
            self._stored_volume_form[:] = np.array([volumeform(jac) for jac in self._stored_jacobian])
            #for i in range(self._stored_jacobian.shape[0]):
            #    self._stored_volume_form[i] = volumeform(self._stored_jacobian[i, :, :])
            self.__needs_update = False

    @property
    def precomputed_N(self):
        """Return matrix of shape function values computed at stored reference points

        Returns
        -------
            N : ndarray
                N.shape[0] is equal to to number of reference points the shape functions were evaluated
                N.shape[1] is equal to to number of shape functions in reference element
        """
        self.precompute()
        return self._precomp_N

    @property
    def precomputed_dN(self):
        """Return matrix of shape function values computed at stored reference points

        Returns
        -------
           dN : ndarray
                dN.shape[0] is equal to the number of reference points the shape functions were evaluated
                dN.shape[1] is equal to the number of reference coordinates
                dN.shape[2] is equal to the number of shape functions in reference element
        """
        self.precompute()
        return self._precomp_dN

    @property
    def jacobian(self):
        """Return Jacobian matrix at stored reference points for currently stored nodes.
        """
        self.update()
        return self._stored_jacobian

    @property
    def volumeForm(self):
        """Return volume form at stored reference points for currently stored nodes.
        """
        self.update()
        return self._stored_volume_form

    def mapPoints(self, points):
        """Map reference points to physical points for given nodes.
        """
        # noinspection PyPep8Naming
        N = self.refcell.N(points)
        if self._nodes.size == 0:
            raise RuntimeError("You have forgotten to set nodes")
        mapped_points = np.dot(N, self._nodes[0:, 0:self._ambient_dim])
        return mapped_points

    def setRefPoints(self, points, copy=False, precompute_now=True):
        if points.shape[1] < self._refcell.dim:
            raise ValueError("Insufficient number of coords for reference points: %d. Needs %d",
                             (points.shape[1], self._refcell.dim))
        if copy:
            self._ref_points = points.copy()
        else:
            self._ref_points = points
        self.__needs_precompute = True
        self.__needs_update = True
        self._stored_volume_form = np.empty((points.shape[0],), dtype=np.float64)
        if precompute_now:
            self.precompute()

    def setNodes(self, cell_nodes, copy=False):
        if cell_nodes.shape[0] != self._refcell.numb_nodes:
            raise ValueError("Number passed nodes %d does not agree with expected %d",
                             (cell_nodes.shape[0], self._refcell.numb_nodes))
        if cell_nodes.shape[1] > 3:
            raise ValueError("Cell nodes dimension must be <=3, got %d" % cell_nodes.shape[1])
        if copy:
            self._nodes = cell_nodes.copy()
        else:
            self._nodes = cell_nodes
        self.__needs_update = True


def calculateJacobian(refcell, nodes, points):
    """Calculate Jacobian matrix of geometric transformation.

    Calculate Jacobian matrix of geometric transformation from reference cell
    to physical cell. The calculation takes place at specific points in reference
    cell, most of the time the Gauss points.

    Parameters
    ----------
         refcell :  RefCell
            Reference cell object.
         nodes : ndarray
            Numpy array of node coordinates. The array is to be of shape (N, 3)
            where N is the number of nodes in  refcell.
         points : ndarray
            Numpy array of points in reference element. The is to be of shape (K, dim)
            where  K is number of evaluation points and dim >= refcell.dim

    Returns
    -------
        ndarray
            Tensor (3D array). The shape of the tensor is (numGpt, dimD, dimC)
            where:

                * numGpt = number of evaluation points
                * dimD = number of reference coordinates (reference space dimension)
                * dimC = number  of node coordinates (ambient space dimension)
    """
    # noinspection PyPep8Naming
    dN = refcell.dN(points)
    jacobian = np.einsum('gdn,nc->gdc', dN, nodes)
    return jacobian

