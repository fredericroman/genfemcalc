# -*- coding: utf-8 -*-
"""
Rasterized mesh as a mask
-------------------------

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '10/27/2019'
__version__ = '0.1.0'

import descartes
import os
import matplotlib
import numpy as np
import rasterio.features

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoImageAsMask(examples.Demo):
    def __init__(self):
        super().__init__()
        self.parser.add_argument('--mesh-file', dest='mesh_file',
                                 default='simple_quad_tri_mesh.vtk',
                                 help='VTK data file to read')
        self.parser.add_argument('--nx-cells', dest='nx_cells', default=5,
                                 type=int, help='Number of cells in X-direction')
        self.parser.add_argument('--ny-cells', dest='ny_cells', default=5,
                                 type=int, help='Number of cells in Y-direction')

    @property
    def cells_resolution(self):
        return self.args.nx_cells, self.args.ny_cells

    def demonstrate(self):
        file_name = os.path.join(DATADIR, self.args.mesh_file)
        mesh = meshgrid.iomrg.readMrgFromVtk(file_name)
        multipoly = meshgrid.utilsshapely.makeMultiPolygon(mesh)

        bounds = multipoly.bounds
        xres, yres = self.cells_resolution
        transform = rasterio.transform.from_bounds(*bounds, xres, yres)
        # CAUTION for rasterisation resolution should be (yres, xres)
        mask = rasterio.features.rasterize(multipoly.geoms, (yres, xres), all_touched=True, transform=transform)
        mask = np.flipud(mask)
        minmax = [bounds[i] for i in (0, 2, 1, 3)]
        background_mesh = meshgrid.modelsmrg.buildRectangle(*minmax, xres, yres)[0]
        background_mesh.addElemField('mask', mask.reshape((-1)))
        mask_filter = meshgrid.adjacency.MeshMaskFilter(background_mesh, mask.reshape((-1,)))
        masked_mesh = mask_filter.buildMesh()
        viewer = meshgrid.graphics.MeshViewer(nrows=2, ncols=1)
        viewer.registerMesh(mesh, 'base')
        viewer.registerMesh(masked_mesh, 'masked')
        viewer.registerMesh(background_mesh, 'background', viewport=(1, 0))
        viewer.show('base', facecolor='yellow', edgecolor='black')
        viewer.show('masked', facecolor='none', edgecolor='red')
        cmap = matplotlib.colors.ListedColormap(['white', 'yellow'])
        viewer.showCellField('background', 'mask', cmap=cmap, edgecolor='red')

        self.showImage()


if __name__ == '__main__':
    demo = DemoImageAsMask()
    demo.run()
