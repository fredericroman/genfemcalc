# -*- coding: utf-8 -*-
"""
Structured grid in rectangle
============================

Illustrate generation of structured grid in cube domain.

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/8/2019'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(os.path.abspath(genfemcalc.__file__))
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoStructuredCubeGrid(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        lbf = [0.0, 0.0, 0.0]
        rtb = [5.0, 6.0, 10.0]
        mesh = meshgrid.modelsmrg.buildCubeMesh(lbf, rtb, cell_resolution=(2, 3, 4))
        filename = os.path.join(OUTDIR, 'cube_mesh.vtk')
        meshgrid.iomrg.writeVtkFromMrg(mesh, filename)


if __name__ == '__main__':
    demo = DemoStructuredCubeGrid()
    demo.run()
