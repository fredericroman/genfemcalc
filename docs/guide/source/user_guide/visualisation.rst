.. include:: ../../replaces.txt
.. _visualisation-sec:

************************************
Visualization of Data and Algorithms
************************************

Retained mode versus pipeline processing in visualization
---------------------------------------------------------

Visualisation of meshes and their components
--------------------------------------------

Visualisation of scalar fields on meshes
----------------------------------------

Visualisation of vector fields on meshes
----------------------------------------

Data visualisation with external tools
--------------------------------------

ParaView
^^^^^^^^

Gnuplot
^^^^^^^

GMSH
^^^^
