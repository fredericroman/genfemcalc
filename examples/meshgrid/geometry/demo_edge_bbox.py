# -*- coding: utf-8 -*-
"""TODO : Add description of demo demo_edge_bbox.py
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/11/2019'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoEdgeBoundingBox(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        master_box = meshgrid.geometrymrg.BoundingBox([0, 0, 0], [10, 10, 10])
        for edge in meshgrid.geometrymrg.BoundingBox.edges:
            edge_box = master_box.getEdgeBox(edge, eps=1.0)
            print(f"{edge} -> {edge_box.lbf}  :   {edge_box.rtb}")

if __name__ == '__main__':
    demo = DemoEdgeBoundingBox()
    demo.run()
