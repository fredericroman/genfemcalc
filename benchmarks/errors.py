# -*- coding: utf-8 -*-
"""
This module provides functionality to make easier measuring errors
of approximate solutions.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/23/2019'
__version__ = '0.1.0'

import math

from genfemcalc import fem

import numpy as np

def computeL2ErrorVersusExact(mesh_integrator, fieldname, exact_field):
    """Compute L2 norm of difference between approximate and exact field

    Parameters
    ----------
        mesh_integrator : MeshIntegrator
            Mesh integrator over which to measure error
        fieldname :
            Name of the field stored at mesh
        exact_field :
            Function to calculate exact filed value
    """
    node_data = mesh_integrator.mesh.node_fields.get(fieldname)
    if node_data is None:
        raise RuntimeError(f"Node field {fieldname} not found in mesh")
    evaluator = fem.meshcalc.MeshFieldEvaluator(mesh_integrator, node_data)

    def integrand(elem, mapper, dtype):
        current_points = mapper.mapPoints(mapper.ref_points)
        dim = exact_field.dim
        exact_val = exact_field.eval(current_points[:, 0:dim])
        approx_val = evaluator.evalUatStoredPoints(elem, dtype)
        err2 = (exact_val-approx_val)**2
        return err2
    error2 = mesh_integrator.integrate(integrand)
    return math.sqrt(error2)
