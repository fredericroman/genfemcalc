# -*- coding: utf-8 -*-
"""
Generic element calculations
============================

"""
__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.04  19:12:55'
__version__ = '0.1.0'

import numpy as np

from genfemcalc import fembasic
from genfemcalc import fem
from genfemcalc import meshgrid

quadnodes = np.array([[1, 1, 0], [3, 1, 0], [4, 3, 0], [0, 2, 0]], dtype='float64')
cell = meshgrid.refcells.getRefCell('vtk', meshgrid.ElemType.convert('quad'))
quadrature = fem.quadratures.Quadrature(cell, 2)

mapper = fem.manifolds.GeomMapper(cell,ambient_dim=3)
mapper.precomputeForQuadrature(quadrature)
mapper.setNodes(quadnodes)

MA = fem.elemcalc.calcMassTerm(mapper)

MB = fembasic.quadfem.calcMassTerm(quadnodes, 2)

if np.allclose(MA, MB):
    print("Matrices are the same")
else:
    print('Matrices are different')
    print(MA)
    print('--------------------')
    print(MB)
print(cell.points)
print(mapper.mapPoints(cell.points))

f = [1,2,3,4]
print (fem.elemcalc.calcSourceTerm(mapper, f))
print (fembasic.quadfem.calcSourceTerm(quadnodes, 2, f))

D = np.array([[2, 4, 1], [3, 5, 2], [0, 0, 5]])
#D = np.array([[1, 0], [0, 1]])

KA = fem.elemcalc.calcEllipticTerm(mapper, D)

KB = fembasic.quadfem.calcEllipticTerm(quadnodes, 2, D)

if np.allclose(KA, KB):
    print("Stiffness are the same")
else:
    print('Stiffness are different')
    print(KA)
    print('--------------------')
    print(KB)



