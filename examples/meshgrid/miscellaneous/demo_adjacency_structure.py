# -*- coding: utf-8 -*-
"""
Cells neighbouring graph
========================

Illustrate creation and visualization of mesh adjacency structure
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/8/2019'
__version__ = '0.1.0'

import os
import graphviz
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoAdjacencyStructure(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        path = os.path.join(DATADIR, 'simple_quad_tri_mesh.vtk')
        mesh = meshgrid.iomrg.readMrgFromVtk(path)
        structure = meshgrid.adjacency.DynamicAdjacency.fromMesh(mesh)
        for cell in structure:
            print(f"Cell {cell.type} ->", [c.type for c in cell.neighbours])

        dot = graphviz.Digraph(comment='Cells neighbourhood')
        for cell in structure:
            for c in cell.neighbours:
                dot.edge(f"{cell.type}", f"{c.type}")
        filepath = os.path.join(OUTDIR, 'adjacency_structure')
        dot.format = 'png'
        dot.render(filepath, view=self.show_images)
        img = mpimg.imread(filepath + '.' + dot.format)
        if self.show_images:
            plt.imshow(img)
            plt.show()


if __name__ == '__main__':
    demo = DemoAdjacencyStructure()
    demo.run()
