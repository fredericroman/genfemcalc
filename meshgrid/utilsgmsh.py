# -*- coding: utf-8 -*-
"""This module provides utilities to deal with PyGmsh meshes.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019-09-01'
__version__ = '0.9.0'

# Python packages
import meshio
import numpy as np
from packaging.version import parse as _parse_version

if _parse_version(meshio.__version__) >= _parse_version('3.0.0'):
    from meshio._msh.common import _meshio_to_gmsh_type as meshio_to_gmsh_type
    from meshio._msh.common import _gmsh_to_meshio_type as gmsh_to_meshio_type
else:
    from meshio.msh_io.common import _meshio_to_gmsh_type as meshio_to_gmsh_type
    from meshio.msh_io.common import _gmsh_to_meshio_type as gmsh_to_meshio_type

# MRG packages

def getValidCellTypes():
    return 'line', 'triangle', 'quad', 'tetra', 'hexahedron'


def makeRegionSelectionTags(mesh, selector=None, **kwargs):
    """Selection can be done bey:
        regions - list of meshgrid.modelsgmsh.Region objects
        tags - numeric tags of regions
        names - names of regions
    """
    myselector = {}
    if selector is not None:
        myselector = selector.copy()
    myselector.update(kwargs)

    selection_tags = []
    if 'regions' in myselector:
        selection_tags.extend([region.tag for region in myselector['regions']])
    if 'tags' in myselector:
        selection_tags.extend(myselector['tags'])
    if 'names' in myselector:
        selection_tags.extend([mesh.field_data[name][0] for name in myselector['names']])
    return selection_tags


def countCells(mesh, cell_types=(), region_selector=None, **kwargs):
    """Return number of cells in specified mesh regions
    """
    selection_tags = makeRegionSelectionTags(mesh, region_selector, **kwargs)
    counter = 0
    if not cell_types:
        cell_types = getValidCellTypes()
    for tpe in mesh.cell_data.keys():
        if tpe in cell_types:
            if selection_tags:
                mesh_tags = mesh.cell_data[tpe]['gmsh:physical']
                sub_cells_idx = np.where(np.in1d(mesh_tags, selection_tags))[0]
                counter += sub_cells_idx.size
            else:
                counter += mesh.cells[tpe].shape[0]
    return counter


def countAdjacencyStorage(mesh, cell_types=(), selector=None, **kwargs):
    """Return tuple of values which are necessary to store elem2nodes and p_elem2nodes
    arrays.
    """
    selection_tags = makeRegionSelectionTags(mesh, selector, **kwargs)
    if not cell_types:
        cell_types = getValidCellTypes()
    e2n_counter = 0
    p_e2n_counter = 1
    for tpe in mesh.cell_data.keys():
        if tpe in cell_types:
            if selection_tags:
                mesh_tags = mesh.cell_data[tpe]['gmsh:physical']
                sub_cells_idx = np.where(np.in1d(mesh_tags, selection_tags))[0]
                p_e2n_counter += sub_cells_idx.size
                e2n_counter += sub_cells_idx.size * mesh.cells[tpe].shape[1]
            else:
                p_e2n_counter += mesh.cells[tpe].shape[0]
                e2n_counter += mesh.cells[tpe].size
    return e2n_counter, p_e2n_counter


def getRegionsNodesIds(mesh, selector=None, **kwargs):
    selection_tags = makeRegionSelectionTags(mesh, selector, **kwargs)
    regions_nodes = set()
    for tpe in mesh.cell_data.keys():
        mesh_tags = mesh.cell_data[tpe]['gmsh:physical']
        sub_cells_idx = np.where(np.in1d(mesh_tags, selection_tags))[0]
        if sub_cells_idx.size > 0:
            regions_nodes.update(mesh.cells[tpe][sub_cells_idx].flatten())
    return sorted(regions_nodes)


def getSubmesh(mesh, selector=None, **kwargs):
    selection_tags = makeRegionSelectionTags(mesh, selector, **kwargs)
    sub_cells = dict()
    cell_data = dict()
    sub_pts_idx = set()
    for tpe in mesh.cell_data.keys():
        mesh_tags = mesh.cell_data[tpe]['gmsh:physical']
        sub_cells_idx = np.where(np.in1d(mesh_tags, selection_tags))[0]
        if sub_cells_idx.size > 0:
            sub_pts_idx.update(mesh.cells[tpe][sub_cells_idx].flatten())
            local_sub_cells = mesh.cells[tpe][sub_cells_idx, :]
            local_sub_cell_data = dict()
            for tag in ['gmsh:physical', 'gmsh:geometrical']:
                if tag in mesh.cell_data[tpe]:
                    local_sub_cell_data[tag] = mesh.cell_data[tpe][tag][sub_cells_idx]
            local_sub_cell_data['parent'] = sub_cells_idx
            sub_cells.update({tpe: local_sub_cells})
            cell_data.update({tpe: local_sub_cell_data})
    l2g_idx = sorted(sub_pts_idx)
    g2l_idx = dict(zip(l2g_idx, np.arange(0, len(l2g_idx), dtype='int64')))
    # Here we construct a mppers (functions) that will map global indices
    # of point to local indices
    CellMapper = np.vectorize(lambda x: g2l_idx[x])

    # remap adjacency
    for tpe in sub_cells.keys():
        sub_cells[tpe] = CellMapper(sub_cells[tpe])

    sub_points = mesh.points[l2g_idx, :]
    point_data = {'l2g': np.array(l2g_idx, dtype='int64')}
    # Create mesh of boundary region
    if _parse_version(meshio.__version__) >= _parse_version('3.0.0'):
        sub_mesh = meshio.Mesh(sub_points, sub_cells, point_data=point_data,
                               cell_data=cell_data, field_data=mesh.field_data)
    else:
        sub_mesh = meshio.mesh.Mesh(sub_points, sub_cells, point_data=point_data,
                                    cell_data=cell_data, field_data=mesh.field_data)
    return sub_mesh
