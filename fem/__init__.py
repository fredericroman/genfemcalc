if __debug__:
    print(f'Invoking __init__.py for {__name__}')

from . import assembly
from . import elemcalc
from . import manifolds
from . import matrixquery
from . import meshcalc
from . import quadratures
