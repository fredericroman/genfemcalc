# -*- coding: utf-8 -*-
"""
Converting Mesh to TriMesh
--------------------------
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/16/2019'
__version__ = '0.1.0'

import os
import numpy
from genfemcalc import examples
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics
import matplotlib.colors

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoShowTriMesh(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        model = meshgrid.modelsgmsh.Rectangle()
        context = {'xa': 0.0, 'xb': 2.0, 'ya': 0.0, 'yb': 2.0,
                   'quadsonly': True, 'transfinite': False,
                   'edge_size': 0.7,
                   'tsdensity': [20, 20]}
        domain = model.discretize(context)
        mesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh, ['quad', 'triangle'])
        tri_mesh = meshgrid.graphics.tri.TriMesh.fromMrgMesh(mesh, copy_points=True)

        meshgrid.geometrymrg.anchorMeshAt(tri_mesh, (3.0, 3.0, 0.0))

        mesh.evalNodeField('temp', 'x')
        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerMesh(mesh, 'main_mesh')
        viewer.registerMesh(tri_mesh, 'tri')

        normalizer = matplotlib.colors.Normalize(0, tri_mesh.numb_elems)
        cmap = matplotlib.cm.get_cmap(name='ocean', lut=tri_mesh.numb_elems)
        viewer.showCellField('tri', 'parent_elem', edgecolor='black', cmap=cmap, norm=normalizer)

        viewer.resetColorBar()
        viewer.cbar.autoscale=False
        normalizer = matplotlib.colors.Normalize(0.0, 4.0)
        viewer.showContourf('main_mesh', 'temp', 10, norm=normalizer)

        meshgrid.geometrymrg.anchorMeshAt(mesh, (0, 3, 0))
        viewer.show('main_mesh', facecolor='yellow', edgecolor='black')

        viewer.keep()


if __name__ == '__main__':
    demo = DemoShowTriMesh()
    demo.run()
