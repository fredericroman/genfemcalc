import unittest
from meshgrid import ElemType  
from meshgrid import iomrg

class TestMeshIO(unittest.TestCase):

    def test_readFromVTK(self):
        fname = 'test/data/simple_quad_tri_mesh.vtk'
        m = iomrg.readMrgFromVtk(fname)
        self.assertEqual(m.numb_nodes, 7, 'Number of nodes should be 7')

class TestElemType(unittest.TestCase):
    def test_convert_from_str(self):
        et = ElemType.convert('line')
        self.assertEqual(et, ElemType.LINE, 
                'Element type should be LINE')
        et = ElemType.convert('triangle')
        self.assertEqual(et, ElemType.TRIANGLE, 
                'Element type should be TRIANGLE')
    def test_convert_from_int(self):
        et = ElemType.convert(12)
        self.assertEqual(et, ElemType.HEXAHEDRON, 
                'Element type should be HEXAHEDRON')
        et = ElemType.convert(10)
        self.assertEqual(et, ElemType.TETRA, 
                'Element type should be TETRA')
    def test_convert_from_ElemType(self):
        et = ElemType.convert(ElemType.QUAD)
        self.assertEqual(et, ElemType.QUAD, 
                'Element type should be QUAD')
        et = ElemType.convert(ElemType.LINE)
        self.assertEqual(et, ElemType.LINE, 
                'Element type should be LINE')
    def test_convert_from_sequence(self):
        tria = ElemType.TRIANGLE
        et = ElemType.convert(('line', 'quad', tria))
        self.assertEqual(et[0], ElemType.LINE, 
                'Element type should be LINE')
        self.assertEqual(et[1], ElemType.QUAD, 
                'Element type should be QUAD')
        self.assertEqual(et[2], ElemType.TRIANGLE, 
                'Element type should be TRIANGLE')

if __name__ == '__main__':
    unittest.main()
