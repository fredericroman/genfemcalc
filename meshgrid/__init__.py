
print(f'Invoking __init__.py for {__name__}')

from ._meshmrg import *
from . import adjacency
from . import cellsgmsh
from . import geometrymrg
from . import iomrg
from . import modelsgmsh
from . import modelsmrg
from . import refcells
from . import utilsgmsh
from . import utilsshapely
from . import wrappers

