# -*- coding: utf-8 -*-
"""
Building solution from string expression
----------------------------------------

Illustrate the usage of ``benchamrks.exact_solutions.ParsedSolution``
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/24/2019'
__version__ = '0.1.0'

import numpy
import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import benchmarks

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoParsedSolution(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        solution = benchmarks.exact_solutions.ParsedSolution('x')
        print('Function dimension: ', solution.dim)
        print('Function expression: ', solution)
        x = numpy.linspace(0.0, 4.0, 5)
        f = solution.eval(x)
        print(type(f))
        print('Points: ', x)
        print('Values: ', f)


if __name__ == '__main__':
    demo = DemoParsedSolution()
    demo.run()
