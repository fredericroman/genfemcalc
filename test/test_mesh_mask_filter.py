"""Unit tests for MeshMaskFilter class
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/10/2019'
__version__ = '0.1.0'

import numpy
import os
import unittest

import genfemcalc
from genfemcalc import meshgrid

PKGDIR = os.path.dirname(os.path.abspath(genfemcalc.__file__))
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class TestMeshMaskFilter(unittest.TestCase):
    def setUp(self):
        filename = os.path.join(DATADIR, 'two_holes_grid.png')
        self.mesh = genfemcalc.meshgrid.iomrg.readMrgFromImage(filename)
        skinner = meshgrid.adjacency.Skinner.fromMesh(self.mesh)
        boundary_mesh = skinner.buildMesh()
        self.submesher = meshgrid.adjacency.MeshMaskFilter(boundary_mesh, skinner.l2g_nodes())
        bbox = meshgrid.geometrymrg.BoundingBox.fromMrgMesh(self.submesher.mesh)
        south_bbox = bbox.getEdgeBox('bottom-front', eps=0.1)
        mask = meshgrid.adjacency.getMeshMaskFromBox(self.submesher.mesh, south_bbox)
        self.submesher.setMask(mask)
        self.south_mesh = self.submesher.buildMesh()

    def test_submesh_size(self):
        self.assertEqual(self.south_mesh.numb_nodes, 11)

    def test_submesh_l2g(self):
        l2g = self.submesher.l2g_nodes()
        for i in range(self.south_mesh.numb_nodes):
            gi = l2g[i]
            local_point = self.south_mesh.node_coords[i, :]
            global_point = self.mesh.node_coords[gi, :]
            self.assertTrue(numpy.allclose(local_point, global_point))

    def test_submesh_geotype(self):
        status = numpy.all(self.south_mesh.elem_geotype == int(genfemcalc.meshgrid.ElemType.LINE))
        self.assertTrue(status)
