# -*- coding: utf-8 -*-
"""Module for geometric transformation of meshes.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/10/2019'
__version__ = '0.1.0'

import copy
import math
import numpy

from genfemcalc import meshgrid


class BoundingBox(object):
    points = numpy.array([[0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0],
                         [0, 0, 1], [1, 0, 1], [1, 1, 1], [0, 1, 1]], dtype='bool')
    edges = {'bottom-front': (0, 1), 'bottom-back': (3, 2),
             'top-front': (4, 5),    'top-back': (7, 6),
             'bottom-left': (0, 3),  'bottom-right': (1, 2),
             'top-left': (4, 7),     'top-right': (5, 6),
             'left-front': (0, 4),   'left-back': (3, 7),
             'right-front': (1, 5),  'right-back': (2, 6)
             }

    def __init__(self, lbf_corner, rtb_corner):
        self._corners = numpy.array([lbf_corner, rtb_corner], dtype=numpy.float64)

    @classmethod
    def fromCenterAndSize(cls, center=(0.0, 0.0, 0.0), size=1.0):
        center = numpy.asarray(center)
        lbf_corner = numpy.asarray(center) - 0.5*size
        rtb_corner = numpy.asarray(center) + 0.5*size
        return cls(lbf_corner, rtb_corner)

    @classmethod
    def fromMrgMesh(cls, mesh):
        mesh = meshgrid.wrappers.MrgMesh(mesh)
        lbf_corner = numpy.min(mesh.points, axis=0)
        rtb_corner = numpy.max(mesh.points, axis=0)
        return cls(lbf_corner, rtb_corner)

    def __getitem__(self, item):
        return self._corners[item, :]

    @property
    def lbf(self):
        return self._corners[0, :]

    @property
    def rtb(self):
        return self._corners[1, :]

    @property
    def lbrt(self):
        return numpy.c[self._corners[0, 0:2], self._corners[1, 0:2]]

    @property
    def extents(self):
        return self._corners[1, :] - self._corners[0, :]

    @property
    def center(self):
        return numpy.mean(self._corners, axis=0)

    def scale(self, factor, inplace=True):
        """Get scaled box. Scale the box with box center being invariant point"""
        if isinstance(factor, float):
            factor = (factor,)*3
        factor = numpy.asarray(factor)-1.0
        ext = self.extents*factor*0.5
        if inplace:
            self._corners[0, :] -= ext
            self._corners[1, :] += ext
            return self
        else:
            return BoundingBox(self._corners[0, :]-ext, self._corners[1, :]+ext)

    def offset(self, distance, inplace=True):
        """Move box corners symmetrically by given distance"""
        if isinstance(distance, float):
            distance = (distance,)*3
        offsets = numpy.asarray(distance)
        if numpy.any(self.extents+2*offsets < 0):
            idx = numpy.where(self.extents+2*offsets < 0.0)
            raise ValueError(f"Distance to big at %s" % (idx,))
        if inplace:
            self._corners[0, :] -= offsets
            self._corners[1, :] += offsets
            return self
        else:
            return BoundingBox(self._corners[0, :]-offsets,
                               self._corners[1, :]+offsets)

    def flatten(self, interleaved=True, dim=3):
        """If interleaved is False, the coordinates are in the
        form[xmin, xmax, ymin, ymax, ..., ..., kmin, kmax].If
        interleaved is True, the coordinates are int the
        form[xmin, ymin, ..., kmin, xmax, ymax, ..., kmax]"""
        if interleaved and dim == 3:
            return self._corners.flatten(order='C')
        elif interleaved and dim == 2:
            return self._corners[:, 0:2].flatten(order='C')
        elif not interleaved and dim == 3:
            return self._corners.flatten(order='F')
        elif not interleaved and dim == 2:
            return self._corners[:, 0:2].flatten(order='F')
        else:
            raise ValueError(f"Dimension {dim} not in (2,3)")

    def anchorAt(self, point, anchor='lbf'):
        anchor = anchor.lower()
        if anchor == 'lbf':
            tr = numpy.asarray(point) - self._corners[0]
        elif anchor == 'rtb':
            tr = numpy.asarray(point) - self._corners[1]
        elif anchor == 'center':
            tr = numpy.asarray(point) - self.center
        else:
            raise ValueError(f"Invalid anchor {anchor}. Expected one of 'lbf', 'rtb' or 'center'")
        self._corners[0, :] += tr
        self._corners[1, :] += tr

    def getEdgeBox(self, location, eps=None):
        if eps is None:
            eps = max(1E-5, numpy.min(self.extents)/3)
        lbf = self.lbf
        rtb = self.rtb
        s, e = self.edges[location]
        lbf_mask = numpy.logical_not(self.points[s, :])
        lbf_mask_inv = self.points[s, :]
        rtb_mask = numpy.logical_not(self.points[e, :])
        rtb_mask_inv = self.points[e, :]
        eps_box_lbf = numpy.array([-eps, -eps, -eps])
        eps_box_rtb = numpy.array([eps, eps, eps])
        edge_lbf = eps_box_lbf + lbf * lbf_mask + rtb * lbf_mask_inv
        edge_rtb = eps_box_rtb + lbf * rtb_mask + rtb * rtb_mask_inv
        return self.__class__(edge_lbf, edge_rtb)


def scale(mesh, scaling_vector, center=(0.0, 0.0, 0.0), inplace=True):
    """Transform mesh by scaling.

    Transform mesh by scaling it in all three directions.

    Parameters
    ----------
        mesh : genfemcalc.meshgrid.Mesh
            Mesh to scale
        scaling_vector : array like
            Vector of scaling factors in directions x,y,z
            If only vector of shape (1,) is given then scling is uniform
            in all directions
        center : array-line (optional, default ``(0.0,0.0,0.0`)``
            Center of scaling operation (this point is unaffected by scaling
            transformation).
        inplace : bool (optional)
            If True (default) operation is done on the mesh argument.
            If False created deep copy of the mesh first.
    Returns
    -------
        genfemcalc.meshgrid.Mesh
            Scaled mesh.
    """
    wrapper = meshgrid.wrappers.MrgMesh(mesh)
    sxyz = numpy.resize(numpy.asarray(scaling_vector, dtype='float64'), (3,))
    new_mesh = wrapper.mesh
    if inplace is False:
        new_mesh = copy.deepcopy(wrapper.mesh)
    translation = (sxyz-1)*numpy.asarray(center, dtype='float64')
    new_mesh.node_coords *= sxyz
    new_mesh.node_coords += translation
    return new_mesh


def rotate_z(mesh, angle=0.0, center=(0.0, 0.0, 0.0), inplace=True, unit='radians'):
    wrapper = meshgrid.wrappers.MrgMesh(mesh)
    dim = wrapper.ambient_dim
    if unit.lower() in ('deg', 'd', 'degrees'):
        angle = math.radians(angle)
    c, s = math.cos(angle), math.sin(angle)
    rot_matrix = numpy.identity(dim, dtype=numpy.float64)
    rot_matrix[0, 0:2] = [c, -s]
    rot_matrix[1, 0:2] = [s, c]
    trans = numpy.zeros((dim,), dtype=numpy.float)
    n_fill = min(dim, len(center))
    trans[0:n_fill] = center[0:n_fill]
    new_mesh = wrapper
    if inplace is False:
        new_mesh = meshgrid.wrappers.MrgMesh(copy.deepcopy(wrapper.mesh))
    for i in range(wrapper.numb_nodes):
        new_mesh.points[i, :] -= trans
        numpy.dot(rot_matrix, new_mesh.points[i, :], out=new_mesh.points[i, :])
        new_mesh.points[i, :] += trans
    return new_mesh.mesh


def translate(mesh, vector, inplace=True):
    """Translate mesh by vector.

    Parameters
    ----------
        mesh : genfemcalc.meshgrid.Mesh
            Mesh to translate
        vector : array like
            Translation vector of [x,y,z] coordinates
        inplace : bool (optional)
            If True (default) operation is done on the mesh argument.
            If False created deep copy of the mesh first
    Returns
    -------
        genfemcalc.meshgrid.Mesh
            Translated mesh
    """
    wrapper = meshgrid.wrappers.MrgMesh(mesh)
    new_mesh = wrapper.mesh
    if inplace is False:
        new_mesh = copy.deepcopy(wrapper.mesh)
    translation = numpy.asarray(vector, dtype='float64')
    new_mesh.node_coords += translation
    return new_mesh


def anchorMeshAt(mesh, anchor_point, mode='lbf', inplace=True):
    """Translate mesh in such way that a specific point of mesh is at anchor point
    The mesh specific point is designated by string:

        * ``'lbf'`` - left-bottom-front point of mesh bounding box
        * ``'rtb'`` - right-top-back point of mesh bounding box
        * ``'center'`` - center point of mesh bounding box

    Instead of ``'lbf'`` and `'rtb'` full names can be used: ``'left-bottom-front'``
    and ``'right-top-back'``, respectively.

    Parameters
    ----------
        mesh : genfemcalc.meshgrid.Mesh
            Mesh to anchor
        anchor_point : array-like
            Point to anchor mesh at
        mode : string
            Mesh point to be anchored: ``'lbf'`` or ``'rtb'`` or ``'center'``
        inplace : bool
            If True operation is done on the input mesh, otherwise new deep copy
            is created
    Returns
    -------
        The anchored mesh.
    """
    anchor = numpy.asarray(anchor_point, dtype='float64')
    box = BoundingBox.fromMrgMesh(mesh)
    if mode == 'center':
        translation = anchor-box.center
    elif mode in ['lbf', 'left-bottom-front']:
        translation = anchor-box.lbf
    elif mode in ['rtb', 'right-top-back']:
        translation = anchor-box.rtb
    else:
        raise ValueError(f"Invalid value for anchoring mode: {mode}")
    return translate(mesh, translation, inplace)


def cellApproxBBox(mesh, cell_id, interleaved=True):
    """Return cell approximate bounding box. Approximate means that bounding
    box is calculated for polygonal(polyhedral) vertex approximation of the cell

    Parameters
    ----------
        mesh :
            MRG compatible mesh
        cell_id: :obj:`int`
            cell index
        interleaved : :obj:`bool`
            If interleaved is False, the coordinates are in the
            form[xmin, xmax, ymin, ymax, ..., ..., kmin, kmax].If
            interleaved is True, the coordinates are int the
            form[xmin, ymin, ..., kmin, xmax, ymax, ..., kmax]
    Returns
    -------
    :obj:`ndarray`
        1D array of flattened box coordinates
    """
    mrgmesh = meshgrid.wrappers.MrgMesh(mesh)
    cell_points = mrgmesh.getCellPoints(cell_id)
    corners = numpy.empty((2, 3), dtype=numpy.float64)
    corners[0, :] = numpy.min(cell_points, axis=0)
    corners[1, :] = numpy.max(cell_points, axis=0)
    if interleaved:
        return corners.flatten(order='C')
    else:
        return corners.flatten(order='F')


class MeshGeometry(object):
    def __init__(self, mesh):
        self.mesh = mesh

    @classmethod
    def fromMrgMesh(cls, mesh):
        if isinstance(mesh, cls):
            return mesh
        else:
            return cls(meshgrid.wrappers.MrgMesh.wrap(mesh))

    def getCellPoints(self, cell_id):
        nodes = self.mesh.getCellNodes(cell_id)
        return self.mesh.points[nodes, :]

    def getCellApproxBarycenter(self, cell_id):
        return numpy.mean(self.getCellPoints(cell_id), axis=0)

    @staticmethod
    def isPointInBox(point, box):
        pt = numpy.asarray(point)
        ca = numpy.asarray(box[0])
        cb = numpy.asarray(box[1])
        if numpy.all(pt >= ca) and numpy.all(pt <= cb):
            return True
        else:
            return False

    @staticmethod
    def arePointsInBox(points, box, mode='all'):
        if mode == 'all':
            for point in points:
                if not MeshGeometry.isPointInBox(point, box):
                    return False
            return True
        elif mode == 'any':
            for point in points:
                if MeshGeometry.isPointInBox(point, box):
                    return True
            return False
        else:
            raise ValueError(f"Invalid mode : {mode}, expected: 'all' or 'any'")

    def isNodeInBox(self, node_id, box):
        return self.isPointInBox(self.mesh.points[node_id, :], box)

    def isCellInBox(self, cell_id, box, mode='all'):
        return self.arePointsInBox(self.getCellPoints(cell_id), box, mode)
