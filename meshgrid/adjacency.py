# -*- coding: utf-8 -*-
"""Provide utilites for mesh adjacency maniputlations.

This module provides data structures and algorithms for manipulation of mesh
adjacency. The `meshgrid.Mesh` class holds only the basic combinatorial data
for mesh, that is adjacency between cells and nodes over which the cell
is spanned. This combinatorial mesh structure is the basis for core Finite
Element Method algorithm. However many mesh manipulation require more elaborate
combinatorial structure in order for some manipulations to be possible or
effectively done. Some of these more elaborate structures are provided here.

The classes offered by this module:

*  :class:`Adjacency` -- light wrapper around adjacency stored in :class:`meshgrid.Mesh` to
    make manipulation of them easier and more concise.
*  :class:`AdjacencyCell` -- encapsulate combinatorial structure of the given mesh cell.
    This structure is derived from the combinatorial structure of
    the reference cell and  mesh cell to nodes adjacency data.
*  :class:`DynamicAdjacency` -- data structure build by linking neighbouring :class:`AdjacencyCell`
    objects. Essentially this data structure provides cell to cell adjacency.
* :class:`Skinner` -- allows to find and hold data about mesh boundary cells.
*  :class:`MeshMaskFilter` - allows to build submeshes from cells' boolean masks.

The algorithms implemented by this module

* inverting adjacency - function :func:`invertAdjacency`
* linking adjacent cells - function :func:`establishNeighbourhood`

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.13 11:21:7'
__version__ = '0.2.0'

import numpy as np
import collections
import itertools

from genfemcalc import meshgrid

def mapOnGlobalIds(local_lists, global_ids):
    return [[global_ids[i] for i in item] for item in local_lists]


def mapOnGlobalIdsAsSet(local_lists, global_ids):
    return [frozenset([global_ids[i] for i in item]) for item in local_lists]


class InverseMapper(object):
    def __init__(self, l2g):
        self.l2g = l2g
        self.g2l = dict(zip(l2g, np.arange(0, len(l2g), dtype='int64')))
        self.mapper = np.vectorize(lambda x: self.g2l[x])

    def __call__(self, array):
        return self.mapper(np.asarray(array))


class Adjacency(object):
    """Wrapper over adjacency values and pointers

    This is just thin wrapper over adjacency relation stored in two 1D arrays.
    The purpose is this wrapper is to simplify iteration and data access.
    """
    def __init__(self, adj, p_adj):
        self.adj = adj
        self.p_adj = p_adj

    @property
    def dtype(self):
        return self.adj.dtype

    @classmethod
    def fromMesh(cls, mesh, copy=False):
        if copy:
            adj = np.copy(mesh.elem2nodes)
            p_adj = np.copy(mesh.p_elem2nodes)
        else:
            adj = mesh.elem2nodes
            p_adj = mesh.p_elem2nodes
        return cls(adj, p_adj)

    def __getitem__(self, item):
        return self.adj[self.p_adj[item]:self.p_adj[item+1]]

    def __len__(self):
        return len(self.p_adj)-1

    def getElemDofs(self, item):
        """FIXME - implement handling of qdim
        FIXME - move totaly out of this class
        """
        return self[item]

    def count(self, cell_id):
        return self.p_adj[cell_id+1]-self.p_adj[cell_id]

    def inverse(self):
        adj_inv, p_adj_inv = invertAdjacency(self.adj, self.p_adj)
        return Adjacency(adj_inv, p_adj_inv)


class AdjacencyCell(object):
    __slots__ = ['parent', 'id', 'type', 'ordered_nodes', 'nodes', 'neighbour_links',
                 'edges', 'faces', 'is_boundary']

    def __init__(self, parent, cell_id, cell_type, nodes):
        self.parent = parent
        self.id = cell_id
        self.type = cell_type
        self.ordered_nodes = list(nodes)
        self.nodes = frozenset(nodes)
        self.neighbour_links = []
        self.edges = mapOnGlobalIdsAsSet(self.refcell.edges, nodes)
        self.faces = mapOnGlobalIdsAsSet(self.refcell.faces, nodes)
        self.is_boundary = None

    @property
    def facets(self):
        if self.dim == 3:
            return self.faces
        elif self.dim == 2:
            return self.edges
        elif self.dim == 1:
            on = self.ordered_nodes
            return [{on[0]}, {on[-1]}]
        else:
            on = self.ordered_nodes
            return [{on[0]}]

    @property
    def numb_facets(self):
        return self.refcell.numb_facets

    @property
    def facets_type(self):
        return self.refcell.facets_type

    @property
    def dim(self):
        return self.refcell.dim

    @property
    def refcell(self):
        return self.parent.refcells[self.type]

    def getFacetNodes(self, idx):
        f = self.refcell.facets[idx]
        return [self.ordered_nodes[i] for i in f]

    @property
    def neighbours(self):
        return map(lambda x: x[1], self.neighbour_links)

    def isNeighbourTo(self, other):
        """Check if one cell is neighbour to another.
        Being neighbours means that two cells share common facet
        or one cell is subcell of another, which means it is
        either face or edge or vertex of another cell
        """
        return other in self.neighbours

    def countNeighbours(self, dim=None):
        if dim is None:
            return len(self.neighbour_links)
        else:
            counter = 0
            for cell in self.neighbours:
                if cell.dim == dim:
                    counter += 1
            return counter

    def getNeighbourFacetIds(self, dim):
        return [x[0] for x in self.neighbour_links if x[1].dim == dim]

    def getNeighbourLinks(self, dim):
        return [x for x in self.neighbour_links if x[1].dim == dim]

    def isSubCellOf(self, other):
        return self.nodes.issubset(other.nodes)

    def findCommonFacetWith(self, other, seed=None):
        common_nodes = self.nodes.intersection(other.nodes)
        if not common_nodes:
            return None
        else:
            local_in_self = self.findFacet(common_nodes, seed)
            local_in_other = other.findFacet(common_nodes, seed)
            if local_in_self != -1 and local_in_other != -1:
                return local_in_self, local_in_other
            else:
                return None

    def findFacet(self, facet, seed=None):
        facet_set = set(facet)
        for myfacet, i in zip(self.facets, itertools.count()):
            if seed is not None:
                if seed not in myfacet:
                    continue
            if myfacet == facet_set:
                return i
        return -1


def establishNeighbourhood(cell, other_cell, seed=None):
    """Check if cells are neighbours and if so record that information in them
    """
    if cell.isNeighbourTo(other_cell):
        return True
    elif cell is other_cell:
        return False
    else:
        locator = None
        if cell.dim == other_cell.dim:
            locator = cell.findCommonFacetWith(other_cell, seed)
        elif cell.dim > other_cell.dim:
            if other_cell.isSubCellOf(cell):
                in_cell = cell.findFacet(other_cell.nodes, seed)
                assert(in_cell > -1)
                locator = (in_cell, -1)
        else:
            if cell.isSubCellOf(other_cell):
                in_other = other_cell.findFacet(cell.nodes, seed)
                assert(in_other > -1)
                locator = (-1, in_other)
        if locator is not None:
            cell.neighbour_links.append((locator[0], other_cell))
            other_cell.neighbour_links.append((locator[1], cell))
        return locator is not None


class DynamicAdjacency(object):
    """Holds additional adjacency structure to those held in a underlying mesh.

    The additional adjacency structure allows easier queries on the expense of additional
    storage and time to setup it. The adjacency structure is build on top of AdjacencyCell
    object that hold two levels of adjacency information - adjacency structure internal to
    a cell and adjacency structure between cells.

    Attributes
    ----------
        _refcells : :obj:`dict` of :obj:`refcells.RefCell`
        _locked : :obj:`bool`
            If True it means that DynamicAdjacency was build from existing mesh and dynamic
            update of the adjacency is suppressed.
    """
    def __init__(self):
        self._cells = dict()
        self._refcells = None
        self._dim = 0
        self._nodes = set()
        self._c2c_storage_size = 0
        self.dtype = np.int64
        self._locked = False
        self._mesh = None

    @property
    def locked(self):
        return self._locked

    @property
    def numb_cells(self):
        return len(self._cells)

    @property
    def numb_nodes(self):
        return len(self._nodes)

    @property
    def dim(self):
        return self._dim

    @property
    def mesh(self):
        if self._locked:
            return self._mesh
        else:
            return self.buildMesh()

    @property
    def refcells(self):
        return self._refcells

    @classmethod
    def fromMesh(cls, mesh):
        """Build adjacency structure from underlying mesh.
        """
        self = cls()
        self._mesh = mesh
        self._locked = True
        self._refcells = meshgrid.refcells.buildRefcellsDict(mesh)
        cell2node = Adjacency.fromMesh(mesh)
        assert(len(mesh.elem_geotype) == len(cell2node))
        for cell_id, cell_type, nodes in zip(itertools.count(), mesh.elem_geotype, cell2node):
            self._cells[cell_id] = AdjacencyCell(self, cell_id, cell_type, nodes)
        self._setupNeighbouringRelation(cell2node)
        self._setupDimension()
        return self

    def _setupDimension(self):
        self._dim = 0
        for refcell in self._refcells.values():
            self._dim = max(self._dim, refcell.dim)

    def _setupNeighbouringRelation(self, cell2node):
        inv_adjacency = cell2node.inverse()
        for cell in self:
            for node in cell.nodes:
                for other_id in inv_adjacency[node]:
                    if other_id is not self:
                        establishNeighbourhood(cell, self._cells[other_id])
            self._c2c_storage_size += cell.countNeighbours()
            self._nodes.update(cell.nodes)

    def __iter__(self):
        return iter(self._cells.values())

    def buildMesh(self):
        raise NotImplementedError('Ask Roman')

    def getCell2Cell(self):
        """Return Adjacency between cells
        """
        p_adj = np.empty((len(self._cells)+1,), dtype=self.dtype)
        adj = np.empty((self._c2c_storage_size,), dtype=self.dtype)
        last = 0
        for i, cell in enumerate(self):
            nn = cell.countNeighbours()
            adj[last:last+nn] = cell.neighbours
            p_adj[i] = last
            last += nn
        p_adj[-1] = last
        return Adjacency(adj, p_adj)


class Skinner(object):
    """Class providing methods to manipulate mesh ``skin'' that is the boundary
    of a mesh
    """
    def __init__(self, adjacency_structure):
        self.structure = adjacency_structure
        self.__has_marked_boundary = False
        self.__has_calculated_size = False
        self._l2g_nodes = None

    @property
    def mesh(self):
        return self.structure.mesh

    @classmethod
    def fromMesh(cls, mesh):
        return cls(DynamicAdjacency.fromMesh(mesh))

    def l2g_nodes(self):
        if self._l2g_nodes is None:
            raise RuntimeError("You have to build mesh first with buildMesh()")
        return self._l2g_nodes

    def buildMesh(self):
        """Build boundary mesh
        """
        self.markBoundaryCells()
        numb_skin_cells, adj_len = self.calculateSkinAdjacencySize()
        if numb_skin_cells < 1:
            raise ValueError("Mesh does not have boundary")
        mesh = meshgrid.Mesh()
        dtype = self.structure.dtype
        mesh.elem2nodes = np.empty((adj_len,), dtype=dtype)
        mesh.elem_geotype = np.empty(numb_skin_cells, 'int')
        mesh.p_elem2nodes = np.empty(numb_skin_cells+1, dtype=dtype)
        mesh.p_elem2nodes[0] = 0
        mesh._updateCounts()
        i = 0

        for cell in self.structure:
            if cell.is_boundary and cell.dim == self.structure.dim:
                neighbour_facets = cell.getNeighbourFacetIds(cell.dim)
                for fid in range(cell.numb_facets):
                    if fid not in neighbour_facets:
                        nodes = cell.getFacetNodes(fid)
                        cs = mesh.p_elem2nodes[i]
                        ce = cs+len(nodes)
                        mesh.elem2nodes[cs:ce] = nodes
                        mesh.p_elem2nodes[i+1] = ce
                        mesh.elem_geotype[i] = cell.facets_type[fid]
                        i += 1
        self._l2g_nodes = np.unique(mesh.elem2nodes)
        mesh.node_coords = self.structure.mesh.node_coords[self._l2g_nodes, :]
        to_local = InverseMapper(self._l2g_nodes)
        mesh.elem2nodes = to_local(mesh.elem2nodes)
        mesh._updateCounts()
        return mesh

    def calculateSkinAdjacencySize(self):
        """Calculate the number skin elements and the size of array
        to store the skin cell to node adjacency
        """
        adj_len = 0
        numb_skin_cells = 0
        for cell in self.structure:
            if cell.is_boundary and cell.dim == self.structure.dim:
                neighbour_facets = cell.getNeighbourFacetIds(cell.dim)
                for fid in range(cell.numb_facets):
                    if fid not in neighbour_facets:
                        numb_skin_cells += 1
                        adj_len += len(cell.facets[fid])
        return numb_skin_cells, adj_len

    def markBoundaryCells(self):
        if not self.__has_marked_boundary:
            self.__has_marked_boundary = True
            for cell in self.structure:
                cell.is_boundary = False
                if cell.dim == self.structure.dim:
                    if cell.countNeighbours(cell.dim) != cell.numb_facets:
                        cell.is_boundary = True
                elif cell.dim == self.structure.dim-1:
                    if cell.countNeighbours(self.structure.dim) != 2:
                        cell.is_boundary = True


class MeshMaskFilter(object):
    def __init__(self, mesh, mask=None, l2g=None):
        self.mesh = mesh
        self.cell2node = Adjacency.fromMesh(mesh)
        self.mask = mask
        if self.mask is None:
            self.resetMask()
        self._master_l2g = l2g
        self._l2g_nodes = None

    def setMask(self, mask):
        exp = len(self.cell2node)
        act = len(mask)
        if len(mask) != len(self.cell2node):
            raise ValueError(f"Invalid mask size : expected {exp} got {act}")
        self.mask = np.asarray(mask, dtype='bool')

    def makeUnionMask(self, *masks):
        for m in masks:
            np.logical_or(np.asarray(m, dtype='bool'), self.mask, out=self.mask)
        return self.mask

    def makeIntersectionMask(self, *masks):
        for m in masks:
            np.logical_and(np.asarray(m, dtype='bool'), self.mask, out=self.mask)
        return self.mask

    def negateMask(self):
        np.logical_not(self.mask, out=self.mask)
        return self.mask

    def resetMask(self, value=True):
        self.mask = value*np.ones(len(self.cell2node, ), dtype='bool')

    def l2g_nodes(self):
        if self._l2g_nodes is None:
            raise RuntimeError("You have to build mesh first with buildMesh()")
        return self._l2g_nodes

    def calculateMaskAdjacencySize(self):
        """Calculate the number submesh elements and the size of array
        to store the submesh cell to node adjacency
        """
        adj_len = 0
        numb_submesh_cells = 0
        for cell_id in range(self.mesh.numb_elems):
            if self.mask[cell_id]:
                numb_submesh_cells += 1
                adj_len += self.cell2node.count(cell_id)
        return numb_submesh_cells, adj_len

    def buildMesh(self):
        numb_submesh_cells, adj_len = self.calculateMaskAdjacencySize()
        mesh = meshgrid.Mesh()
        if numb_submesh_cells < 1:
            raise RuntimeWarning("The masked submesh is empty")
        dtype = self.cell2node.dtype
        mesh.elem2nodes = np.empty((adj_len,), dtype=dtype)
        mesh.elem_geotype = np.empty(numb_submesh_cells, 'int')
        mesh.p_elem2nodes = np.empty(numb_submesh_cells+1, dtype=dtype)
        mesh.p_elem2nodes[0] = 0
        mesh._updateCounts()
        i = 0
        for cell_id in range(self.mesh.numb_elems):
            if self.mask[cell_id]:
                nodes = self.cell2node[cell_id]
                cs = mesh.p_elem2nodes[i]
                ce = cs+len(nodes)
                mesh.elem2nodes[cs:ce] = nodes
                mesh.p_elem2nodes[i+1] = ce
                mesh.elem_geotype[i] = self.mesh.elem_geotype[cell_id]
                i+=1
        self._l2g_nodes = np.unique(mesh.elem2nodes)
        mesh.node_coords = self.mesh.node_coords[self._l2g_nodes, :]
        to_local = InverseMapper(self._l2g_nodes)
        mesh.elem2nodes = to_local(mesh.elem2nodes)
        if self._master_l2g is not None:
            self._l2g_nodes = self._master_l2g[self._l2g_nodes]
        mesh._updateCounts()
        return mesh


def getMeshMaskFromBox(mesh, box, masking_type='barycenter'):
    meshgeom = meshgrid.geometrymrg.MeshGeometry.fromMrgMesh(mesh)
    mask = np.empty((meshgeom.mesh.numb_elems,), dtype='bool')
    if masking_type == 'barycenter':
        def validator(cid):
            return meshgrid.geometrymrg.MeshGeometry.isPointInBox(meshgeom.getCellApproxBarycenter(cid), box)
    elif masking_type in ['all', 'cell_all_nodes']:
        def validator(cid):
            pts = meshgeom.getCellPoints(cid)
            return meshgrid.geometrymrg.MeshGeometry.arePointsInBox(pts, box, mode='all')
    elif masking_type in ['any', 'cell_any_node']:
        def validator(cid):
            pts = meshgeom.getCellPoints(cid)
            return meshgrid.geometrymrg.MeshGeometry.arePointsInBox(pts, box, mode='any')
    else:
        valid = ['barycenter', 'all', 'cell_all_nodes', 'any', 'cell_any_node']
        raise ValueError(f"Invalid masking type '{masking_type}'. Expected one of {valid}")

    for cell_id in range(meshgeom.mesh.numb_elems):
        mask[cell_id] = validator(cell_id)

    return mask


def invertAdjacency(adj, p_adj):
    """Compute inverse adjacency tables.

    Adjacency is relation  source->targets.
    This adjacency is stored in two 1D arrays adj and p_adj.
    The adj array stores targets for consecutive sources while
    array p_adj stores index where of targes for the sources begin.
    The convention is that p_adj stores one element more than the number
    of sources so p_adj[i+1] - p_adj[i] is valid for each i in sources
    and equla to the number of targets for source i.
    """
    counter = collections.Counter(adj)
    number_of_targets = len(counter)
    adj_inv = np.empty(adj.shape, dtype=adj.dtype)
    p_adj_inv = np.zeros((number_of_targets+1,), dtype=adj.dtype)
    offset = np.zeros((number_of_targets,), dtype=adj.dtype)
    # Count occurrence of targets
    for trg in range(number_of_targets):
        p_adj_inv[trg+1] = counter[trg]
    np.cumsum(p_adj_inv, out=p_adj_inv)
    for ss, se, src in zip(p_adj, p_adj[1:], itertools.count(0)):
        # adj[ss,se] is the array of targets for the source src
        # the plus 1 below is in order to access proper place in adj_inv_p
        trg = adj[ss:se]
        placement = p_adj_inv[trg] + offset[trg]
        adj_inv[placement] = src
        offset[trg] += 1
    return adj_inv, p_adj_inv


def invertAdjacencyOld(adj, p_adj):
    """Invert adjacency stored in two 1D arrays of values and boundaries.

    Example:
    Adjacency between 3 elements (2 quads and one segment) and 6 nodes
    p_adj   [0, 4, 8, 10]
    adj         [0, 1, 2, 3, 1, 4, 5, 2, 2, 3]
    str_indices [0, 0, 0, 0, 1, 1, 1, 1, 2, 2]
    trg_sorts = [0, 1, 4, 2, 7, 8, 3, 9, 5, 6]
    trg =       [0, 1, 1, 2, 2, 2, 3, 3, 4, 5]
    adj_inv     [0, 0, 1, 0, 1, 2, 0, 2, 1, 1]
    adj_inv_p   [0, 1, 3, 6, 8, 9, 10]
    adj_inv     [0 | 0 1 |  0 1 2 | 0 2 | 1 | 1]

    .. code-block:: text

                  2
               3    2     5
                  0    1
               0    1     4

    """
    dtype = adj.dtype
    num_src = len(p_adj) - 1
    src_indices = np.repeat(np.arange(num_src, dtype=dtype), np.diff(p_adj))
    trg_sorts = np.argsort(adj)
    trg = adj[trg_sorts]
    adj_inv = src_indices[trg_sorts]
    num_trg = trg[-1]+1
    p_adj_inv = np.empty(num_trg+1, dtype=dtype)
    p_adj_inv[0:-1] = np.where(np.roll(trg, 1) != trg)[0]
    p_adj_inv[-1] = len(adj_inv)
    return adj_inv, p_adj_inv
