# -*- coding: utf-8 -*-
"""TODO : Add description of demo demo_polysoup_to_vtk.py
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '11/30/2019'
__version__ = '0.1.0'

import os
import shapely

import genfemcalc
from genfemcalc import examples
from genfemcalc.meshgrid import utilsshapely

PKGDIR = os.path.dirname(os.path.abspath(genfemcalc.__file__))
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoPolySoupToVTK(examples.Demo):
    @staticmethod
    def build_sample_polysoup():
        poly_soup = []
        poly_spec = {'center': (0.0, 0.0), 'out_radius': 1.0,
                     'nsides': 4, 'start_angle': 45.0, 'randomize_angle': False}
        poly_soup.append(utilsshapely.buildRandomPolygon(**poly_spec))
        poly_spec['center'] = (1.0, 1.0)
        poly_soup.append(utilsshapely.buildRandomPolygon(**poly_spec))
        poly_spec['center'] = (-2.0, 2.0)
        outer = utilsshapely.buildRandomPolygon(**poly_spec)
        poly_spec['center'] = (-2.2, 2.0)
        poly_spec['out_radius'] = 0.1
        inner1 = utilsshapely.buildRandomPolygon(**poly_spec)
        poly_spec['center'] = (-1.8, 2.0)
        inner2 = utilsshapely.buildRandomPolygon(**poly_spec)
        holes = shapely.geometry.MultiPolygon((inner1, inner2))
        diff = outer.difference(inner1)
        diff = diff.difference(inner2)
        poly_soup.append(diff)
        poly_soup.append(holes)
        return poly_soup

    def demonstrate(self):
        poly_soup = DemoPolySoupToVTK.build_sample_polysoup()
        outfile = os.path.join(OUTDIR, 'polysoup.vtk')
        utilsshapely.writeVtkFromPolySoup(outfile, poly_soup)

if __name__ == '__main__':
    demo = DemoPolySoupToVTK()
    demo.run()
