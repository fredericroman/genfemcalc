genfemcalc.fembasic.linefem module
==================================

.. automodule:: genfemcalc.fembasic.linefem
   :members:
   :undoc-members:
   :show-inheritance:
