 # -*- coding: utf-8 -*-
"""TODO : Add description of demo ${FILE_NAME}
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '${DATE}'
__version__ = '0.1.0'

import os

import femcalc
from femcalc import examples

PKGDIR = os.path.dirname(femcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')

class ${DEMO_CLASS_NAME}(examples.Demo):
    def demonstrate(self):
        pass  # TODO add demonstration code
        
if __name__ == '__main__':
    demo = ${DEMO_CLASS_NAME}()
    demo.run()
