# -*- coding: utf-8 -*-
"""This module provides utilities for calculation on mesh level, for instance
integration of fields over meshes.


"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.14 6:45:12'
__version__ = '0.3.0'

import numpy
import math

from genfemcalc import fem
from genfemcalc import meshgrid


class MeshMapper(object):
    """Performs mapping from reference to physical element for each element
    of associated mesh.
    """
    def __init__(self, mesh, ambient_dim=None):
        self._mesh = meshgrid.wrappers.MrgMesh.wrap(mesh)
        if ambient_dim is None:
            self._ambient_dim = self._mesh.ambient_dim
        else:
            self._ambient_dim = ambient_dim
        self._mappers = {}
        self._setup()

    def _setup(self):
        self._dim = 0
        for tag in self._mesh.unique_cell_types:
            cell = meshgrid.refcells.getRefCell('vtk', tag)
            self._dim = max(self._dim, cell.dim)
            self._mappers[tag] = fem.manifolds.GeomMapper(cell, self._ambient_dim)

    @property
    def mesh(self):
        return self._mesh

    @property
    def dim(self):
        return self._dim

    @property
    def ambient_dim(self):
        return self._ambient_dim

    @property
    def numb_nodes(self):
        return self._mesh.numb_nodes

    @property
    def numb_cells(self):
        return self._mesh.numb_elems

    def getMapperForCellType(self, cell_type):
        return self._mappers[cell_type]

    def getMapper(self, cell_index):
        """Return GeomMapper appropriate for give cell
        Parameters
        ----------
            cell_index : int
                Element index
        Returns
        -------
            GeomMapper
                Geometric mapper for element
        """
        cell_type = self._mesh.cell2type[cell_index]
        cell_nodes = self.getCellNodes(cell_index)
        nodes_coords = self._mesh.points[cell_nodes]
        mapper = self._mappers[cell_type]
        mapper.setNodes(nodes_coords)
        return mapper

    def mapPoints(self, cell_index, points):
        """For given mesh cell map points given by reference coordinates to physical coordinates

        Parameters
        ----------
            cell_index : int
                Index of cell
            points : ndarray like
                Array of shape (n, d) where n is number of points and d >= dimension of RefCell
                associated with given element.
        Returns
        -------
            ndarray
                Array of physical point coordinates. The array shape is (n, c)
                where n is the number of mapped point and c == ambient dimension of the mesh mapper.
        """
        mapper = self.getMapper(cell_index)
        return mapper.mapPoints(points)

    def getCellNodes(self, cell_index):
        """Return node indices of given cell

        Parameters
        ----------
            cell_index : int
                Cell index

        Returns
        -------
            ndarray
                Array of node indices
        """
        return self._mesh.getCellNodes(cell_index)

    def getNodesCoords(self, node_indices):
        """Return array of coordinates of nodes with given indices
        Parameters
        ----------
            node_indices, array like
        Returns
        -------
            ndarray
                Array of shape (n, c) where n is is the number of given indices
                and c is the ambient dimension of the mapper
        """
        return self._mesh.node_coords[node_indices]


class MeshIntegrator(MeshMapper):
    """MeshIntegrator support integration over cells of a mesh by integrating
    over reference cells associated with physical cells of a mesh
    """
    def __init__(self, mesh, ambient_dim=None, quadratures=None, ngpt=None):
        super().__init__(mesh, ambient_dim)
        if ngpt is None:
            ngpt = 2**self.ambient_dim
        self.quadratures = fem.quadratures.QuadratureRegistry()
        if quadratures is not None:
            for quadrature in quadratures:
                self.quadratures.add(quadrature)
        for mapper in self._mappers.values():
            refcell = mapper.refcell
            quadrature = self.quadratures.get(refcell.type)
            if quadrature is None:
                quadrature = self.quadratures.setForRefcell(refcell, ngpt)
            mapper.precomputeForQuadrature(quadrature)

    def integrate(self, element_function, dtype=numpy.float64):
        integral = 0.0
        for cell_id in self.mesh.cells():
            mapper = self.getMapper(cell_id)
            if mapper.dim != self.dim:
                continue
            val = element_function(elem=cell_id, mapper=mapper, dtype=dtype)
            volume_form = mapper.volumeForm
            # integral += numpy.einsum('g...,g,g->...', val, volume_form,
            #                        mapper.quadrature.weights, dtype=dtype)
            integral += numpy.sum(val*volume_form*mapper.quadrature.weights)
        return integral


class ConstFieldEvaluator(object):
    def __init__(self, mesh_mapper, value):
        self.value = value
        self.mesh_mapper = mesh_mapper

    @property
    def mesh(self):
        return self.mesh_mapper.mesh

    def evalUatStoredPoints(self, elem, dtype=numpy.float64, **kwargs):
        mapper = self.mesh_mapper.getMapper(elem)
        return numpy.full((mapper.numb_ref_points,), fill_value=self.value, dtype=dtype)

    def evalU2atStoredPoints(self, elem, dtype=numpy.float64, **kwargs):
        mapper = self.mesh_mapper.getMapper(elem)
        return numpy.full((mapper.numb_ref_points,), value=self.value**2, dtype=dtype)

    def evalGradUGradUatStoredPoints(self, elem, dtype=numpy.float64, **kwargs):
        mapper = self.mesh_mapper.getMapper(elem)
        return numpy.zeros((mapper.numb_ref_points,),  dtype=dtype)


class MeshFieldEvaluator(object):
    def __init__(self, mesh_mapper, field_data):
        MeshFieldEvaluator._checkFieldShape(field_data)
        self.mesh_mapper = mesh_mapper
        self.elem2nodes = mesh_mapper.mesh.elem2nodes
        self.field_data = numpy.resize(field_data, (field_data.size,))
    @property
    def mesh(self):
        return self.mesh_mapper.mesh

    @staticmethod
    def _checkFieldShape(field_data):
        field_shape = field_data.shape
        if numpy.prod(field_shape) != field_data.size:
            raise RuntimeError("Only interpolation of scalar fields is supported")

    def evalUatStoredPoints(self, elem, dtype=numpy.float64, **kwargs):
        mapper = self.mesh_mapper.getMapper(elem)
        elem_dofs = self.elem2nodes.getElemDofs(elem)
        field_at_dofs = self.field_data[elem_dofs]
        base_functions = mapper.precomputed_N  # (g,n) shaped matrix
        interpolated_field = numpy.einsum('gn,n->g', base_functions, field_at_dofs, dtype=dtype)
        return interpolated_field

    def evalU2atStoredPoints(self, elem, dtype=numpy.float64, **kwargs):
        interpolated_field = self.evalUatStoredPoints(elem, dtype)
        return interpolated_field**2

    def evalU4atStoredPoints(self, elem, dtype=numpy.float64, **kwargs):
        interpolated_field = self.evalUatStoredPoints(elem, dtype)
        return interpolated_field**4

    def evalGradUGradUatStoredPoints(self, elem, dtype=numpy.float64, **kwargs):
        mapper = self.mesh_mapper.getMapper(elem)
        elem_dofs = self.elem2nodes.getElemDofs(elem)
        field_at_dofs = self.field_data[elem_dofs]
        jacobian = mapper.jacobian

        def jjtinv(jmat):
            return numpy.linalg.inv(numpy.matmul(jmat, jmat.T))

        jjt_inv = numpy.array([jjtinv(jmat) for jmat in jacobian])
        # noinspection PyPep8Naming
        dN = mapper.precomputed_dN
        tensors = (field_at_dofs, dN, jjt_inv, dN, field_at_dofs)
        formula = 'i,gsi,gst,gtj,j->g'
        interpolated_field = numpy.einsum(formula, *tensors, dtype=dtype)
        return interpolated_field

    def evalAtStoredPoints(self, elem, mode='u', dtype=numpy.float64, **kwargs):
        if mode == 'u':
            return self.evalUatStoredPoints(elem, dtype=dtype, **kwargs)
        elif mode == 'u*u':
            return self.evalU2atStoredPoints(elem, dtype=dtype, **kwargs)
        elif mode == 'u**4':
            return self.evalU4atStoredPoints(elem, dtype=dtype, **kwargs)
        elif mode == 'gradu*gradu':
            return self.evalGradUGradUatStoredPoints(elem, dtype)
        else:
            raise ValueError('Invalid interpolation mode {mode}. Expected u or gradu')


def computeFieldIntegral(mesh_integrator, field_data, integrand_type='u', dtype=numpy.float64):
    """Construct field integrand and integrate it over mesh.
    """
    integrand_type = integrand_type.replace(' ', '').lower()
    interpolator = MeshFieldEvaluator(mesh_integrator, field_data)
    integrands = {'u': interpolator.evalUatStoredPoints,
                  'u*u': interpolator.evalU2atStoredPoints,
                  'u**4': interpolator.evalU4atStoredPoints,
                  'gradu*gradu': interpolator.evalGradUGradUatStoredPoints
                  }
    if integrand_type not in integrands:
        expected = list(integrands.keys())
        raise ValueError(f"Invalid integrand {integrand_type}. Expected one of {expected}")

    def integrand(elem, **_unused):
        return integrands[integrand_type](elem, dtype)

    return mesh_integrator.integrate(integrand, dtype=dtype)


def computeFieldNormL2(mesh_integrator, field_data, dtype=numpy.float64):
    return math.sqrt(computeFieldIntegral(mesh_integrator, field_data, integrand_type='u*u',
                                          dtype=dtype))


def computeFieldNormH1(mesh_integrator, field_data, dtype=numpy.float64):
    norm2 = 0.0
    for integrand in ['u*u', 'gradu*gradu']:
        norm2 += computeFieldIntegral(mesh_integrator, field_data, integrand_type=integrand,
                                      dtype=dtype)
    return math.sqrt(norm2)
