.. include:: ../../replaces.txt
.. _meshing-sec:

***************************************
Geometric Modelling and Mesh Generation
***************************************

Why we need geometric models
----------------------------

Categories of geometric models
------------------------------

Programming geometric models in GMSH
------------------------------------

Mesh generation algorithms
--------------------------

Mesh generation with GMSH
-------------------------

Mesh optimisation
-----------------

Evaluating mesh quality
^^^^^^^^^^^^^^^^^^^^^^^

Mesh refinement and coarsening
------------------------------



