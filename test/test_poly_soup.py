"""
Test operations on poly soup
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '11/23/2019'
__version__ = '0.1.0'

import shapely
import unittest
import genfemcalc
from genfemcalc import meshgrid
from genfemcalc.meshgrid.utilsshapely import buildRandomPolygon


class TestPolySoup(unittest.TestCase):
    def setUp(self):
        self.poly_soup = []
        poly_spec = {'center': (0.0, 0.0), 'out_radius': 1.0,
                     'nsides': 4, 'start_angle': 45.0, 'randomize_angle': False}
        self.poly_soup.append(buildRandomPolygon(**poly_spec))
        poly_spec['center'] = (1.0, 1.0)
        self.poly_soup.append(buildRandomPolygon(**poly_spec))
        poly_spec['center'] = (-2.0, 2.0)
        outer = buildRandomPolygon(**poly_spec)
        poly_spec['center'] = (-2.2, 2.0)
        poly_spec['out_radius'] = 0.1
        inner1 = buildRandomPolygon(**poly_spec)
        poly_spec['center'] = (-1.8, 2.0)
        inner2 = buildRandomPolygon(**poly_spec)
        holes = shapely.geometry.MultiPolygon((inner1, inner2))
        diff = outer.difference(inner1)
        diff = diff.difference(inner2)
        self.poly_soup.append(diff)
        self.poly_soup.append(holes)

    def test_count_points(self):
        expected = 28
        actual = meshgrid.utilsshapely.countPointsInPolySoup(self.poly_soup)
        self.assertEqual(expected, actual, f"expected : {expected}, actual : {actual}")

    def test_count_polys(self):
        expected = 7
        actual = meshgrid.utilsshapely.countPolysInPolySoup(self.poly_soup)
        self.assertEqual(expected, actual, f"expected : {expected}, actual : {actual}")

    def test_statistics(self):
        stat = meshgrid.utilsshapely.getPolySoupStats(self.poly_soup)
        self.assertEqual(stat.pts, 28)
        self.assertEqual(stat.polys, 7)
        self.assertEqual(stat.polys_multi, 1)
        self.assertEqual(stat.polys_int, 2)
        self.assertEqual(stat.polys_ext, 5)
        self.assertEqual(stat.pts_ext, 20)
        self.assertEqual(stat.pts_int, 8)
        self.assertEqual(stat.max_holes, 2)
        self.assertEqual(stat.max_pts_int, 4)
        self.assertEqual(stat.max_pts_ext, 4)
        self.assertEqual(stat.max_polys_multi, 2)
