# -*- coding: utf-8 -*-
"""This module provides classes and function for viewing meshes.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.10.14 9:45:51'
__version__ = '0.10.0'


# Python packages

import descartes
import itertools
import matplotlib.pyplot as plt
import matplotlib.collections
import matplotlib.colors
import matplotlib.cm
import shapely.geometry
import numpy as np
import os
import types
import shapely

import genfemcalc
import enum
from genfemcalc import meshgrid


class ActorRole(enum.IntEnum, metaclass=genfemcalc.utils.enums.MetaIntEnum):
    """
    MetaIntEnum adds to this class two convenient methods: fromStr, convert
    """
    MESH = 0
    POINTS = 1
    BOX = 2
    CONNECTOR = 3
    NONE = 100


class MetaActor(type):
    def __new__(cls, clsname, bases, clsdict, **kwargs):
        clsobj = super().__new__(cls, clsname, bases, clsdict)
        return clsobj

    def __init__(cls, clsname, bases, clsdict, role=ActorRole.NONE):
        super().__init__(cls, bases, clsdict)
        cls.role = role


class Actor(object, metaclass=MetaActor):
    def __init__(self, ax, name='dummy'):
        self.ax = ax
        self.name = name

    def isA(self, role):
        return self.role == ActorRole.convert(role)


class ConnectorActor(object):
    """FIXME"""
    def __init__(self, start_point, end_point):
        print(f"Connector between {start_point} and {end_point}")

    @classmethod
    def betweenCells(cls, source_mesh, source_cid, target_mesh, target_cid):
        s_geom = meshgrid.geometrymrg.MeshGeometry.fromMrgMesh(source_mesh)
        t_geom = meshgrid.geometrymrg.MeshGeometry.fromMrgMesh(target_mesh)
        return cls(s_geom.getCellApproxBarycenter(source_cid), t_geom.getCellApproxBarycenter(target_cid))


class BoxActor(Actor, role=ActorRole.BOX):
    def __init__(self, ax, box, **kwargs):
        super().__init__(ax)
        xy = box.lbf[0:2]
        width, height = box.extents[0:2]
        self.patch = matplotlib.patches.Rectangle(xy, width, height, **kwargs)

    def show(self):
        self.ax.add_patch(self.patch)


class PointsActor(Actor, role=ActorRole.POINTS):
    """Wrapper that holds together points and its graphics representation.

    Attributes
    ----------
        points : :obj:`ndarray`
            Points' coordinates array
        name : string
            Name under mesh is registered
        nodes : list of Artist
            List of Artist objest (returned by plt.plot) representing nodes
        node_labels : list of Artist
            List of Artist objets (returnes by plot.annotate)
    """
    def __init__(self, ax, points, name):
        super().__init__(ax, name)
        self.points = points
        self.nodes = []
        self.node_labels = []
        return

    def __build_representation(self, **kwargs):
        """Create PolyCollection graphics object for mesh actor.
        """
        pass

    def __build_nodes(self,  **_kwargs):
        self.nodes = self.ax.plot(self.points[:, 0], self.points[:, 1], marker='o', ls='')

    def __build_node_labels(self, **_kwargs):
        numb_labels = self.points.shape[0]
        self.node_labels = [self.ax.annotate('%d' % i, self.points[i, :])
                            for i in range(numb_labels)]

    def show(self, _ax, **kwargs):
        self.showNodes(**kwargs)
        return

    def showNodes(self, **kwargs):
        """Draw node markers or update their properties.
        """
        if not self.nodes:
            self.__build_nodes(**kwargs)
        with open(os.devnull, 'w') as f:
            plt.setp(self.nodes, file=f, **kwargs)
        return

    def showNodeLabels(self, **kwargs):
        """Draw cell labels or update their properties.
        """
        if not self.node_labels:
            self.__build_node_labels(**kwargs)
        plt.setp(self.node_labels, **kwargs)
        return

    def showCellLabels(self, **kwargs):
        pass

    def deleteGraphics(self):
        for artist in self.node_labels:
            artist.remove()
        for artist in self.nodes:
            artist.remove()


class MeshActor(Actor, role=ActorRole.MESH):
    """Wrapper that holds together mesh and its graphics representation.

    Attributes
    ----------
        mesh : Mesh 
            Mesh in MRG format
        pc : matplotlib.collections.PolyCollection
            Collection of polygons representing elements
        name : string
            Name under mesh is registered
        nodes : list of Artist
            List of Artist objest (returned by plt.plot) representing nodes
        node_labels : list of Artist
            List of Artist objets (returnes by plot.annotate) 
        cell_labels : list of Artist
            List of Artist object (returned by plot.annotate)
    """
    def __init__(self, ax, mesh, name):
        super().__init__(ax, name)
        self._mesh = meshgrid.wrappers.MrgMesh(mesh)
        self.pc = None
        self.nodes = []
        self.node_labels = []
        self.cell_labels = []
        return

    @property
    def mesh(self):
        return self._mesh.mesh

    @property
    def points(self):
        return self._mesh.points[:, 0:2]

    def __build_representation(self, **kwargs):
        """Create PolyCollection graphics object for mesh actor.
        """
        if self.pc is None:
            nodes = [nodes for nodes in self._mesh.elem2nodes]
            verts = [self.points[ne, :] for ne in nodes]
            self.pc = matplotlib.collections.PolyCollection(verts, **kwargs)

    def __build_nodes(self, **_kwargs):
        self.nodes = self.ax.plot(self.points[:, 0], self.points[:, 1], marker='o', ls='')

    def __build_node_labels(self, **_kwargs):
        numb_labels = self.points.shape[0]
        self.node_labels = [self.ax.annotate('%d' % i, self.points[i, :])
                            for i in range(numb_labels)]

    def __build_cell_labels(self, **_kwargs):
        for i, nodes in enumerate(self._mesh.elem2nodes):
            center = self._mesh.points[nodes, :].mean(0)
            self.cell_labels.append(self.ax.annotate('%d' % i, center[0:2]))

    def show(self, ax, **kwargs):
        if self.pc is None:
            self.__build_representation(**kwargs)
            ax.add_collection(self.pc)
        self.pc.update(kwargs)

    def showNodeField(self, cbar, name, ax, cmap=None, norm=None, **kwargs):
        self.show(ax, **kwargs)
        c = np.zeros((len(self._mesh.elem2nodes),), np.float64)
        field_data = self._mesh.node_fields.get(name)
        if field_data is None:
            available = list(self._mesh.node_fields.keys())
            raise KeyError(f"No field named {name} in mesh. \n Available: {available}")
        for i, adj in enumerate(self._mesh.elem2nodes, 0):
            c[i] = np.mean(field_data[adj])
        cbar.setArray(c)
        cmap, norm = cbar.setup(cmap=cmap, norm=norm)
        self.pc.set_array(c)
        self.pc.set_cmap(cmap)
        self.pc.set_norm(norm)
        self.pc.update(kwargs)
        return c

    def showCellField(self, cbar, name, ax, cmap=None, norm=None, **kwargs):
        self.show(ax, **kwargs)
        field_data = self._mesh.elem_fields.get(name)
        if field_data is None:
            available = list(self._mesh.elem_fields.keys())
            raise KeyError(f"No field named {name} in mesh. \n Available: {available}")
        cbar.setArray(field_data)
        cmap, norm = cbar.setup(cmap=cmap, norm=norm)
        self.pc.set_array(field_data.reshape((-1,)))
        self.pc.set_norm(norm)
        self.pc.set_cmap(cmap)
        self.pc.update(kwargs)
        return field_data

    def showField(self, cbar, name, ax, **kwargs):
        if self.mesh.elem_fields.get(name) is not None:
            return self.showCellField(cbar, name, ax, **kwargs)
        elif self.mesh.node_fields.get(name) is not None:
            return self.showNodeField(cbar, name, ax, **kwargs)
        else:
            raise KeyError(f"No cell nor node field {name} in mesh")

    def showNodes(self, **kwargs):
        """Draw node markers or update their properties.
        """
        if not self.nodes:
            self.__build_nodes(**kwargs)
        with open(os.devnull, 'w') as f:
            plt.setp(self.nodes, file=f, **kwargs)

    def showNodeLabels(self, **kwargs):
        """Draw cell labels or update their properties.
        """
        if not self.node_labels:
            self.__build_node_labels(**kwargs)
        plt.setp(self.node_labels, **kwargs)

    def showCellLabels(self, **kwargs):
        """Draw cell labels or update their properties.
        """
        if not self.cell_labels:
            self.__build_cell_labels(**kwargs)
        plt.setp(self.cell_labels, **kwargs)

    def deleteGraphics(self):
        if self.pc is not None:
            self.pc.remove()
        for anot in self.node_labels:
            anot.remove()
        for anot in self.cell_labels:
            anot.remove()
        for anot in self.nodes:
            anot.remove()


class ColorBar(object):
    cmap = matplotlib.cm.get_cmap(name='jet', lut=40)
    norm = matplotlib.colors.Normalize()

    def __init__(self, ax, cmap=None, norm=None, array=None, visible=True):
        self.autoscale = True
        self.mappable = matplotlib.cm.ScalarMappable(cmap=ColorBar.cmap, norm=ColorBar.norm)
        self.setArray(np.linspace(0.0, 1, 11))
        self.setup(cmap=cmap, norm=norm)
        self.setArray(array)
        if visible:
            self.cbar = matplotlib.pyplot.colorbar(self.mappable, ax=ax)

    def reset(self):
        self.setup(cmap=ColorBar.cmap, norm=ColorBar.norm)

    def setLut(self, lut):
        cmap = matplotlib.cm.get_cmap(self.cmap.name, lut=lut)
        self.setup(cmap=cmap)

    def setArray(self, array):
        if array is not None:
            self.mappable.set_array(array)
            if self.autoscale:
                self.mappable.autoscale()
            self.mappable.changed()

    def setup(self, *, cmap=None, norm=None):
        need_update = False
        if cmap is not None:
            self.mappable.set_cmap(cmap)
            need_update = True
        if norm is not None:
            self.mappable.set_norm(norm)
            need_update = True
        if need_update:
            if self.autoscale:
                self.mappable.autoscale()
            self.mappable.changed()
        return self.mappable.cmap, self.mappable.norm


class MeshViewer(object):
    """Manage visualisation of MRG meshes via Matplotlib.

    MeshViewer allows to draw polygonal meshes, mark nodes, lable nodes and cells.
    MeshViewer stores reference to visualized meshes toogeter with their graphic
    representation as objest of type MeshActor. This allow
        * to postpone mesh drawin
        * to alter visual representation of a mesh after it is drawn
        * (possibly to extend viewer with some computation that needs access to mesh data)

    Meshes are registered in MeshViewer using string names.    
    """
    def __init__(self, colorbar=False, nrows=1, ncols=1):
        fig, ax = plt.subplots(nrows, ncols)
        self.fig = fig
        if nrows*ncols == 1:
            ax = np.asarray(ax)
        self.axes = ax.reshape((nrows, ncols))
        self.ax = self.axes[0, 0]
        self.cbar = ColorBar(self.ax, visible=colorbar)
        self.actors = {}

    def autoscale(self, viewport=None, aspect='auto'):
        if viewport is None:
            for i in range(self.axes.shape[0]):
                for j in range(self.axes.shape[1]):
                    self.axes[i, j].autoscale()
                    self.axes[i, j].set_aspect(aspect)
        else:
            self.axes[viewport].autoscale()
            self.axes[viewport].set_aspect(aspect)

    def resetColorBar(self):
        self.cbar.reset()

    def removeColorBar(self):
        self.cbar.cbar.remove()
        self.fig.show()

    def registerMesh(self, mesh, name='default', show=False, overwrite=False, viewport=(0, 0)):
        """Registers mesh for future viewing
        """
        if name in self.actors and not overwrite:
            raise KeyError(f'"{name}" already used for mesh registration')
        self.actors[name] = MeshActor(self.axes[viewport], mesh, name)

        if show:
            self.show(name)

    def getMesh(self, name):
        actor = self.actors.get(name)
        if actor is None:
            raise KeyError(f"Mesh: {name} not registered in the viewer")
        if actor.isA('mesh'):
            return actor.mesh
        else:
            raise ValueError(f"Actor {name} is not mesh, it is {actor.role}")

    def registerPoints(self, points, name='default', show=False, overwrite=False, viewport=(0, 0)):
        """Registers mesh for future viewing
        """
        if name in self.actors and not overwrite:
            raise KeyError(f'"{name}" already used for mesh registration')

        self.actors[name] = PointsActor(self.axes[viewport], points, name)

        if show:
            self.show(name)

    def unregister(self, name='default', keepgraphics=False):
        """Remove mesh and its graphics representation from the viewer.
        """
        actor = self.actors.get(name)
        if actor is not None:
            if not keepgraphics:
                actor.deleteGraphics()
            del self.actors[name]

    def showNodes(self, name, **kwargs):
        """Draw node markers or update their properties.
        """
        actor = self.actors[name]
        actor.showNodes(**kwargs)

    def showNodeLabels(self, name, **kwargs):
        """Draw node labels or update their properties.
        """
        actor = self.actors[name]
        actor.showNodeLabels(**kwargs)

    def showCellLabels(self, name, **kwargs):
        """Draw cell labels or update their properties.
        """
        actor = self.actors[name]
        actor.showCellLabels(**kwargs)

    showElemLabels = showCellLabels

    def show(self, name, **kwargs):
        ax = self.actors[name].ax
        self.actors[name].show(ax, **kwargs)
        ax.autoscale()
        ax.axis('equal')
        return

    def showNodeField(self, name, field_name, cmap=None, norm=None, **kwargs):
        ax = self.actors[name].ax
        self.actors[name].showNodeField(self.cbar, field_name, ax, cmap, norm, **kwargs)
        ax.autoscale()
        ax.axis('equal')

    def showCellField(self, name, field_name, cmap=None, norm=None, **kwargs):
        ax = self.actors[name].ax
        self.actors[name].showCellField(self.cbar, field_name, ax, cmap, norm, **kwargs)
        ax.autoscale()
        ax.axis('equal')
        return

    def showField(self, name, field_name, cmap=None, norm=None, **kwargs):
        ax = self.actors[name].ax
        field_data = self.actors[name].showField(self.cbar, field_name, ax, cmap=cmap,
                                                 norm=norm, **kwargs)
        self.cbar.setArray(array=field_data)
        ax.autoscale()
        ax.axis('equal')
        return

    def showContourf(self, name, field_name, *args, cmap=None, norm=None, **kwargs):
        tri_name = 'tri' + name
        tri_mesh = meshgrid.graphics.tri.TriMesh.fromMrgMesh(self.actors[name].mesh)
        self.registerMesh(tri_mesh, name=tri_name)
        field_data = tri_mesh.getParentData(field_name)
        cmap, norm = self.cbar.setup(cmap=cmap, norm=norm)
        self.cbar.setArray(field_data)
        ax = self.actors[name].ax
        ax.tricontourf(tri_mesh, tri_mesh.getParentData(field_name), *args, cmap=cmap,
                       norm=norm, **kwargs)
        return tri_name

    def showBox(self, box, viewport=(0, 0), **kwargs):
        actor = BoxActor(self.axes[viewport], box, **kwargs)
        actor.show()
        return actor

    def showPolygons(self, polygons, viewport=(0, 0), shrink=None, **kwargs):
        colors = kwargs.pop('colors', None)
        if colors is None:
            colors = itertools.repeat(kwargs.pop('facecolor', 'yellow'))
        for poly, color in zip(polygons, colors):
            if shrink is not None:
                factor = shrink-1.0
                poly = shapely.geometry.Polygon(poly.buffer(factor))
            # print('Poly Type: ', type(poly), poly.geom_type)
            # if isinstance(poly, shapely.geometry.multipolygon.MultiPolygon):
            #    for p in poly:
            #        print("SUBPOLY type: ", type(p))
            patch = descartes.patch.PolygonPatch(poly, facecolor=color, **kwargs)
            self.axes[viewport].add_patch(patch)

    def showPoints(self, points, viewport=(0, 0), **kwargs):
        self.axes[0, 0].scatter(points[:, 0], points[:, 1], c=points[:, 2], **kwargs)

    def keep(self):
        """Holds graphics window on screen until it is closed.
        """
        self.fig.show()
        return

    def savefig(self, output_filename):
        """Save graphics on file.
        """
        self.fig.savefig(output_filename + '_setp' + '.eps', format='eps')
        self.fig.savefig(output_filename + '_setp' + '.jpg', format='jpg')
        self.fig.savefig(output_filename + '_setp' + '.pdf', format='pdf')
        return
