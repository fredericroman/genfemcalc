# -*- coding: utf-8 -*-
"""Ths module produces matrices and linear systesm with requested
properties.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/7/2019'
__version__ = '0.1.0'

import scipy.sparse
import numpy

def getTridiagSystem(dim, format='dense', dtype='float64', solution=None):
    """Retrun tuple of (A,b,x) where 
        * A is system matrix
        * b is RHS
        * x is solution
    """
    main_diag = [2*dim]*dim
    upper_diag = [i for i in range(1, dim)]
    lower_diag = [i for i in range(dim-1, 0, -1)]
    diagonals = (main_diag, upper_diag, lower_diag)
    offsets = (0, 1, -1)
    lhs = scipy.sparse.diags(diagonals, offsets, format=format, dtype=dtype)
    if solution is None:
        solution = numpy.ones((dim, 1), dtype=dtype)
    rhs = lhs*solution
    return lhs, rhs, solution