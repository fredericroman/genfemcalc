# -*- coding: utf-8 -*-
"""
2D Mesh to MultiPolygon
========================

Show how to create MultiPolygon from a MRG mesh.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '10/27/2019'
__version__ = '0.1.0'

import descartes
import os
import matplotlib
import matplotlib.pyplot as plt
import rasterio.features

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoMesh2MultiPolygon(examples.Demo):
    def demonstrate(self):
        file_name = os.path.join(DATADIR, 'simple_quad_tri_mesh.vtk')
        mesh = meshgrid.iomrg.readMrgFromVtk(file_name)
        multipoly = meshgrid.utilsshapely.makeMultiPolygon(mesh)

        print(f"Number of polygons in MultiPolygon: {len(multipoly)}")

        fig, axs = plt.subplots(2, 2)
        fig.subplots_adjust(wspace=0.35)
        shrinked = multipoly.buffer(-0.1)
        for polygon in multipoly:
            patch = descartes.patch.PolygonPatch(polygon, facecolor='yellow', edgecolor='black')
            axs[0, 0].add_patch(patch)
        for polygon in shrinked:
            patch = descartes.patch.PolygonPatch(polygon, facecolor='red', edgecolor='black')
            axs[0, 0].add_patch(patch)
        axs[0, 0].autoscale()
        axs[0, 0].set_aspect('equal')

        bounds = multipoly.bounds
        xres, yres = 10, 10
        transform = rasterio.transform.from_bounds(*bounds, xres, yres)
        cmap = matplotlib.colors.ListedColormap(['white', 'yellow'])
        img = rasterio.features.rasterize((multipoly,), (xres, yres), transform=transform)
        axs[0, 1].imshow(img, cmap=cmap)

        img2 = rasterio.features.rasterize((multipoly,), (xres, yres), all_touched=True, transform=transform)
        axs[1, 1].imshow(img2, cmap=cmap)

        axs[1, 0].axis('off')
        axs[0, 1].title.set_text('all_touched=0')
        axs[1, 1].title.set_text('all_touched=1')
        

        self.showImage()


if __name__ == '__main__':
    demo = DemoMesh2MultiPolygon()
    demo.run()
