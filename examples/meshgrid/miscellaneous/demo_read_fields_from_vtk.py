# -*- coding: utf-8 -*-
"""
Mesh with fields from VTK
=========================

"""
__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.04  19:22:1'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import meshgrid
from genfemcalc import examples

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoReadFieldsFromVTK(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        file_name = os.path.join(DATADIR, 'mesh_with_fields.vtk')
        mesh = meshgrid.iomrg.readMrgFromVtk(file_name)
        for name, data in mesh.node_fields.items():
            field_shape = data.shape
            print(f'Nodal field {name} with shape {field_shape}')
        for name, data in mesh.elem_fields.items():
            field_shape = data.shape
            print(f'Cell field {name} with shape {field_shape}')


if __name__ == '__main__':
    demo = DemoReadFieldsFromVTK()
    demo.run()
