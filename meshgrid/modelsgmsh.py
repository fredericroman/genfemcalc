# -*- coding: utf-8 -*-
"""The purpose of this module is to provide functions to generate some
simple mesh models using PyGmsh as wrapper for GMSH mesh generator.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019-09-01'
__version__ = '0.9.0'


# Python packages
import pygmsh
import re
import sys
import textwrap


class ModelGenerator(object):
    """Factory class for geometric models
    """
    def Generate(self, model_name, context):
        """Dispatch method"""
        cls = getattr(sys.modules[__name__], model_name)
        generator = cls()
        return generator.discretize(context)

    def WriteScript(self, model_name, filename, context):
        cls = getattr(sys.modules[__name__], model_name)
        generator = cls()
        generator.writeScript(filename, context)
        return None


class Domain(object):
    def __init__(self, name, mesh, regions):
        self._name = name
        self._mesh = mesh
        self._regions = regions
        return

    @property
    def name(self):
        return self._name

    @property
    def mesh(self):
        return self._mesh

    @property
    def regions(self):
        return self._regions


class MetaModelFactory(type):
    _template = ''
    _default_context = {}

    @property
    def template(cls):
        return cls._template

    @property
    def default_context(cls):
        print('dupa')
        return cls._default_context


class ModelFactoryBase(object):
    _template = ''
    _default_context = {}
    verbose_gmsh = False

    @property
    def template(self):
        return self.__class__._template

    @property
    def default_context(self):
        print('bingo')
        return self.__class__.default_context

    def writeScript(self, filename, context):
        """Saves scritpt generatef from template and context
        to the specified 'geo' file.
        """
        with open(filename, 'w') as the_file:
            the_file.write(self.getScript(context))

    def getScriptTemplate(self):
        preamble = r"""
        False = 0;
        True = 1;
        element_order = <element_order>;
        incomplete = <incomplete>;
        """
        
        settings = r"""
        Mesh.ElementOrder = <element_order>;
        Mesh.SecondOrderIncomplete = <incomplete>;
        """

        pre = textwrap.dedent(preamble)
        tpl = textwrap.dedent(self._template)
        suf = textwrap.dedent(settings)
        return pre + tpl + suf

    def getScript(self, context=None):
        if context is None:
            context = {}
        mycontext = self.filterContext(context)

        script = self.getScriptTemplate()

        for param in mycontext.keys():
            val = mycontext[param]
            try:
                iter(val)
            except TypeError:
                # not iterable
                value = str(val)
            else:
                # iterable
                value = ','.join(map(str, val))
            regexp = "<%s>" % param
            script = re.sub(regexp, value, script)

        return script

    def getDefaultContext(self):
        meshing_context = {'element_order': 1,
                           'incomplete': False}
        mycontext = self._default_context.copy()
        mycontext.update(meshing_context)
        return mycontext
        
    def filterContext(self, context):
        mycontext = self.getDefaultContext()
        mycontext.update(context)
        tsdensity = list(mycontext.get('tsdensity', [5]))
        for i in range(3-len(tsdensity)):
            tsdensity.append(tsdensity[-1])
        mycontext['tsdensity'] = tsdensity
        return mycontext

    def discretize(self, context=None, **kwargs):
        """Discretize(triangulate) the model model. Model is defined by template script
        and parameters hold in context dictionary.
        """
        tmpcontext = {}
        if context is not None:
            tmpcontext = context.copy()
        tmpcontext.update(kwargs)

        mycontext = self.filterContext(tmpcontext)

        geom = pygmsh.built_in.Geometry()
        geom.add_raw_code(self.getScript(mycontext))

        name = type(self).__name__
        wanted_keys = ['verbose']  # The keys you want
        gmsh_args = dict((k, kwargs[k]) for k in wanted_keys if k in kwargs)
        if gmsh_args.get('verbose') is None:
            gmsh_args['verbose'] = self.verbose_gmsh
        mesh = pygmsh.generate_mesh(geom, **gmsh_args)

        domain = Domain(name, mesh, self.regions)

        return domain


class Region(object):
    @property
    def name(self):
        return self._name

    @property
    def dim(self):
        return self._dim

    @property
    def tag(self):
        return self._tag

    def __init__(self, name, dim, tag):
        self._name = name
        self._dim = dim
        self._tag = tag
        return

    def __repr__(self):
        return '<Region(name="%s", dim=%d, tag=%d)>'%(self.name, self.dim, self.tag)

    def __str__(self):
        return 'Region %s: dim=%d, tag=%d'%(self.name, self.dim, self.tag)


class Segment(ModelFactoryBase, metaclass=MetaModelFactory):
    _template = """
    xa = <xa>;
    xb = <xb>;
    ya = <ya>;
    yb = <yb>;
    lc = <edge_size>;
    tsdensity[] = {<tsdensity>};
    transfinite = <transfinite>;

    quadsonly = <quadsonly>;
    If (quadsonly != 0)
    Mesh.RecombinationAlgorithm = 1;
    Mesh.RecombineAll = 1;
    EndIf

    Point(1) = {xa,   ya,    0, lc};
    Point(2) = {xb,   yb,    0, lc};
    Line(1) = {1, 2};

    If (transfinite != 0)
      Transfinite Line {1} = tsdensity[0];
    EndIf

    Physical Point("p_start") = {1};
    Physical Point("p_end") = {2};
    Physical Line("domain") = {1};
    """

    _default_context = {'xa': 0.0,
                        'xb': 3.0,
                        'ya': 0.0,
                        'yb': 4.0,
                        'transfinite': True,
                        'tsdensity': 2,
                        'quadsonly' : False,
                        'edge_size' : 1.0}

    regions = [Region(name='p_start', dim=0, tag=1),
               Region(name='p_end', dim=0, tag=2),
               Region(name='domain', dim=1, tag=3)]


class Rectangle(ModelFactoryBase, metaclass=MetaModelFactory):
    _template = """
    xa = <xa>;
    xb = <xb>;
    ya = <ya>;
    yb = <yb>;
    lc = <edge_size>;
    tsdensity[] = {<tsdensity>};
    transfinite = <transfinite>;

    quadsonly = <quadsonly>;
    If (quadsonly != 0)
    Mesh.RecombinationAlgorithm = 1;
    Mesh.RecombineAll = 1;
    EndIf

    Point(1) = {xa,   ya,    0, lc};
    Point(2) = {xb,   ya,    0, lc};
    Point(3) = {xb,   yb,    0, lc};
    Point(4) = {xa,   yb,    0, lc};
    Line(1) = {1, 2};
    Line(2) = {2, 3};
    Line(3) = {3, 4};
    Line(4) = {4, 1};
    Line Loop(1) = {1, 2, 3, 4};
    Plane Surface(1) = {1};
    If (transfinite != 0)
      Transfinite Line {1,3} = tsdensity[0];
      Transfinite Line {2,4} = tsdensity[1];
      Transfinite Surface "*";
    EndIf

    Physical Line("b_south") = {1};
    Physical Line("b_east") = {2};
    Physical Line("b_north") = {3};
    Physical Line("b_west") = {4};
    Physical Surface("domain") = {1};
    """

    _default_context = {'xa': 0.0,
                        'xb': 2.0,
                        'ya': 0.0,
                        'yb': 1.0,
                        'transfinite': False,
                        'tsdensity': [4, 4],
                        'quadsonly': False,
                        'edge_size': 0.3}

    regions = [Region(name='b_south', dim=1, tag=1),
               Region(name='b_east', dim=1, tag=2),
               Region(name='b_north', dim=1, tag=3),
               Region(name='b_west', dim=1, tag=4),
               Region(name='domain', dim=2, tag=5)]


class SandwichRectangle(ModelFactoryBase, metaclass=MetaModelFactory):
    _template = """
    a = <a>/2;
    b = <b>/2;
    lc = <edge_size>;
    tsdensity[] = {<tsdensity>};
    transfinite = <transfinite>;

    quadsonly = <quadsonly>;
    If (quadsonly != 0)
    Mesh.RecombinationAlgorithm = 1;
    Mesh.RecombineAll = 1;
    EndIf

    Point(1) = {-a,   -b,    0, lc};
    Point(2) = {a,    -b,    0, lc};
    Point(3) = {a,    b,     0, lc};
    Point(4) = {-a,   b,     0, lc};
    Point(5) = {-a,   0,     0, lc};
    Point(6) = {a,    0,     0, lc};
    Line(1) = {1, 2};
    Line(2) = {2, 6};
    Line(3) = {6, 3};
    Line(4) = {3, 4};
    Line(5) = {4, 5};
    Line(6) = {5, 1};
    Line(7) = {5, 6};
    Line Loop(1) = {1, 2, -7, 6};
    Line Loop(2) = {7, 3,  4, 5};
    Plane Surface(1) = {1};
    Plane Surface(2) = {2};

    If (transfinite != 0)
      Transfinite Line {1,4,7} = tsdensity[0];
      Transfinite Line {2,3,5,6} = tsdensity[1];
      Transfinite Surface "*";
    EndIf

    Physical Line("b_south") = {1};
    Physical Line("b_east_lower") = {2};
    Physical Line("b_east_upper") = {3};
    Physical Line("b_north") = {4};
    Physical Line("b_west_upper") = {5};
    Physical Line("b_west_lower") = {6};
    Physical Line("i_split") = {7};
    Physical Surface("domain_lower") = {1};
    Physical Surface("domain_upper") = {2};
    """

    _default_context = {'a' : 2.0,
                       'b' : 1.0,
                       'quadsonly' : False,
                       'transfinite' : False,
                       'tsdensity' : [4],
                       'edge_size' : 0.3}

    regions = [Region(name='b_south', dim=1, tag=1),
               Region(name='b_east_lower', dim=1, tag=2),
               Region(name='b_east_upper', dim=1, tag=3),
               Region(name='b_north', dim=1, tag=4),
               Region(name='b_west_upper', dim=1, tag=5),
               Region(name='b_west_lower', dim=1, tag=6),
               Region(name='i_split', dim=1, tag=7),
               Region(name='domain_lower', dim=2, tag=8),
               Region(name='domain_upper', dim=2, tag=9)]


class RectangleCircHole(ModelFactoryBase, metaclass=MetaModelFactory):
    """Factory for model of rectangle with circular hole"""

    _template = """
    lc = <edge_size>;
    a = <a>/2;
    b = <b>/2;
    r = <r>;
    xh = <xh>;
    yh = <yh>;
    lcFactors[] = {<lcFactors>};

    quadsonly = <quadsonly>;
    If (quadsonly != 0)
    Mesh.RecombinationAlgorithm = 1;
    Mesh.RecombineAll = 1;
    EndIf

    Point(1)  = {-a, -b, 0, lcFactors(0)*lc};
    Point(2)  = { a, -b, 0, lcFactors(1)*lc};
    Point(3)  = { a,  b, 0, lcFactors(2)*lc};
    Point(4)  = {-a,  b, 0, lcFactors(3)*lc};
    Point(5)  = {xh, yh-r, 0, lcFactors(4)*lc};
    Point(6)  = {xh+r, yh, 0, lcFactors(5)*lc};
    Point(7)  = {xh, yh+r, 0, lcFactors(6)*lc};
    Point(8)  = {xh-r, yh, 0, lcFactors(7)*lc};
    Point(9)  = {xh, yh, 0, lc};
    Line(1) = {1,2};
    Line(2) = {2,3};
    Line(3) = {3,4};
    Line(4) = {4,1};
    Circle(5) = {5,9,6};
    Circle(6) = {6,9,7};
    Circle(7) = {7,9,8};
    Circle(8) = {8,9,5};
    Line Loop(1) = {1,2,3,4};
    Line Loop(2) = {5,6,7,8};
    Plane Surface(1) = {1,2};
    Physical Line("b_south") = {1};
    Physical Line("b_east") = {2};
    Physical Line("b_north") = {3};
    Physical Line("b_west") = {4};
    Physical Line("b_hole") = {5,6,7,8};
    Physical Surface("d_domain") = {1};
    """

    _default_context = {'a' : 2.0,
                       'b' : 1.0,
                       'xh' : 0.0,
                       'yh' : 0.0,
                       'r'  : 0.225,
                       'quadsonly' : False,
                       'edge_size' : 0.3,
                       'lcFactors': [0.1,1,1,1,1,0.1,1,1]}

    regions = [Region(name='b_south', dim=1, tag=1),
               Region(name='b_east', dim=1, tag=2),
               Region(name='b_north', dim=1, tag=3),
               Region(name='b_west', dim=1, tag=4),
               Region(name='b_hole', dim=1, tag=5),
               Region(name='domain', dim=2, tag=6)]


class RectangleHole(ModelFactoryBase, metaclass=MetaModelFactory):
    """Factory for rectangle with rectangular hole"""

    _template = """
    a = <a>/2;
    b = <b>/2;
    ah = <ah>/2;
    bh = <bh>/2;
    xh = <xh>;
    yh = <yh>;
    lc = <edge_size>;
    lcFactors[] = {<lcFactors>};

    quadsonly = <quadsonly>;
    If (quadsonly != 0)
    Mesh.RecombinationAlgorithm = 1;
    Mesh.RecombineAll = 1;
    EndIf

    Point(1)  = {-a, -b, 0, lcFactors(0)*lc};
    Point(2)  = { a, -b, 0, lcFactors(1)*lc};
    Point(3)  = { a,  b, 0, lcFactors(2)*lc};
    Point(4)  = {-a,  b, 0, lcFactors(3)*lc};
    Point(5)  = {xh-ah, yh-bh, 0, lcFactors(4)*lc};
    Point(6)  = {xh+ah, yh-bh, 0, lcFactors(5)*lc};
    Point(7)  = {xh+ah, yh+bh, 0, lcFactors(6)*lc};
    Point(8)  = {xh-ah, yh+bh, 0, lcFactors(7)*lc};
    Line(1) = {1, 2};
    Line(2) = {2, 3};
    Line(3) = {3, 4};
    Line(4) = {4, 1};
    Line(5) = {5, 8};
    Line(6) = {8, 7};
    Line(7) = {7, 6};
    Line(8) = {6, 5};
    Line Loop(1) = {1, 2, 3, 4};
    Line Loop(2) = {5,6,7,8};
    Plane Surface(1) = {1,2};
    Physical Line("b_south") = {1};
    Physical Line("b_east") = {2};
    Physical Line("b_north") = {3};
    Physical Line("b_west") = {4};
    Physical Line("b_hole") = {5,6,7,8};
    Physical Surface("domain") = {1};
    """

    _default_context = {'a' : 2.0,
                       'b' : 1.0,
                       'ah': 2.0/3, # hole x dimension
                       'bh': 1.0/3, # hole y dimension
                       'xh': 0.0, # xcenter of hole
                       'yh': 0.0, # y center of hole
                       'quadsonly' : False,
                       'lcFactors': [1,1,1,1,1,1,1,1],
                       'edge_size' : 0.3}

    regions = [Region(name='b_south', dim=1, tag=1),
               Region(name='b_east', dim=1, tag=2),
               Region(name='b_north', dim=1, tag=3),
               Region(name='b_west', dim=1, tag=4),
               Region(name='b_hole', dim=1, tag=5),
               Region(name='domain', dim=2, tag=6)]


class HalfAnnulus(ModelFactoryBase, metaclass=MetaModelFactory):
    """Factory for half annulus"""
    _template = """
    r=<r>;
    R=<R>;
    lc = <edge_size>;
    lcFactors[] = {<lcFactors>};

    tsdensity_radial = <tsdensity_radial>;
    tsdensity_circum = <tsdensity_circum>;
    transfinite = <transfinite>;

    quadsonly = <quadsonly>;
    If (quadsonly != 0)
      Mesh.RecombinationAlgorithm = 1;
      Mesh.RecombineAll = 1;
    Else
      Mesh.RecombineAll = 0;
    EndIf

    Point(1) = {r,0,0, lcFactors(0)*lc};
    Point(2) = {R,0,0, lcFactors(1)*lc};
    Point(3) = {0,R,0, lcFactors(2)*lc};
    Point(4) = {-R,0,0, lcFactors(3)*lc};
    Point(5) = {-r,0,0, lcFactors(4)*lc};
    Point(6) = {0,r,0, lcFactors(15*lc};
    Point(7) = {0,0,0, lc};
    Line(1) = {1,2};
    Circle(2) = {2,7,3};
    Circle(3) = {3,7,4};
    Line(4) = {4,5};
    Circle(5) = {5,7,6};
    Circle(6) = {6,7,1};
    Line(7) = {6,3};
    Line Loop(1) = {1,2,-7,6};
    Line Loop(2) = {7,3,4,5};
    Plane Surface(1) = {1};
    Plane Surface(2) = {2};

    If (transfinite != 0)
      Transfinite Line {1,4,7} = tsdensity_radial;
      Transfinite Line {2,3,5,6} = tsdensity_circum;
      Transfinite Surface "*";
      Recombine Surface "*";
    EndIf

    Physical Line("b_right") = {1};
    Physical Line("b_left") = {4};
    Physical Line("b_inner") = {5,6};
    Physical Line("b_outer") = {2,3};
    Physical Surface("domain") = {1,2};
    """

    _default_context = {'R': 2.0,
                        'r': 1.0,
                        'transfinite': False,
                        'tsdensity_radial': 5,
                        'tsdensity_circum': 10,
                        'quadsonly': False,
                        'lcFactors': [1, 1, 1, 1, 1, 1],
                        'edge_size': 0.3}

    regions = [Region(name='b_right', dim=1, tag=1),
               Region(name='b_left', dim=1, tag=2),
               Region(name='b_inner', dim=1, tag=3),
               Region(name='b_outer', dim=1, tag=4),
               Region(name='domain', dim=2, tag=5)]


class Cuboid(ModelFactoryBase, metaclass=MetaModelFactory):
    """Factory for Cuboid"""

    _template = """
    a = <a>/2;
    b = <b>/2;
    c = <c>/2;
    lc = <edge_size>;
    tsdensity[] = {<tsdensity>};
    Point(1) = {-a, -b, -c, lc};
    Point(2) = {a, -b, -c, lc};
    Point(3) = {-a, b, -c, lc};
    Point(4) = {a, b, -c, lc};
    Point(5) = {a, b, c, lc};
    Point(6) = {a, -b, c, lc};
    Point(7) = {-a, b, c, lc};
    Point(8) = {-a, -b, c, lc};
    /* Cube edges */
    Line(1) = {3, 7};
    Line(2) = {7, 5};
    Line(3) = {5, 4};
    Line(4) = {4, 3};
    Line(5) = {3, 1};
    Line(6) = {2, 4};
    Line(7) = {2, 6};
    Line(8) = {6, 8};
    Line(9) = {8, 1};
    Line(10) = {1, 2};
    Line(11) = {8, 7};
    Line(12) = {6, 5};
    /* Loops for cube faces */
    Line Loop(13) = {7, 8, 9, 10};
    Plane Surface(14) = {13};
    Line Loop(15) = {6, 4, 5, 10};
    Plane Surface(16) = {-15};
    Line Loop(17) = {3, 4, 1, 2};
    Plane Surface(18) = {17};
    Line Loop(19) = {12, -2, -11, -8};
    Plane Surface(20) = {19};
    Line Loop(21) = {7, 12, 3, -6};
    Plane Surface(22) = {-21};
    Line Loop(23) = {9, -5, 1, -11};
    Plane Surface(24) = {-23};
    Surface Loop(25) = {14, 22, 20, 18, 16, 24};

    Volume(26) = {25};

    transfinite = <transfinite>;
    If (transfinite != 0)
    Transfinite Curve {2,4,8,10} = tsdensity[0] Using Bump <tsbump>;
    Transfinite Curve {5,6,11,12} = tsdensity[1] Using Bump <tsbump>;
    Transfinite Curve {9,7,3,1} = tsdensity[2] Using Bump <tsbump>;
    Transfinite Surface "*";
    Recombine Surface "*";
    Transfinite Volume "*";
    EndIf

    Physical Surface("b_top") = {20};
    Physical Surface("b_bottom") = {16};
    Physical Surface("b_front") = {22};
    Physical Surface("b_back") = {24};
    Physical Surface("b_left") = {18};
    Physical Surface("b_right") = {14};
    Physical Volume("domain") = {26};
    """

    _default_context = {'a' : 2.0,
                       'b' : 2.0,
                       'c' : 2.0,
                       'transfinite' : True,
                       'tsdensity' : [10],
                       'tsbump'    : 1.0,
                       'edge_size' : 0.3}

    regions = [Region(name='b_top', dim=2, tag=1),
               Region(name='b_bottom', dim=2, tag=2),
               Region(name='b_front', dim=2, tag=3),
               Region(name='b_back', dim=2, tag=4),
               Region(name='b_left', dim=2, tag=5),
               Region(name='b_right', dim=2, tag=6),
               Region(name='domain', dim=3, tag=7)]


class Cube(Cuboid, metaclass=MetaModelFactory):
    def filterContext(self, context):
        mycontext = super().filterContext(context)
        mycontext['b'] = mycontext['a']
        mycontext['c'] = mycontext['a']
        return mycontext


class CuboidSphericalVoid(ModelFactoryBase, metaclass=MetaModelFactory):
    """Factory for Cuboid with Spherical void"""

    _template = """
    a = <a>/2;
    b = <b>/2;
    c = <c>/2;
    r = <r>;
    lc = <edge_size>;
    Point(1) = {-a, -b, -c, lc};
    Point(2) = {a, -b, -c, lc};
    Point(3) = {-a, b, -c, lc};
    Point(4) = {a, b, -c, lc};
    Point(5) = {a, b, c, lc};
    Point(6) = {a, -b, c, lc};
    Point(7) = {-a, b, c, lc};
    Point(8) = {-a, -b, c, lc};
    /* void centre */
    Point(9) = {0, 0, 0, lc};
    Point(10) = {r,0, 0, lc};
    Point(11) = {0,0, -r, lc};
    Point(12) = {0,0, r, lc};
    /* Cube edges */
    Line(1) = {3, 7};
    Line(2) = {7, 5};
    Line(3) = {5, 4};
    Line(4) = {4, 3};
    Line(5) = {3, 1};
    Line(6) = {2, 4};
    Line(7) = {2, 6};
    Line(8) = {6, 8};
    Line(9) = {8, 1};
    Line(10) = {1, 2};
    Line(11) = {8, 7};
    Line(12) = {6, 5};
    /* Void edges */
    Circle(13) = {12,9,10};
    Circle(14) = {10,9,11};
    /* Loops for cube faces */
    Line Loop(13) = {7, 8, 9, 10};
    Plane Surface(14) = {13};
    Line Loop(15) = {6, 4, 5, 10};
    Plane Surface(16) = {-15};
    Line Loop(17) = {3, 4, 1, 2};
    Plane Surface(18) = {17};
    Line Loop(19) = {12, -2, -11, -8};
    Plane Surface(20) = {19};
    Line Loop(21) = {7, 12, 3, -6};
    Plane Surface(22) = {-21};
    Line Loop(23) = {9, -5, 1, -11};
    Plane Surface(24) = {-23};
    Surface Loop(25) = {14, 22, 20, 18, 16, 24};

    quarter1[] = Extrude{{0,0,1},{0,0,1},Pi/2}{Curve{13, 14};};
    quarter2[] = Extrude{{0,0,1},{0,0,1},Pi/2}{Curve{quarter1[0],quarter1[3]};};
    quarter3[] = Extrude{{0,0,1},{0,0,1},Pi/2}{Curve{quarter2[0],quarter2[3]};};
    quarter4[] = Extrude{{0,0,1},{0,0,1},Pi/2}{Curve{quarter3[0],quarter3[3]};};

    Surface Loop(26) = {quarter1[1], quarter1[4],
                        quarter2[1], quarter2[4],
                        quarter3[1], quarter3[4],
                        quarter4[1], quarter4[4]};
    Volume(27) = {25, 26};

    Physical Surface("b_top") = {20};
    Physical Surface("b_bottom") = {16};
    Physical Surface("b_front") = {22};
    Physical Surface("b_back") = {24};
    Physical Surface("b_left") = {18};
    Physical Surface("b_right") = {14};
    Physical Surface("b_hole") = {quarter1[1], quarter1[4],
                                  quarter2[1], quarter2[4],
                                  quarter3[1], quarter3[4],
                                  quarter4[1], quarter4[4]};

    Physical Volume("domain") = {27};
    """

    _default_context = {'a' : 2.0,
                       'b' : 2.0,
                       'c' : 4.0,
                       'r' : 0.5,
                       'edge_size' : 0.3}

    regions = [Region(name='b_top', dim=2, tag=1),
               Region(name='b_bottom', dim=2, tag=2),
               Region(name='b_front', dim=2, tag=3),
               Region(name='b_back', dim=2, tag=4),
               Region(name='b_left', dim=2, tag=5),
               Region(name='b_right', dim=2, tag=6),
               Region(name='b_hole', dim=2, tag=7),
               Region(name='domain', dim=3, tag=8)]


class CubeSphericalVoid(CuboidSphericalVoid, metaclass=MetaModelFactory):
    def filterContext(self, context):
        mycontext = super().filterContext(context)
        mycontext['b'] = mycontext['a']
        mycontext['c'] = mycontext['a']
        return mycontext


class CuboidSphericalBump(ModelFactoryBase, metaclass=MetaModelFactory):
    """Factory for Cuboid with spherical bump void"""

    _template = """
    a = <a>/2;
    b = <b>/2;
    c = <c>/2;
    r = <r>;
    lc = <edge_size>;
    Point(1) = {-a, -b, -c, lc};
    Point(2) = {a, -b, -c, lc};
    Point(3) = {-a, b, -c, lc};
    Point(4) = {a, b, -c, lc};
    Point(5) = {a, b, c, lc};
    Point(6) = {a, -b, c, lc};
    Point(7) = {-a, b, c, lc};
    Point(8) = {-a, -b, c, lc};
    /* void centre */
    Point(9) = {0, 0, -c, lc};
    Point(10) = {r,0, -c, lc};
    Point(11) = {0,0, -c+r, lc};
    /* Cube edges */
    Line(1) = {3, 7};
    Line(2) = {7, 5};
    Line(3) = {5, 4};
    Line(4) = {4, 3};
    Line(5) = {3, 1};
    Line(6) = {2, 4};
    Line(7) = {2, 6};
    Line(8) = {6, 8};
    Line(9) = {8, 1};
    Line(10) = {1, 2};
    Line(11) = {8, 7};
    Line(12) = {6, 5};
    /* Bumb edges */
    Circle(13) = {10,9,11};

    /* Loops for cube faces */
    Line Loop(12) = {7, 8, 9, 10};
    Plane Surface(12) = {12};
    Line Loop(15) = {6, 4, 5, 10};
    Line Loop(17) = {3, 4, 1, 2};
    Plane Surface(18) = {17};
    Line Loop(19) = {12, -2, -11, -8};
    Plane Surface(20) = {19};
    Line Loop(21) = {7, 12, 3, -6};
    Plane Surface(22) = {-21};
    Line Loop(23) = {9, -5, 1, -11};
    Plane Surface(24) = {-23};

    /* Extruding spherical bump */
    quarter1[] = Extrude{{0,0,1},{0,0,1},Pi/2}{Curve{13};};
    quarter2[] = Extrude{{0,0,1},{0,0,1},Pi/2}{Curve{quarter1[0]};};
    quarter3[] = Extrude{{0,0,1},{0,0,1},Pi/2}{Curve{quarter2[0]};};
    quarter4[] = Extrude{{0,0,1},{0,0,1},Pi/2}{Curve{quarter3[0]};};

    Line Loop(46) = {quarter1[2], quarter2[2], quarter3[2], quarter4[2]};
    Plane Surface(46) = {-15, 46};

    Surface Loop(25) = {12, 22, 20, 18, 46, 24,
                        quarter1[1],
                        quarter2[1],
                        quarter3[1],
                        quarter4[1]};
    Volume(27) = {25};

    Physical Surface("b_top") = {20};
    Physical Surface("b_bottom") = {46};
    Physical Surface("b_front") = {22};
    Physical Surface("b_back") = {24};
    Physical Surface("b_left") = {18};
    Physical Surface("b_right") = {12};
    Physical Surface("b_bump") = {quarter1[1],
                                  quarter2[1],
                                  quarter3[1],
                                  quarter4[1]};

    Physical Volume("domain") = {27};
    """

    _default_context = {'a' : 4.0,
                       'b' : 2.0,
                       'c' : 2.0,
                       'r' : 0.5,
                       'edge_size' : 0.3}

    regions = [Region(name='b_top', dim=2, tag=1),
               Region(name='b_bottom', dim=2, tag=2),
               Region(name='b_front', dim=2, tag=3),
               Region(name='b_back', dim=2, tag=4),
               Region(name='b_left', dim=2, tag=5),
               Region(name='b_right', dim=2, tag=6),
               Region(name='b_bump', dim=2, tag=7),
               Region(name='domain', dim=3, tag=8)]


class Lshape(ModelFactoryBase, metaclass=MetaModelFactory):
    """Meta model for LShape.
    """
    _template = """
    a = <a>/2;
    b = <b>/2;
    ah = <ah>;
    bh = <bh>;
    lc = <edge_size>;
    lcFactors[] = {<lcFactors>};

    quadsonly = <quadsonly>;
    If (quadsonly != 0)
    Mesh.RecombinationAlgorithm = 1;
    Mesh.RecombineAll = 1;
    EndIf

    Point(1)  = {-a, -b, 0, lcFactors(0)*lc};
    Point(2)  = { a, -b, 0, lcFactors(1)*lc};
    Point(3)  = { a,  b-bh, 0, lcFactors(2)*lc};
    Point(4)  = { a-ah, b-bh, 0, lcFactors(3)*lc};
    Point(5)  = {a-ah, b, 0, lcFactors(4)*lc};
    Point(6)  = {-a, b, 0, lcFactors(5)*lc};
    Line(1) = {1, 2};
    Line(2) = {2, 3};
    Line(3) = {3, 4};
    Line(4) = {4, 5};
    Line(5) = {5, 6};
    Line(6) = {6, 1};
    Line Loop(1) = {1, 2, 3, 4, 5, 6};
    Plane Surface(1) = {1};
    Physical Line("b_south") = {1};
    Physical Line("b_east") = {2};
    Physical Line("b_north_h") = {3};
    Physical Line("b_east_h") = {4};
    Physical Line("b_north") = {5};
    Physical Line("b_west") = {6};
    Physical Surface("domain") = {1};
    """

    _default_context = {'a' : 2.0,
                       'b' : 1.0,
                       'ah': 2.0/2, # hole x dimension
                       'bh': 1.0/2, # hole y dimension
                       'quadsonly' : False,
                       'lcFactors': [1,1,1,1,1,1],
                       'edge_size' : 0.3}

    regions = [Region(name='b_south', dim=1, tag=1),
               Region(name='b_east', dim=1, tag=2),
               Region(name='b_north_h', dim=1, tag=3),
               Region(name='b_east_h', dim=1, tag=4),
               Region(name='b_north', dim=1, tag=5),
               Region(name='b_west', dim=1, tag=6),
               Region(name='domain', dim=2, tag=7)]


class CarCompartment(ModelFactoryBase, metaclass=MetaModelFactory):
    """Meta model for car compartment.
    """
    _template = """
    lc = <edge_size>;
    lcFactors[] = {<lcFactors>};
    quadsonly = <quadsonly>;

    If (quadsonly != 0)
    Mesh.RecombinationAlgorithm = 1;
    Mesh.RecombineAll = 1;
    EndIf
    Point(1) = {0.392855, 0.172856, 0.000000, lcFactors(0)*lc};
    Point(2) = {1.131422, 0.157142, 0.000000, lcFactors(1)*lc};
    Point(3) = {1.414278, 0.109999, 0.000000, lcFactors(2)*lc};
    Point(4) = {1.759990, 0.109999, 0.000000, lcFactors(3)*lc};
    Point(5) = {1.807133, 0.361427, 0.000000, lcFactors(4)*lc};
    Point(6) = {2.184274, 0.298570, 0.000000, lcFactors(5)*lc};
    Point(7) = {2.451415, 0.785710, 0.000000, lcFactors(6)*lc};
    Point(8) = {2.514272, 0.769996, 0.000000, lcFactors(7)*lc};
    Point(9) = {2.294273, 0.282856, 0.000000, lcFactors(8)*lc};
    Point(10) = {2.467129, 0.345712, 0.000000, lcFactors(9)*lc};
    Point(11) = {2.749985, 0.314284, 0.000000, lcFactors(10)*lc};
    Point(12) = {2.985698, 0.298570, 0.000000, lcFactors(11)*lc};
    Point(13) = {3.001412, 0.518569, 0.000000, lcFactors(12)*lc};
    Point(14) = {3.064269, 0.628568, 0.000000, lcFactors(13)*lc};
    Point(15) = {3.001412, 0.769996, 0.000000, lcFactors(14)*lc};
    Point(16) = {2.687128, 1.052851, 0.000000, lcFactors(15)*lc};
    Point(17) = {2.357130, 1.147137, 0.000000, lcFactors(16)*lc};
    Point(18) = {1.948561, 1.162851, 0.000000, lcFactors(33)*lc};
    Point(19) = {1.131422, 1.194279, 0.000000, lcFactors(17)*lc};
    Point(20) = {0.439998, 0.879995, 0.000000, lcFactors(18)*lc};
    Point(21) = {0.691425, 0.832853, 0.000000, lcFactors(19)*lc};
    Point(22) = {0.754282, 0.597140, 0.000000, lcFactors(20)*lc};
    Point(23) = {0.565711, 0.487140, 0.000000, lcFactors(32)*lc};
    Point(24) = {0.204285, 0.392855, 0.000000, lcFactors(21)*lc};
    Point(25) = {0.989995, 0.298570, 0.000000, lcFactors(22)*lc};
    Point(26) = {1.209993, 0.282856, 0.000000, lcFactors(23)*lc};
    Point(27) = {1.414278, 0.219999, 0.000000, lcFactors(24)*lc};
    Point(28) = {1.524277, 0.219999, 0.000000, lcFactors(25)*lc};
    Point(29) = {1.807133, 0.801424, 0.000000, lcFactors(26)*lc};
    Point(30) = {1.807133, 0.974280, 0.000000, lcFactors(27)*lc};
    Point(31) = {1.744276, 0.974280, 0.000000, lcFactors(28)*lc};
    Point(32) = {1.665705, 0.675711, 0.000000, lcFactors(29)*lc};
    Point(33) = {1.477135, 0.329998, 0.000000, lcFactors(30)*lc};
    Point(34) = {1.021423, 0.377141, 0.000000, lcFactors(31)*lc};
    Line(1) = {1, 2};
    Line(2) = {2, 3};
    Line(3) = {3, 4};
    Line(4) = {4, 5};
    Line(5) = {5, 6};
    Line(6) = {6, 7};
    Line(7) = {7, 8};
    Line(8) = {8, 9};
    Line(9) = {9, 10};
    Line(10) = {10, 11};
    Line(11) = {11, 12};
    Line(12) = {12, 13};
    Line(13) = {13, 14};
    Line(14) = {14, 15};
    Line(15) = {15, 16};
    Line(16) = {16, 17};
    Line(17) = {17, 18};
    Line(18) = {18, 19};
    Line(19) = {19, 20};
    Line(20) = {20, 21};
    Line(21) = {21, 22};
    Line(22) = {22, 23};
    Line(23) = {23, 24};
    Line(24) = {24, 1};
    Line(25) = {25, 34};
    Line(26) = {34, 33};
    Line(27) = {33, 32};
    Line(28) = {32, 31};
    Line(29) = {31, 30};
    Line(30) = {30, 29};
    Line(31) = {29, 28};
    Line(32) = {28, 27};
    Line(33) = {27, 26};
    Line(34) = {26, 25};
    Line Loop(1) = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24};
    Line Loop(2) = {25,26,27,28,29,30,31,32,33,34};
    Plane Surface(1) = {1,2};
    Physical Line("b_engine") = {24};
    Physical Line("b_roof") = {16,17,18};
    Physical Line("b_outer_other") = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,19,20,21,22,23};
    Physical Line("b_inner") = {25,26,27,28,29,30,31,32,33,34};
    Physical Surface("domain") = {1};
    """
    _default_context = {'quadsonly': False,
                        'lcFactors': [1.0]*34,
                        'edge_size': 0.3}
    regions = [Region(name='b_outer', dim=1, tag=1),
               Region(name='b_inner', dim=1, tag=2),
               Region(name='domain', dim=2, tag=3)]
