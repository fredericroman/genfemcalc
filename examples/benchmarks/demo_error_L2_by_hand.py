# -*- coding: utf-8 -*-
"""
Error in L2 norm hand calculations
----------------------------------

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '10/5/2019'
__version__ = '0.1.0'

import os
import numpy as np

import genfemcalc
import math
from genfemcalc import examples
from genfemcalc import meshgrid
from genfemcalc import fem
from genfemcalc import benchmarks
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoErrorL2ByHand(examples.Demo):

    @staticmethod
    def N(ksi, eta):
        return np.array([(1 - ksi) * (1 - eta), (1 + ksi) * (1 - eta),
                        (1 + ksi) * (1 + eta), (1 - ksi) * (1 + eta)],
                        np.float64)/4.0
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        vertices = np.array([[1.0, 1.0],
                           [2.0, 1.0],
                           [3.0, 1.0],
                           [1.0, 2.0],
                           [2.0, 2.0],
                           [3.0, 2.0]])
        elements = np.array([[0, 1, 4, 3], [1, 2, 5, 4]], dtype=np.int)
        s3 = 1.0/math.sqrt(3.0)
        gp1D = np.array([-s3, s3])
        gp2D = np.c_[np.tile(gp1D, 2), np.repeat(gp1D, 2)]
        print(gp2D)

        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerPoints(name='vertices', points=vertices)

        def tr(pts, sx, tx, sy, ty):
            return np.c_[sx*pts[:, 0]+tx,  sy*pts[:, 1]+ty]

        def tr1(pts):
            return tr(pts, 1.0/2, 3.0/2, 1.0/2, 3.0/2)

        def tr2(pts):
            return tr(pts, 1.0/2, 5.0/2, 1.0/2, 3.0/2)

        def N(ksi, eta):
            return np.array([(1 - ksi) * (1 - eta), (1 + ksi) * (1 - eta),
                             (1 + ksi) * (1 + eta), (1 - ksi) * (1 + eta)],
                            np.float64) / 4.0

        gp1 = tr1(gp2D)
        gp2 = tr2(gp2D)

        viewer.registerPoints(gp1, 'gp1')
        viewer.registerPoints(gp2, 'gp2')

        viewer.show('vertices')
        viewer.show('gp1', color='red')
        viewer.show('gp2', color='blue')
        viewer.keep()

        def F(pts):
            pts = pts.reshape(-1,2)
            return pts[:, 0]**2 + pts[:, 1]**2

        print('Value of function F at vertices')
        f = F(vertices)
        print(f)
        print('Value of function F interpolated in element 1 at gauss points')
        gpf0_0 = np.dot(f[elements[0, :]],N(gp2D[0, 0], gp2D[0, 1]))
        gpf0_1 = np.dot(f[elements[0, :]],N(gp2D[1, 0], gp2D[1, 1]))
        gpf0_2 = np.dot(f[elements[0, :]],N(gp2D[2, 0], gp2D[2, 1]))
        gpf0_3 = np.dot(f[elements[0, :]],N(gp2D[3, 0], gp2D[3, 1]))
        print('Approx in elem 2:', gpf0_0, gpf0_1, gpf0_2, gpf0_3)

        ef_0 = F(gp1[0, :])
        ef_1 = F(gp1[1, :])
        ef_2 = F(gp1[2, :])
        ef_3 = F(gp1[3, :])
        print("Exact elem1: ", ef_0, ef_1, ef_2, ef_3)

        int1 = 0.25*((gpf0_0 - ef_0)**2 +
                     (gpf0_1 - ef_1)**2 +
                     (gpf0_2 - ef_2)**2 +
                     (gpf0_3 - ef_3)**2)
        print(f"Integral in the second element: {int1}")

        gpf0_0 = np.dot(f[elements[1, :]] , N(gp2D[0, 0], gp2D[0, 1]))
        gpf0_1 = np.dot(f[elements[1, :]] , N(gp2D[1, 0], gp2D[1, 1]))
        gpf0_2 = np.dot(f[elements[1, :]] , N(gp2D[2, 0], gp2D[2, 1]))
        gpf0_3 = np.dot(f[elements[1, :]] , N(gp2D[3, 0], gp2D[3, 1]))
        print('Approx in elem 2:', gpf0_0, gpf0_1, gpf0_2, gpf0_3)

        print('Value of the exact function at gauss points')
        ef_0 = F(gp2[0, :])
        ef_1 = F(gp2[1, :])
        ef_2 = F(gp2[2, :])
        ef_3 = F(gp2[3, :])

        print("Exact elem1: ", ef_0, ef_1, ef_2, ef_3)
        int2 = 0.25*((gpf0_0 - ef_0)**2 +
                     (gpf0_1 - ef_1)**2 +
                     (gpf0_2 - ef_2)**2 +
                     (gpf0_3 - ef_3)**2)
        print(f"Integral in the second element: {int2}")

        error = math.sqrt(int1 + int2)
        print(f"Error: {error}")

        mesh, dummy = meshgrid.modelsmrg.buildRectangle(1, 3, 1, 2, 2, 1)
        solution = benchmarks.exact_solutions.ParsedSolution('x**2+y**2')
        solution.addToMeshAs('solution', mesh)
        integrator = fem.meshcalc.MeshIntegrator(mesh, ngpt=2)
        print('Mesh points')
        print(integrator.mesh.points)
        print('Mesh nodal values of field')
        print(integrator.mesh.node_fields['solution'])
        error = benchmarks.errors.computeL2ErrorVersusExact(integrator, 'solution', solution)
        print(f"L2 interpolation error: {error}")

if __name__ == '__main__':
    demo = DemoErrorL2ByHand()
    demo.run()
