# -*- coding: utf-8 -*-
"""
Mesh from image
===============

Illustrates how to read black-and-white image into MRG mesh.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.10.14 13:10:57'
__version__ = '0.2.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoMrgFromImage(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        filename = os.path.join(DATADIR, 'two_holes_grid.png')
        mesh_a = meshgrid.iomrg.readMrgFromImage(filename)
        mesh_b = meshgrid.iomrg.readMrgFromImage(filename, complement=True)

        box = meshgrid.geometrymrg.BoundingBox.fromMrgMesh(mesh_a)
        meshgrid.geometrymrg.anchorMeshAt(mesh_b, box.rtb,
                                          mode='lbf', inplace=True)

        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerMesh(mesh_a, 'mesh_a')
        viewer.registerMesh(mesh_b, 'mesh_b')
        viewer.show('mesh_a', color='black', facecolor='yellow')
        viewer.show('mesh_b', color='black', facecolor='cyan')
        viewer.keep()


if __name__ == '__main__':
    demo = DemoMrgFromImage()
    demo.run()
