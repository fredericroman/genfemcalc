# -*- coding: utf-8 -*-
"""
Barycenters of mesh cells
=========================

For a simple mesh below map barycentric points of reference elements
to physical points

.. code-block:: text

                      6
                    / |
                  /   |
                 /  2 |
       3 ------ 2 --- 5
       |        |     |
       |   0    |  1  |
       |        |     |
       0 ------ 1 --- 4

"""
__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.09.02  21:49:37'
__version__ = '0.1.0'

import numpy
import os

# MRG modules
import genfemcalc
from genfemcalc import meshgrid
from genfemcalc import fem
from genfemcalc import examples
import genfemcalc.meshgrid.graphics

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoMeshMapper(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        fname = os.path.join(DATADIR, 'simple_quad_tri_mesh.vtk')
        mesh = meshgrid.iomrg.readMrgFromVtk(fname)

        mesh_mapper = fem.meshcalc.MeshMapper(mesh, ambient_dim=2)

        physical_points = []
        for cell_index in range(mesh_mapper.numb_cells):
            ref_points = mesh_mapper.getMapper(cell_index).refcell.points
            center = numpy.mean(ref_points, axis=0).reshape(1, -1)
            physical_points.append(mesh_mapper.mapPoints(cell_index, center))

        physical_points = numpy.asarray(physical_points)

        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerMesh(mesh, 'base_mesh')
        viewer.show('base_mesh')
        viewer.registerPoints(physical_points, 'centers')
        viewer.showNodes('centers', color='red')
        viewer.keep()


if __name__ == '__main__':
    demo = DemoMeshMapper()
    demo.run()
