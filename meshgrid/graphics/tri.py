# -*- coding: utf-8 -*-
"""Support transforming MRG mesh to matplotlib.tri.Triangulation
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019-10-27 01:19:59.694317'
__version__ = '0.2.0'

import collections
import copy
import matplotlib.tri
import numpy

from genfemcalc import meshgrid
import numpy as np


class TriMesh(matplotlib.tri.Triangulation, meshgrid.wrappers.MeshProtocol):
    """Wrapper over MRG mesh to present it as Matplotib triangulation.
    """
    def __init__(self, x, y, triangles=None, mask=None):
        meshgrid.wrappers.MeshProtocol.__init__(self)
        self._points = numpy.c_[numpy.asarray(x), numpy.asarray(y)]
        matplotlib.tri.Triangulation.__init__(self, self._points[:, 0], self._points[:, 1], triangles, mask)
        self._mesh = None
        self._cell_data = dict()

    @property
    def numb_nodes(self):
        return len(self.x)

    @property
    def numb_elems(self):
        return self.neighbors.shape[0]

    @property
    def numb_cells(self):
        return self.numb_elems

    @property
    def unique_cell_types(self):
        return frozenset([meshgrid.ElemType.TRIANGLE])

    @property
    def cells_family(self):
        return meshgrid.refcell.CellFamily.VTK

    @property
    def cell2type(self):
        return numpy.asarray([int(meshgrid.ElemType.TRIANGLE)]*self.numb_cells)

    @property
    def points(self):
        return self._points

    @property
    def parent(self):
        return self._mesh

    @property
    def mesh(self):
        return self

    @property
    def elem2nodes(self):
        return self.triangles.reshape((-1))

    @property
    def p_elem2nodes(self):
        return numpy.arange(0, ((self.numb_elems+1)*3), 3)

    def getCellNodes(self, cell_id):
        return self.triangles[:, cell_id]

    def getCellDim(self, _cell_id):
        return 2

    @property
    def node_coords(self):
        return self._points

    @node_coords.setter
    def node_coords(self, coords):
        self._points = coords
        self.x = self._points[:, 0]
        self.y = self._points[:, 1]

    @property
    def elem_fields(self):
        return self._cell_data


    @property
    def node_fields(self):
        return self._mesh.node_fields

    @property
    def cell_data(self):
        return self._cell_data

    def getMappedParentCellData(self, data_name):
        return self.parent.elem_fields[data_name][self.cell_data['parent_elem']]

    def getParentData(self, data_name):
        data = self.parent.node_fields.get(data_name)
        if data is not None:
            return data
        elif data_name in self.parent.elem_fields:
            return self.getMappedParentCellData(data_name)
        else:
            raise KeyError(f"No data called {data_name} found")


    @classmethod
    def fromMrgMesh(cls, mesh, copy_points=False):
        mesh = meshgrid.wrappers.MrgMesh(mesh)
        counter = collections.Counter(mesh.cell2type)
        numb_tri = counter[meshgrid.ElemType.TRIANGLE] +\
            2*counter[meshgrid.ElemType.QUAD]
        to_parent = np.ones((numb_tri,), dtype='float')
        triangles = np.empty((numb_tri, 3), dtype=np.int64)
        tri = 0
        for elem, nodes in enumerate(mesh.elem2nodes):
            if mesh.cell2type[elem] == meshgrid.ElemType.TRIANGLE:
                triangles[tri, :] = nodes
                to_parent[tri] = elem
                tri += 1
            elif mesh.cell2type[elem] == meshgrid.ElemType.QUAD:
                triangles[tri, :] = nodes[[0, 1, 2]]
                to_parent[tri] = elem
                tri += 1
                triangles[tri, :] = nodes[[2, 3, 0]]
                to_parent[tri] = elem
                tri += 1
        if copy_points:
            points = copy.deepcopy(mesh.points)
        else:
            points = mesh.points
        self = cls(x=points[:, 0], y=points[:, 1], triangles=triangles)
        self._cell_data['parent_elem'] = to_parent
        self._points = points
        self._mesh = mesh.mesh
        return self
