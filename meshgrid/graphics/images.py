# -*- coding: utf-8 -*-
"""
Module for images manipulation and images based visualisation.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/27/2019'
__version__ = '0.1.0'

import matplotlib.cm
import matplotlib.colors


def writeArray(filename, array, norm=None, cmap=None):
    """Two-dimensional array.
    """
    if cmap is None:
        cmap = matplotlib.cm.get_cmap('seismic', lut=3)
    if norm is None:
        norm = matplotlib.colors.Normalize(array.min(), array.max())
    sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
    image = sm.to_rgba(array)
    matplotlib.pyplot.imsave(filename, image)
    return image
