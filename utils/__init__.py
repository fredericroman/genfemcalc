"""
This package collect various modules that are used across whole genfemcalc.
In particular it provides the following moules:

* ``symbolic`` - utilities for symbolic calculations
"""

if __debug__:
    print(f'Invoking __init__.py for {__name__}')


from . import symbolic
from . import enums
