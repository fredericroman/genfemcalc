# -*- coding: utf-8 -*-
"""
Build skin from image mesh
--------------------------

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '10/9/2019'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import benchmarks
from genfemcalc import meshgrid
import genfemcalc.meshgrid.graphics
import genfemcalc.benchmarks.meshes

PKGDIR = os.path.dirname(os.path.abspath(genfemcalc.__file__))
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoSkinnerFromImage(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        filename = os.path.join(DATADIR, 'two_holes_grid.png')
        mesh = meshgrid.iomrg.readMrgFromImage(filename)
        skinner = meshgrid.adjacency.Skinner.fromMesh(mesh)

        boundary_mesh = skinner.buildMesh()

        filename = os.path.join(OUTDIR, 'two_holes_skin.vtk')
        meshgrid.iomrg.writeVtkFromMrg(boundary_mesh, filename)
        self.report(f"Writing file: {filename}")

        viewer = meshgrid.graphics.MeshViewer()
        viewer.registerMesh(mesh, 'main_mesh')
        viewer.registerMesh(boundary_mesh, 'bmesh')
        viewer.show('main_mesh', color='black', facecolor='yellow')
        viewer.keep()


if __name__ == '__main__':
    demo = DemoSkinnerFromImage()
    demo.run()
