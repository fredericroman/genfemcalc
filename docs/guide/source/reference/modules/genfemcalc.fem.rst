genfemcalc.fem package
======================

.. automodule:: genfemcalc.fem
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::

   genfemcalc.fem.assembly
   genfemcalc.fem.elemcalc
   genfemcalc.fem.manifolds
   genfemcalc.fem.matrixquery
   genfemcalc.fem.meshcalc
   genfemcalc.fem.quadratures
