# -*- coding: utf-8 -*-
"""
Rasterisation
-------------

Show basic rasterization of polygon using raseterio package.
"""

import matplotlib.pyplot as plt
import os
import rasterio.features
import shapely.geometry

import genfemcalc
from genfemcalc import examples

PKGDIR = os.path.dirname(os.path.abspath(genfemcalc.__file__))
DATADIR = os.path.join(PKGDIR, 'test', 'data')
OUTDIR = os.path.join(PKGDIR, 'examples', 'output')


class DemoRasterize(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        poly = shapely.geometry.Polygon([(3.5, 3.0,),
                                         (18.0, 4.7),
                                         (10.0, 16.0),
                                         (3.0, 16.0)])
        bounds = poly.bounds
        xres, yres = 200,200
        transform = rasterio.transform.from_bounds(*bounds, xres, yres)
        img = rasterio.features.rasterize((poly,), (xres, yres), transform=transform)
        plt.imshow(img)
        plt.show()


if __name__ == '__main__':
    demo = DemoRasterize()
    demo.run()
