"""FIXME : describe unit test suite
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '11/11/2019'
__version__ = '0.1.0'

import unittest
import numpy
from genfemcalc import fem


class TestScaledRangeArray(unittest.TestCase):
    def test_real_factor(self):
        a = numpy.ones((10,), dtype='float64')
        borders = numpy.array([0, 3, 5, 10])
        factors = numpy.array([2, 6, 7])
        sra = fem.assembly.ScaledRangeArray(a, borders=borders, factors=factors)
        self.assertAlmostEqual(sra[0], 2.0)
        self.assertAlmostEqual(sra[3], 6.0)
        expected = numpy.array([2.0, 2.0, 2.0, 6.0, 6.0, 7.0], dtype='float64')
        self.assertTrue(numpy.allclose(expected, sra[0:6]))
        expected = numpy.array([2.0, 2.0, 2.0, 6.0, 6.0, 7.0, 7.0, 7.0, 7.0, 7.0],
                               dtype='float64')
        self.assertTrue(numpy.allclose(expected, sra[0:]))

    def test_complex_factor(self):
        a = numpy.ones((10,), dtype='complex128')+1j
        borders = numpy.array([0, 3, 5, 10])
        factors = numpy.array([2, 6, 7])
        sra = fem.assembly.ScaledRangeArray(a, borders=borders, factors=factors)
        self.assertAlmostEqual(sra[0], 2.0*(1+1j))
        self.assertAlmostEqual(sra[3], 6.0*(1+1j))
        expected = numpy.array([2.0*(1+1j), 2.0*(1+1j), 2.0*(1+1j), 6.0*(1+1j), 6.0*(1+1j), 7.0*(1+1j)], dtype='complex128')
        self.assertTrue(numpy.allclose(expected, sra[0:6]))
        expected = numpy.array([2.0*(1+1j), 2.0*(1+1j), 2.0*(1+1j), 6.0*(1+1j), 6.0*(1+1j), 7.0*(1+1j), 7.0*(1+1j), 7.0*(1+1j), 7.0*(1+1j), 7.0*(1+1j)],
                               dtype='complex128')
        self.assertTrue(numpy.allclose(expected, sra[0:]))
