# -*- coding: utf-8 -*-
""" Provides classes describing benchmark problems

"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019.08.23  20:44:19'
__version__ = '0.1.0'

import sympy
import sympy.vector
import numpy

from genfemcalc import meshgrid


def _broadcast(fun):
    return lambda *x: numpy.broadcast_arrays(fun(*x), *x)[0]


class PoissonInRectangle(object):
    _boundary_regions = ['b_south', 'b_east', 'b_north', 'b_west']

    def __init__(self, exact_solution, xrange=(0.0, 1.0), yrange=(0.0, 1.0)):
        self.exact_solution = exact_solution
        self.xrange = xrange
        self.yrange = yrange
        self._args = sympy.symbols('x y')
        self._geom_model = meshgrid.modelsgmsh.Rectangle()

    @property
    def geom_model(self):
        return self._geom_model

    @property
    def boundary_regions(self):
        return self._boundary_regions

    def getModelContext(self):
        context = self._geom_model.getDefaultContext()
        mycontext = {'xa': self.xrange[0], 'xb': self.xrange[1],
                     'ya': self.yrange[0], 'yb': self.yrange[1]}
        context.update(mycontext)
        return context

    def getDomain(self, **kwargs):
        meshing_context = kwargs.copy()
        for key in ('xa', 'xb', 'ya', 'yb'):
            meshing_context.pop(key, None)
        context = self.getModelContext()
        context.update(meshing_context)
        return self._geom_model.discretize(context)

    def getSourceTerm(self):
        expr = self.exact_solution.expression
        return -expr.diff(self._args[0], 2) - expr.diff(self._args[1], 2)

    def getBoundaryTerm(self, bc_type, region_name):
        self._check_valid_region(region_name)
        if bc_type.lower() == 'dirichlet':
            return self.exact_solution.expression
        elif bc_type.lower() == 'neumann':
            x, y = self._args
            normal_var = {'b_south': (-1, y),
                          'b_east': (1, x),
                          'b_north': (1, y),
                          'b_west': (-1, x)}
            normal, var = normal_var[region_name]
            return self.exact_solution.expression.diff(var) * normal
        else:
            raise ValueError(f"Invalid boundary condition type: {bc_type}")

    def evalSourceTermOn(self, points):
        expr = self.getSourceTerm()
        numeric_fun = _broadcast(sympy.lambdify(self._args, expr, modules='numpy'))
        values = numeric_fun(*points[:,0:2].T)
        return values

    def evalBoundaryTermOn(self, bc_type, region_name, points):
        self._check_valid_region(region_name)
        expr = self.getBoundaryTerm(bc_type, region_name)
        numeric_fun = _broadcast(sympy.lambdify(self._args, expr, modules='numpy'))
        values = numeric_fun(*points[:, 0:2].T)
        return values

    def checkIfOnBoundary(self, region_name, points, rtol=1e-5, atol=1e-8):
        self._check_valid_region(region_name)
        coord_value = {'b_south': (1, self.yrange[0]),
                       'b_east':  (0, self.xrange[1]),
                       'b_north': (1, self.yrange[1]),
                       'b_west':  (0, self.xrange[0])}
        coord, value = coord_value[region_name]
        return numpy.allclose(points[:, coord], value, rtol=rtol, atol=atol)

    def _check_valid_region(self, region_name):
        if region_name not in self._boundary_regions:
            raise ValueError("Invalid region name '%s'. Expected %s" %
                             (region_name, self._boundary_regions))

