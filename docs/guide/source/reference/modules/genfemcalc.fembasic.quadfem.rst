genfemcalc.fembasic.quadfem module
==================================

.. automodule:: genfemcalc.fembasic.quadfem
   :members:
   :undoc-members:
   :show-inheritance:
