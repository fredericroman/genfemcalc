# -*- coding: utf-8 -*-
"""This module contains the mesh class.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019-10-27 01:18:52.880026'
__version__ = '0.11.0'

# Python packages
import collections
import enum
import numpy as np
import sys
import sympy

from genfemcalc import utils
from genfemcalc.meshgrid.refcells import CellFamily


class ElemType(enum.IntEnum, metaclass=utils.enums.MetaIntEnum):
    """Enumeration class for element types supported by MRG Mesh.

    The names and values of enumeration correspond to VTK element
    types.
    >>> ElemType.LINE
    Convert string to enum regardless of string case.

    >>> _mrgmesh.ElemType.FromStr('line')
    >>> _mrgmesh.ElemType.FromStr('LinE')

    Convert item to the list of enums.

    >>> a = mesh._mrgmesh.ElemType.convert(('line','quad'))
    >>> b = mesh._mrgmesh.ElemType.convert((3,9))
    >>> c = mesh._mrgmesh.ElemType.convert(a)
    """
    VERTEX = 1
    LINE = 3
    TRIANGLE = 5
    QUAD = 9
    TETRA = 10
    HEXAHEDRON = 12

    @property
    def numb_nodes(self):
        npe = {ElemType.VERTEX: 1,
               ElemType.LINE: 2,
               ElemType.QUAD: 4,
               ElemType.TRIANGLE: 3,
               ElemType.TETRA: 4,
               ElemType.HEXAHEDRON: 8}
        return npe[self]

    @property
    def dim(self):
        dim = {ElemType.VERTEX: 0,
               ElemType.LINE: 1,
               ElemType.QUAD: 2,
               ElemType.TRIANGLE: 2,
               ElemType.TETRA: 3,
               ElemType.HEXAHEDRON: 3}
        return dim[self]



class Mesh(object):
    """Mesh class for finite element mesh.

    Attributes
    ----------
    numb_nodes : :obj:`int`
        Number of nodes
    numb_elems : :obj:`int`
        Number of elements
    space_dim : int
        Dimension of space
    node_coords : 2D ndarray
        Array of node coordinates
    node_fields : (dict of str: ndarray)
        Dictionary of nodal field. Keys are strings while values
        are 2D arrays
    elem2nodes : 1D ndarray
        Nodes for each element stored consecutively
    p_elem2nodes : 1D ndarray
        Pointers to ``elem2nodes`` array where adjacency of given element start.
        CAUTION the number of elemens in ``p_elem2nodes`` array should be bigger
        by 1 than number of elements so the expression
        ``p_elem2nodes[elem+1]-p_elem2nodes[elem]`` is always valid and yields number
        of nodes in element ``elem``
    elem_geotype :  1D ndarray
        Numeric id of element type
    """
    def __init__(self, numb_nodes=0, numb_elems=0, elem_type=ElemType.QUAD, space_dim=3):
        """
        Create empty or partially initialized mesh.

        If called without arguments creates empty mesh.

        If ``numb_nodes``, ``numb_elems`` and ``elem_type`` are given the the mesh
        is partially initialized. This makes easier creating meshes with uniform element type
        by hand. The partial initialization includes:

            * self.node_coords is initialized with ``numpy.empty((numb_elems, space_dim), 'float64')``
            * self.elem2nodes allocated with correct size and filled with -1
            * self.p_elem2nodes is properly initialized for given ``elem_type``
            * self.elem_geotype is properly initialized for given ``elem_type``

        Parameters
        ----------
        numb_nodes : :obj:`int`, optional
            number of nodes, if given is used to alocate array of nodes' coordinates
        numb_elems : :obj:`int, optional
            number of elements, if given is used to allocate adjacency arrays
        elem_type : :obj:`meshgrid.ElemType`, optional
            type of elements assumed when allocating adjacency arrays
        space_dim : :obj:`int`, optional
            number of coordinates per point
        """
        self.space_dim = space_dim
        self.numb_nodes = numb_nodes
        self.node_coords = np.empty((numb_nodes, space_dim), dtype=np.float64)
        self.node_fields = {}

        self.numb_elems = numb_elems
        elem_type = ElemType.convert(elem_type)
        stop = (numb_elems + 1) * elem_type.numb_nodes
        step = elem_type.numb_nodes
        self.p_elem2nodes = np.arange(0, stop, step, dtype='int64')
        self.elem2nodes = np.full(elem_type.numb_nodes * numb_elems, fill_value=-1, dtype='int64')
        self.elem_geotype = np.full((numb_elems,), fill_value=int(elem_type), dtype='int')
        self.elem_fields = {}
        return

    @property
    def cells_family(self):
        """
        :obj:`meshgrid.refcells.CellFamily`: family of mesh cells. The value is ``CellFamily.VTK``
        """
        return CellFamily.VTK

    @property
    def unique_elem_types(self):
        """Return unique sequence of cells' geometric tags
        :obj:`frozenset` of :obj:`int`: (read only) set of cells' type id
        """
        return frozenset(np.unique(self.elem_geotype))

    def _updateCounts(self):
        """This function is temporary hack easily update number of elements and
        number of points after coordinates and adjacency arrays are set.
        The values of ``self.numb_nodes`` and ``self.numb_elems`` are updated
        from ``self.node_coords`` and ``self.p_elem2nodes`` arrays.
        The value of ``self.space_dim`` is updated from ``self.node_coords``
        """
        self.numb_nodes = self.node_coords.shape[0]
        self.space_dim = self.node_coords.shape[1]
        self.numb_elems = self.p_elem2nodes.size - 1

    def __addField(self, data_dict, field_name, data_array, overwrite=False, copy=False):
        """Generic implementation of adding a data field to mesh.
        """
        if data_dict is self.node_fields:
            datatype = 'nodal'
            itemcount = self.numb_nodes
        else:
            datatype = 'elems'
            itemcount = self.numb_elems

        if (field_name in data_dict) and (overwrite == False):
            raise KeyError(f'*** Error: Field {field_name} already exists in mesh %s data.' % datatype)

        shape = data_array.shape

        if shape[0] != itemcount:
            msg = "Error: Number of array values %d is not equal to %s numbers %d."
            raise ValueError(msg % (shape[0], datatype, itemcount))

        if copy:
            data_dict[field_name] = data_array.copy
        else:
            data_dict[field_name] = data_array

        if len(shape) == 1:
            data_dict[field_name] = data_dict[field_name].reshape(shape[0], 1)

    def addElemField(self, field_name, data_array, overwrite=False, copy=False):
        """Adds array as element data.
        """
        self.__addField(self.elem_fields, field_name, data_array, overwrite, copy)

    def addNodeField(self, field_name, data_array, overwrite=False, copy=False):
        """Adds array as node data.
        """
        self.__addField(self.node_fields, field_name, data_array, overwrite, copy)

    def evalNodeField(self, field_name, expression, overwrite=False, store=True):
        """Evaluate field at nodes and optionally store it in mesh.

        Parameters
        ----------
        field_name : str
            name of the data array to be added in mesh
        expression: str
            string of the symbolic expression to be evaluated.
            (coordinates are denoted by x,y,z)
            (the expression can be passed within or without square bracket)
        overwrite: bool, optional
            If True the existing field with the field_name is silently overwritten.
            If False and the field_name exists raise KeyError exception.
            The default is False.
        store : bool, optional
            If True store evaluated field as node data array.
            If False just evaluate and return it but do not store as node data.
            The default is True.

        Returns
        -------
        numpy.array(dtype=float64)
            2D array of shape (N, C) where N is number of nodes and C is number
            of field components.
        """
        expression = expression.strip()
        if expression[0] != '[':
            expression = '[' + expression
        if expression[-1] != ']':
            expression = expression + ']'

        symbolic_expr = sympy.Matrix(sympy.parsing.sympy_parser.parse_expr(expression))
        fieldfun = utils.symbolic.lambdify_wrapper(sympy.symbols('x y z'), symbolic_expr)
        fielddata = np.asarray(fieldfun(*self.node_coords.T))
        fielddata = np.squeeze(fielddata)

        if store:
            if (field_name in self.node_fields) and not overwrite:
                raise KeyError(f'Error: Field {field_name} already exists in mesh node data.')
            else:
                self.node_fields[field_name] = fielddata
        return fielddata

    def writeToStdout(self):
        """Write mesh class on stdout.
        """
        mygps = '#  '
        print(f'{mygps}space_dim : {self.space_dim}')
        print(f'{mygps}numb_nodes : {self.numb_nodes}')
        print(f'{mygps}node_coords : {self.node_coords}')
        print(f'{mygps}node_fields : {self.node_fields}')
        print(f'{mygps}numb_elems : {self.numb_elems}')
        print(f'{mygps}elem2nodes : {self.elem2nodes}')
        print(f'{mygps}p_elem2nodes : {self.p_elem2nodes}')
        print(f'{mygps}elem_geotype : {self.elem_geotype}')
        print(f'{mygps}elem_fields : {self.elem_fields}')
        return

    def countElem(self, selector=None):
        """Return the number of elements in the mesh of type indicated by selector.

        If selector is None all element types are counted.
        
        Parameters
        ----------
            mesh  - MRG Mesh class
            selector - sequence of ElemType enums, element names or integer
                       values corresponding to VTK element types
        Returns
        -------
            Count of elements of given types
        """
        # count how many items of value in selector are in array
        if selector is None:
            count = self.numb_elems
        else:
            int_selector = [int(x) for x in ElemType.convert(selector)]
            cdict = collections.Counter(self.elem_geotype)
            count = sum(map(lambda x: x[1] * (x[0] in int_selector), cdict.items()))

        return count

    def listElemCount(self, selector=None, output=None):
        """Write to output (or sys.stdout) information about number of elements
        in a mesh.

        If selector is given detailed number of elements is written only for
        selected types.
        """
        listing = '#  number of elements:\n'
        listing += '#  total: %d\n' % self.numb_elems
        cdict = collections.Counter(self.elem_geotype)
        if selector is None:
            iterover = cdict.keys()
        else:
            iterover = [int(x) for x in ElemType.convert(selector)]

        for key in iterover:
            listing += '#  %s : %d\n' % (ElemType(key).name, cdict[key])

        if output:
            output.write(listing)
        else:
            sys.stdout.write(listing)
        return


