"""
Test routine for building random polygon
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '11/23/2019'
__version__ = '0.1.0'

import unittest
from genfemcalc import meshgrid


class TestBuildRandomPolygon(unittest.TestCase):
    def setUp(self):
        pass

    def test_number_sides(self):
        poly_spec = {'center': (0.0, 0.0), 'out_radius': 1.0,
                     'nsides': 4, 'start_angle': 45.0, 'randomize_angle': False}
        poly = meshgrid.utilsshapely.buildRandomPolygon(**poly_spec)
        self.assertEqual(5, len(poly.exterior.coords), f"Expected: 4  Actual : {len(poly.exterior.coords)}")
