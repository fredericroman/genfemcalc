"""FIXME : describe unit test suite
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '10/5/2019'
__version__ = '0.1.0'

import unittest
import meshgrid


class TestUtilsGmsh(unittest.TestCase):
    def setUp(self):
        model_context = {'xa': 0.0, 'xb': 3.0, 'ya': 0.0, 'yb': 2.0,
                         'transfinite': True, 'quadsonly': True, 'tsdensity': [4, 3]}
        rectangle = meshgrid.modelsgmsh.Rectangle()
        self.domain = rectangle.discretize(model_context, verbose=False)

    def test_countCells(self):
        self.assertEqual(16, meshgrid.utilsgmsh.countCells(self.domain.mesh))
        self.assertEqual(3, meshgrid.utilsgmsh.countCells(self.domain.mesh, names=['b_south']))
        self.assertEqual(5, meshgrid.utilsgmsh.countCells(self.domain.mesh, tags=[1, 2]))
        regions = [self.domain.regions[i] for i in (2, 4)]  # North + Domain
        self.assertEqual(9, meshgrid.utilsgmsh.countCells(self.domain.mesh, regions=regions))

    def test_countAdjacencyStorage(self):
        self.assertEqual((44, 17), meshgrid.utilsgmsh.countAdjacencyStorage(self.domain.mesh))
        self.assertEqual((6, 4), meshgrid.utilsgmsh.countAdjacencyStorage(self.domain.mesh,
                                                                          names=['b_south']))
        self.assertEqual((10, 6), meshgrid.utilsgmsh.countAdjacencyStorage(self.domain.mesh,
                                                                           tags=[1, 2]))
        regions = [self.domain.regions[i] for i in (2, 4)]  # North + Domain
        self.assertEqual((30, 10), meshgrid.utilsgmsh.countAdjacencyStorage(self.domain.mesh,
                                                                            regions=regions))
