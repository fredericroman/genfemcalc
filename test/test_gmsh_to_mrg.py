"""
Test conversion from GMSH mesh to MRG mesh
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '10/5/2019'
__version__ = '0.1.0'

import unittest
import numpy

from genfemcalc import meshgrid


class TestGmshToMrg(unittest.TestCase):
    def setUp(self):
        model_context = {'xa': 0.0, 'xb': 3.0, 'ya': 0.0, 'yb': 2.0,
                         'transfinite': True, 'quadsonly': True, 'tsdensity': [4, 3]}
        rectangle = meshgrid.modelsgmsh.Rectangle()
        self.domain = rectangle.discretize(model_context, verbose=False)
        self.mesh = meshgrid.iomrg.buildMrgFromGmsh(self.domain.mesh)

    def test_cells_count(self):
        self.assertEqual(16, self.mesh.numb_elems)

    def test_regions(self):
        expected_regions = numpy.array([1, 1, 1, 2, 2, 3, 3, 3,
                                        4, 4, 5, 5, 5, 5, 5, 5], dtype='int')
        self.assertTrue(numpy.allclose(expected_regions, self.mesh.elem_fields['_regions']))


class TestRegionsGmshToMrg(unittest.TestCase):
    def setUp(self):
        model_context = {'xa': 0.0, 'xb': 3.0, 'ya': 0.0, 'yb': 2.0,
                         'transfinite': True, 'quadsonly': True, 'tsdensity': [4, 3]}
        rectangle = meshgrid.modelsgmsh.Rectangle()
        self.domain = rectangle.discretize(model_context, verbose=False)

    def test_region_by_name(self):
        mesh = meshgrid.iomrg.buildMrgFromGmsh(self.domain.mesh, names=['b_south', 'domain'])
        self.assertEqual(9, mesh.numb_elems)
        self.assertEqual(12, mesh.numb_nodes)
        expected_regions = numpy.array([1, 1, 1, 5, 5, 5, 5, 5, 5], dtype='int')
        self.assertTrue(numpy.allclose(expected_regions, mesh.elem_fields['_regions']))

    def test_just_boundary_region_by_name(self):
        mesh = meshgrid.iomrg.buildMrgFromGmsh(self.domain.mesh, names=['b_south'])
        self.assertEqual(3, mesh.numb_elems)
        self.assertEqual(4, mesh.numb_nodes)
        expected_regions = numpy.array([1, 1, 1], dtype='int')
        self.assertTrue(numpy.allclose(expected_regions, mesh.elem_fields['_regions']))

    def test_south_north_adjacency(self):
        mesh = meshgrid.iomrg.buildMrgFromGmsh(self.domain.mesh, names=['b_south', 'b_north'])
        expected_p_elem2nodes = numpy.array([0, 2, 4, 6, 8, 10, 12], dtype=numpy.int64)
        expected_elem2nodes = numpy.array([0, 4, 4, 5, 5, 1, 2, 6, 6, 7, 7, 3])
        self.assertTrue(numpy.allclose(expected_p_elem2nodes, mesh.p_elem2nodes))
        self.assertTrue(numpy.allclose(expected_elem2nodes, mesh.elem2nodes))
    
    def test_south_north_elem_geotype(self):
        mesh = meshgrid.iomrg.buildMrgFromGmsh(self.domain.mesh, names=['b_south', 'b_north'])
        expected_elem_geotype = numpy.full(6, fill_value=int(meshgrid.ElemType.LINE), dtype=numpy.int64)
        self.assertTrue(numpy.allclose(expected_elem_geotype, mesh.elem_geotype))
