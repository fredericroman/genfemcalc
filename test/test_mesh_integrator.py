"""FIXME : describe unit test suite
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '10/4/2019'
__version__ = '0.1.0'

import unittest

from genfemcalc import meshgrid
from genfemcalc import fem


class TestMeshIntegrator(unittest.TestCase):
    def setUp(self):
        self.xmin, self.xmax = -1.0, 2.0
        self.ymin, self.ymax = -2.0, 4.0
        mesh, dummy = meshgrid.modelsmrg.buildRectangle(self.xmin, self.xmax,
                                                        self.ymin, self.ymax,
                                                        nx=3, ny=6)
        self.mesh = meshgrid.wrappers.MrgMesh(mesh)

    def test_const_fun_integration(self):
        value = 2.0
        integrator = fem.meshcalc.MeshIntegrator(self.mesh, ngpt=4)
        interpolator = fem.meshcalc.ConstFieldEvaluator(integrator, value=value)
        actual = integrator.integrate(interpolator.evalUatStoredPoints)
        expected = value * (self.xmax - self.xmin) * (self.ymax - self.ymin)
        self.assertAlmostEqual(actual, expected)

    def test_linear_fun_integrator(self):
        self.mesh.evalNodeField('temp', 'x+1')
        integrator = fem.meshcalc.MeshIntegrator(self.mesh, ngpt=4)
        interpolator = fem.meshcalc.MeshFieldEvaluator(integrator, self.mesh.node_fields['temp'])
        actual = integrator.integrate(interpolator.evalUatStoredPoints)
        expected = 27.0
        self.assertAlmostEqual(actual, expected)
