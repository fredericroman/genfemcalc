# -*- coding: utf-8 -*-
"""
Module to help working with enum classes.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '11/16/2019'
__version__ = '0.1.0'

import enum
import types

def _fromStr(cls, name):
    """Convert string to enum regardless of string case
    """
    return cls[name.upper()]

def _convert(cls, items):
    """Convert item to the list of enums"""
    if isinstance(items, str):
        return cls.fromStr(items)
    elif isinstance(items, int):
        return cls(items)
    elif isinstance(items, cls):
        return items
    else:
        return [cls.fromStr(x) if isinstance(x, str) else cls(x) for x in items]


class MetaIntEnum(type(enum.IntEnum)):
    def __new__(cls, clsname, bases, clsdict):
        clsobj = super().__new__(cls, clsname, bases, clsdict)
        clsobj.fromStr = types.MethodType(_fromStr, clsobj)
        clsobj.convert = types.MethodType(_convert, clsobj)
        return clsobj
