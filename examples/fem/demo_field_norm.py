# -*- coding: utf-8 -*-
"""
Calculate norm of a mesh field
------------------------------
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '9/13/2019'
__version__ = '0.1.0'

import os
import sympy
import sympy.vector
import math

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
from genfemcalc import fem

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoFieldNorm(examples.Demo):
    def __init__(self):
        super().__init__()

    def demonstrate(self):
        model = meshgrid.modelsgmsh.Rectangle()
        context = {'xa': 0.0, 'xb': 20.0, 'ya': 0.0, 'yb': 20.0,
                   'quadsonly': False, 'transfinite': True,
                   'tsdensity': [100, 100]}
        domain = model.discretize(context)
        mesh = meshgrid.iomrg.buildMrgFromGmsh(domain.mesh)
        expression = '3*x+y+x'
        field_data = mesh.evalNodeField(field_name='u', expression=expression)
        integrator = fem.meshcalc.MeshIntegrator(mesh, ngpt=6, ambient_dim=2)
        zz = fem.meshcalc.computeFieldIntegral(integrator, field_data, integrand_type='u')
        sym_expr = sympy.parsing.sympy_parser.parse_expr(expression)
        x, y = sympy.symbols('x, y')
        xa, xb = context['xa'], context['xb']
        ya, yb = context['xa'], context['xb']
        sym_integral = sympy.integrals.integrate(sym_expr, (x, xa, xb), (y, ya, yb))
        print(f'Integral of field: numeric {zz} symbolic {sym_integral}')
        norm = fem.meshcalc.computeFieldNormL2(integrator, field_data)
        sym_norm = math.sqrt(sympy.integrals.integrate(sym_expr**2, (x, xa, xb), (y, ya, yb)))
        grad = sympy.diff(sym_expr, x)**2 + sympy.diff(sym_expr, y)**2
        sym_grad = (sympy.integrals.integrate(grad, (x, xa, xb), (y, ya, yb)))
        #zz_g = fem.meshcalc.computeFieldIntegral(integrator, field_data, integrand_type='gradu*gradu')
        print(f"Field norm: {norm}, {sym_norm}")
        #print(f"Field gradu^2 {zz_g}, {sym_grad}")

if __name__ == '__main__':
    demo = DemoFieldNorm()
    demo.run()
