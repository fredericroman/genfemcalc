.. include:: ../../replaces.txt
.. _fem_operations-sec:

*************************
Finite Element Operations
*************************

Building shape functions
------------------------

Numerical integration
---------------------

Geometric mappings of cells
---------------------------

Interpolating fields
--------------------

Integrating fields over meshes
------------------------------

Calculations on element level
-----------------------------

Assembly of global system
-------------------------

Handling Dirichlet boundary conditions
--------------------------------------

Interpolation between meshes
----------------------------

Calculating norms and errors
----------------------------
