# -*- coding: utf-8 -*-
"""Module to produce meshes used in veryfication and demonstrations.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '16/10/2019'
__version__ = '0.2.0'

import genfemcalc.meshgrid


def getSingleQuadMesh():
    """Build mesh consisting of a single quad element.
    
    Returns
    -------
        :obj:`genfemcalc.meshgrid.Mesh` :
            Single element mesh of quad cell.
    """
    mesh = genfemcalc.meshgrid.Mesh(numb_nodes=4, numb_elems=1, elem_type='quad')
    mesh.node_coords[0, :] = [1.0, 1.0, 0.0]
    mesh.node_coords[1, :] = [2.0, 1.0, 0.0]
    mesh.node_coords[2, :] = [2.0, 3.0, 0.0]
    mesh.node_coords[3, :] = [1.0, 2.0, 0.0]
    mesh.elem2nodes[:] = [0, 1, 2, 3]
    return mesh


def getSingleTriangleMesh():
    """Build mesh consisting of a single quad element.
    
    Returns
    -------
        :obj:`genfemcalc.meshgrid.Mesh` :
            Single element mesh of quad cell.
    """
    mesh = genfemcalc.meshgrid.Mesh(numb_nodes=3, numb_elems=1, elem_type='triangle')
    mesh.node_coords[0, :] = [3.0, 1.0, 0.0]
    mesh.node_coords[1, :] = [4.0, 1.0, 0.0]
    mesh.node_coords[2, :] = [4.0, 3.0, 0.0]
    mesh.elem2nodes[:] = [0, 1, 2]
    return mesh


def getStructuredMeshInRectangle(resolution=(4, 4), xside=1.0, yside=1.0, cell_types=None):
    """Generate structured mesh in rectangle.
    Parameters
    ----------
        resolution : tuple
            Number of nodes in X and Y direction
        xside : double
            Length of X side
        yside : double
            Length of Y side
        cell_types : (list of string)
    Returns
    -------
        genfemcalc.meshgrid.Mesh
            Generated mesh
    """
    model = genfemcalc.meshgrid.modelsgmsh.Rectangle()
    context = {'xa': 0.0, 'xb': xside, 'ya': 0.0, 'yb': yside,
               'quadsonly': True, 'transfinite': True,
               'tsdensity': resolution}
    domain = model.discretize(context)
    mesh = genfemcalc.meshgrid.iomrg.buildMrgFromGmsh(domain.mesh, cell_types)
    return mesh
