# -*- coding: utf-8 -*-
"""
Random polygons
---------------
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '11/16/2019'
__version__ = '0.1.0'

import os

import genfemcalc
from genfemcalc import examples
from genfemcalc import meshgrid
import meshgrid.graphics
from meshgrid.utilsshapely import buildRandomPolygon

PKGDIR = os.path.dirname(genfemcalc.__file__)
DATADIR = os.path.join(PKGDIR, 'test', 'data')


class DemoRandomPolygons(examples.Demo):
    def demonstrate(self):
        viewer = meshgrid.graphics.MeshViewer()
        centers = [(0.0, 0.0), (1.0, 0.0), (1.0, 1.0), (0.0, 1.0)]
        factors = [0, 0.2, 0.5, 0.8]
        for center, factor in zip(centers, factors):
            polygon = buildRandomPolygon(center, out_radius=0.45, radius_factor=factor, nsides=8)
            viewer.showPolygons((polygon,), facecolor='yellow', edgecolor='black')
        spec = {'out_radius': 0.45, 'nsides': 8, 'randomize_angle':False,
                'start_angle': 0.0, 'radius_factor' : 1.0}
        polygon = buildRandomPolygon((2, 2), **spec)
        viewer.showPolygons((polygon,), facecolor='yellow', edgecolor='black')
        spec['nsides'] = 24
        spec['radius_factor'] = 0.9
        polygon = buildRandomPolygon((1, 2), **spec)
        viewer.showPolygons((polygon,), facecolor='yellow', edgecolor='black')
        spec['nsides'] = 16
        spec['radius_factor'] = 0.5
        polygon = buildRandomPolygon((2, 1), **spec)
        viewer.showPolygons((polygon,), facecolor='yellow', edgecolor='black')
        viewer.autoscale()
        viewer.keep()

if __name__ == '__main__':
    demo = DemoRandomPolygons()
    demo.run()
