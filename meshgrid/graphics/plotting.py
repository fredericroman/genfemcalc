# -*- coding: utf-8 -*-
"""This module contains a set of functions to plot mesh.
"""

__authors__ = 'RP, FM'
__copyright__ = '(c) Magoules Research Group (MRG)'
__date__ = '2019-09-01'
__version__ = '0.9.0'


# Python packages
import matplotlib
import matplotlib.tri as mtri       
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
import numpy as np

# MRG packages
from genfemcalc.meshgrid._meshmrg import Mesh, ElemType


def patch(source_mesh, fileroot, dpi=600):
    """Plot elements of the mesh manually by plotting the polygons.
    """
    selfsig = lambda x: x.__module__ + '.' + x.__name__
    mygpse = selfsig(patch) + '()'
    print(f'Traceback {mygpse}: Saving files (.eps, .jpg, .pdf).')

    def plotPolygon(axe,xy):
        axe.add_patch(Polygon(xy,closed=True,ec='k',lw=1,fill=False)) 
        return
       
    fig, axs = plt.subplots(dpi=dpi)
    axs.axis('equal')
        
    # create windows
    x_min = np.min(source_mesh.node_coords[:,0])        
    x_max = np.max(source_mesh.node_coords[:,0])
    y_min = np.min(source_mesh.node_coords[:,1])                
    y_max = np.max(source_mesh.node_coords[:,1])  
    x_range = x_max - x_min
    y_range = y_max - y_min
    plt.xlim(x_min-0.05*x_range, x_max+0.05*x_range)  # 5% more and less
    plt.ylim(y_min-0.05*y_range, y_max+0.05*y_range)  # 5% more and less   

# slow    
#        xy = np.array([])           
#        for i in range(mymesh.numb_elems):
#            etype = ElemType(mymesh.elem_geotype[i])
#            cs = mymesh.p_elem2nodes[i]
#            ce = mymesh.p_elem2nodes[i+1]
#            npe = ce - cs              
#            XY = np.empty((0,2))
#            for ii in range(cs,ce):
#                x1 = mymesh.node_coords[mymesh.elem2nodes[ii],0]
#                x2 = mymesh.node_coords[mymesh.elem2nodes[ii],1]
#                xy = np.array([x1,x2])
#                XY = np.append(XY, xy.reshape(1,2), axis=0)
#            plotPolygon(axs,XY)
      
# slow        
#        xy = np.array([])           
#        for i in range(mymesh.numb_elems):
#            etype = ElemType(mymesh.elem_geotype[i])
#            cs = mymesh.p_elem2nodes[i]
#            ce = mymesh.p_elem2nodes[i+1]
#            npe = ce - cs              
#            XY = np.empty((0,2))
#            for ii in range(cs,ce):
#                xyb = mymesh.node_coords[mymesh.elem2nodes[ii],0:2]
#                xy = np.array([xyb[0],xyb[1]])
#                XY = np.append(XY, xy.reshape(1,2), axis=0)
#            plotPolygon(axs,XY)            

# fast
    xy = np.array([])       
    for i in range(source_mesh.numb_elems):
        cs = source_mesh.p_elem2nodes[i]
        ce = source_mesh.p_elem2nodes[i+1]   
        xy = source_mesh.node_coords[source_mesh.elem2nodes[cs:ce],0:2]
        plotPolygon(axs,xy)
#    axs.set_axis_off()
    
    # show graphic
#    plt.show()
    # save graphic   
    #plt.savefig(fileroot + '_patch' + '.eps',format='eps')
    plt.savefig(fileroot + '_patch' + '.jpg',format='jpg')
    #plt.savefig(fileroot + '_patch' + '.pdf',format='pdf')
    plt.close()
    return
        

def contourf(source_mesh, node_data, fileroot, title='', dpi=600):
    """Plot node data parameter on the mesh.
    """
    selfsig = lambda x: x.__module__ + '.' + x.__name__
    mygpse = selfsig(contourf) + '()'
    print(f'Traceback {mygpse}: Saving files (.eps, .jpg, .pdf).')

    x = source_mesh.node_coords[:,0]
    y = source_mesh.node_coords[:,1]
    z = node_data 

    # create triangles for triangulation
    triangles = []
    for i in range(source_mesh.numb_elems):
        etype = ElemType(source_mesh.elem_geotype[i])
        if etype == ElemType.TRIANGLE:
            cs = source_mesh.p_elem2nodes[i]
            ce = source_mesh.p_elem2nodes[i+1]
            triangles.append(source_mesh.elem2nodes[cs:ce]) # fast
#            triangles.append(list(mymesh.elem2nodes[cs:ce])) # slow
        if etype == ElemType.QUAD:
            cs = source_mesh.p_elem2nodes[i]
            ce = source_mesh.p_elem2nodes[i+1]
#            triangles.append(list(mymesh.elem2nodes[cs:cs+3])) # slow
#            triangles.append(list( (mymesh.elem2nodes[cs],mymesh.elem2nodes[cs+2],mymesh.elem2nodes[cs+3]) )) # slow
            triangles.append([source_mesh.elem2nodes[k] for k in [cs,cs+1,cs+3]]) # slow
            triangles.append([source_mesh.elem2nodes[k] for k in [cs+1,cs+2,cs+3]]) # slow
            
    # creates triangulation
    triang = mtri.Triangulation(x, y, triangles)
    x_min = np.min(x)
    x_max = np.max(x)
    y_min = np.min(y)
    y_max = np.max(y)                
    xi, yi = np.meshgrid(np.linspace(x_min, x_max, 200), np.linspace(y_min, y_max, 200))
    
    # creates interpolation
#    interp_lin = mtri.LinearTriInterpolator(triang, z)
#    zi_lin = interp_lin(xi, yi)
#    interp_cubic_geom = mtri.CubicTriInterpolator(triang, z, kind='geom')
#    zi_cubic_geom = interp_cubic_geom(xi, yi)
    interp_cubic_min_E = mtri.CubicTriInterpolator(triang, z, kind='min_E')
    zi_cubic_min_E = interp_cubic_min_E(xi, yi)
       
#    # plot triangulation
#    fig, axs = plt.subplots()                
#    axs.tricontourf(triang, z)
#    axs.triplot(triang, 'ko-')
#    axs.set_title('Triangular grid')
#    axs.set_axis_off()        
#    # plot linear interpolation to quad grid
#    fig, axs = plt.subplots()        
#    axs.contourf(xi, yi, zi_lin)
#    #axs.plot(xi, yi, 'k-', lw=0.5, alpha=0.5)
#    #axs.plot(xi.T, yi.T, 'k-', lw=0.5, alpha=0.5)
#    axs.set_title("Linear interpolation")
#    axs.set_axis_off()      
#    # plot cubic interpolation to quad grid, kind=geom
#    fig, axs = plt.subplots()        
#    axs.contourf(xi, yi, zi_cubic_geom)
#    #axs.plot(xi, yi, 'k-', lw=0.5, alpha=0.5)
#    #axs.plot(xi.T, yi.T, 'k-', lw=0.5, alpha=0.5)
#    axs.set_title("Cubic interpolation,\nkind='geom'")
#    axs.set_axis_off()        
    # plot cubic interpolation to quad grid, kind=min_E
    fig, axs = plt.subplots(dpi=dpi)
    axs.axis('equal')
    try:
        # im = axs.contourf(xi, yi, zi_cubic_min_E,
        #               300,
        #               cmap=plt.cm.get_cmap('rainbow', 256)
        # )
        plt.title(title)

        # to fix unique legend
        array_to_visualize = zi_cubic_min_E
        cmap = matplotlib.cm.get_cmap(name='rainbow', lut=256)

        # color_min, color_max = -1., +1.
        # norm = matplotlib.colors.Normalize(color_min,color_max)
        norm = matplotlib.colors.Normalize()   # ***************************************** ERROR

        # dumy = np.linspace(-2,2,3)
        # array_to_visualize = dumy


        mappable = matplotlib.cm.ScalarMappable(cmap=cmap, norm=norm)
        mappable.set_array(array_to_visualize)
        mappable.autoscale()  # ***************************************** ERROR
        matplotlib.pyplot.colorbar(mappable, ax=plt.gca())
        mappable.changed()
        im = axs.contourf(xi, yi, zi_cubic_min_E,
                          300,
                          cmap=cmap,
                          norm=norm
                 )
        plt.cool()

#
#         im = axs.contourf(xi, yi, zi_cubic_min_E,
#                       300,
#                       cmap=plt.cm.get_cmap('rainbow', 256)#, vmin=0, vmax=0.07
#         )
#         #                      cmap = plt.cm.get_cmap('jet', 256)
#         #                      cmap=plt.cm.get_cmap('rainbow', 60)
# #        im = axs.pcolormesh(xi, yi, zi_cubic_min_E)
#        plt.cool()
#        fig.colorbar(im, ax=axs)
#        axs.plot(xi, yi, 'k-', lw=0.5, alpha=0.5)
#        axs.plot(xi.T, yi.T, 'k-', lw=0.5, alpha=0.5)
#        axs.set_title("Cubic interpolation,\nkind='min_E'")
#        axs.set_axis_off()

        # create windows
        x_range = x_max - x_min
        y_range = y_max - y_min
        plt.xlim(x_min-0.05*x_range, x_max+0.05*x_range)  # 5% more and less
        plt.ylim(y_min-0.05*y_range, y_max+0.05*y_range)  # 5% more and less

        # show graphic
#        plt.show()
        # save graphic
        #plt.savefig(fileroot + '_contourf' + '.eps', format='eps')
        plt.savefig(fileroot + '_contourf' + '.jpg', format='jpg')
        #plt.savefig(fileroot + '_contourf' + '.pdf', format='pdf')
        plt.close()
    except:
        print(f'Error:')
        print(f'  In function {mygpse} functionality is ambiguous.')
        pass
    return
