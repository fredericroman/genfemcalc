# -*- coding: utf-8 -*-
"""
Terms on line element
=====================

"""
import numpy as np

from genfemcalc import fembasic
from genfemcalc import fem
from genfemcalc import meshgrid

linenodes = np.array([[0, 0, 0], [3, 4, 0]])
cell = meshgrid.refcells.getRefCell('vtk', meshgrid.ElemType.convert('line'))
quadrature = fem.quadratures.Quadrature(cell, 2)
 
mapper = fem.manifolds.GeomMapper(cell,ambient_dim=2)
mapper.precomputeForQuadrature(quadrature)
mapper.setNodes(linenodes)

print('precomputed dN',mapper.precomputed_dN.shape)
MA = fem.elemcalc.calcMassTerm(mapper)

MB = fembasic.linefem.calcMassTerm(linenodes)

if np.allclose(MA, MB):
    print("Matrices are the same")
else:
    print('Matrices are different')
    print(MA)
    print('--------------------')
    print(MB)
print(cell.points)
print(mapper.mapPoints(cell.points))

f = [1,2]
print (fem.elemcalc.calcSourceTerm(mapper, f))
print (fembasic.linefem.calcSourceTerm(linenodes, f))

D = np.array([[1,0],[0,1]])

KA = fem.elemcalc.calcEllipticTerm(mapper, D)

KB = fembasic.linefem.calcEllipticTerm(linenodes)

if np.allclose(KA, KB):
    print("Stiffness are the same")
else:
    print('Stiffness are different')
    print(KA)
    print('--------------------')
    print(KB)



